package cpm.qbslearning.bibleforce;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import cpm.qbslearning.bibleforce.util.Constants;

public class AboutDetailActivity extends AppCompatActivity {
    ImageView backView;
    TextView titleView;
    WebView webView;
    private LinearLayout llSignupBack;
    private TextView txtAboutTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_detail);

        backView = (ImageView) findViewById(R.id.v_back);
        llSignupBack = findViewById(R.id.ll_signup_back);
        txtAboutTitle = findViewById(R.id.txtAboutTitle);

        llSignupBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        titleView = (TextView) findViewById(R.id.v_title);
        webView = (WebView) findViewById(R.id.v_web);
        Intent intent = getIntent();
        if (intent != null) {
            String title = intent.getStringExtra(Constants.TITLE);
            String url = intent.getStringExtra(Constants.ABOUT_URL);
            titleView.setText(title + "");
            txtAboutTitle.setText(title);
            webView.loadUrl(url);
        }
    }

}
