package cpm.qbslearning.bibleforce.service;

public class DownloadService /*extends IntentService */{
  /*  private PrefManager manager;
    private static Context mContext;
    public static boolean status = false;
    public static final String NOTIFICATION = "cpm.regenapps.bibleforce.dashboardActivity";
    public static final String RESULT = "RESULT";
    private static final String ACTION_NETWORK_OPR = "cpm.regenapps.bibleforce.action.DOWNLOADING";
    private static int size = 0, count = 0;
    public static int temp = 0;
    final static Object mutex = new Object();
    DownloadingThread downloadingThread;
    static DashboardActivity.DashboardHandler dashboardHandler;

    static class LocalHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            temp++;
            Bundle bundle = new Bundle();
            Message message = new Message();

            String msgRec= msg.getData().getString("msg");
            int dataRec= msg.getData().getInt("data");
            Log.e("handler", temp + "");
            if (temp >= size) {
                Log.e(this.getClass().getName(), "Message received: " + (String) msg.obj);
                bundle.putInt("data", 1);
                bundle.putString("msg", "DB successfully updated");
                message.setData(bundle);
                dashboardHandler.sendMessage(message);
            }
        }
    }

    protected Handler handler;

    public DownloadService() {
        super("name");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        manager = new PrefManager(this);
        this.handler = new LocalHandler();
        Log.e("service start", "true");
    }


//    public static void startDownloadingService(Context context, DashboardActivity.DashboardHandler handler) {
//        mContext = context;
//        Log.e("service ", "getCalled");
//        Intent intent = new Intent(context, DownloadService.class);
//        intent.setAction(ACTION_NETWORK_OPR);
//        dashboardHandler = handler;
//        mContext.startService(intent);
//    }


//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        super.onStartCommand(intent, flags, startId);
//        if (intent != null) {
//            if (intent.getAction().equals(ACTION_NETWORK_OPR)) {
//                Thread thread = new Thread(getRunnable(0));
//                thread.start();
//            }
//        } else {
//            Log.e("service ", "get null intent");
//        }
//        return Service.START_STICKY;
//    }
//
//    @Override
//    public IBinder onBind(Intent intent) {

//        throw new UnsupportedOperationException("Not yet implemented");
//    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            if (intent.getAction().equals(ACTION_NETWORK_OPR)) {

                handleNetworkOperation();
//                Notification notification = new Notification.Builder(mContext)
//                        .setSmallIcon(R.drawable.icon)
//                        .setContentText("Initial set up")
//                        .setContentTitle("Bible")
//                        .getNotification();
//                startForeground(1, notification);
            }
        } else {
            Log.e("service ", "get null intent");
        }
    }


//    private Runnable getRunnable(final int type) {
//
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                threadOperation(type);
//
//                Log.e("Service", "Thread end");
//            }
//        };
//
//        return runnable;
//    }
//
//    synchronized boolean threadOperation(int type) {
//
//        if (type == 0) {
//            handleNetworkOperation();
//            return true;
//        } else {
//            // publishResults("threadOpeation", status);
//            return status;
//        }
//    }

    private boolean handleNetworkOperation() {
        Log.e("service", "start handleNetworkOperation");
        boolean dataBackupStatus = false;
        SharedPreferences pref = manager.getSharedPref();
        String uId = pref.getString(getString(R.string.uId), null);
        String token = pref.getString(getString(R.string.token), null);
        String dId = pref.getString(getString(R.string.dId), null);
        Log.e("service", "service getData " + uId + ".,  " + token + "  , " + dId);

        Call<ChapterResponse> responseCall = RetrofitClient.getRetrofitClient(BASE_URL)
                .create(RetrofitInterface.class)
                .getChapters(token, uId, "getAllChapters", dId);

        responseCall.enqueue(new Callback<ChapterResponse>() {
            @Override
            public void onResponse(Call<ChapterResponse> call, Response<ChapterResponse> response) {
                Log.e("service", "response" + response.toString());
                ChapterResponse chapterResponse = response.body();
                if (chapterResponse != null) {
                    Log.e("service", "service message" + chapterResponse.getMessage());
                    Log.e("service", "service status" + chapterResponse.getStatus());

                    if (!chapterResponse.getStatus().equals("0") || !chapterResponse.getStatus().equals("Session Expired")) {
                        Log.e("service", "service message" + chapterResponse.getMessage());
                        size=chapterResponse.getResultData().size();
                        downloadingThread = new DownloadingThread(chapterResponse, handler);
                        downloadingThread.start();
                    } else {
                        Log.e("service", "service message" + chapterResponse.getStatus() + ",");
                    }
                }
            }

            @Override
            public void onFailure(Call<ChapterResponse> call, Throwable t) {
                //   Log.e("onFailure", t.getMessage());
                call.cancel();
                // publishResults("retrofit Error ",false);
            }
        });


        return true;
    }


    class DownloadingThread extends Thread {
        ChapterResponse mResponse;
        Handler mHandler;

        DownloadingThread(final ChapterResponse response, Handler handler) {
            mResponse = response;
            mHandler = handler;
        }

        @Override
        public void run() {
            super.run();
            if (mResponse.getResultData() != null)
                dataBackUp(mResponse, mHandler);
            else {
                String msg = mResponse.getMessage();
                publishResults(msg,true);
                Log.e("service ",msg);
                Bundle bundle=new Bundle();
                bundle.putString("msg",msg);
                bundle.putInt("data",0);
                Message message=new Message();
                message.setData(bundle);
                mHandler.sendMessage(message);
            }
        }
    }


//    public void awaitAndReportData() throws InterruptedException {
//        countDownLatch.await();
//        synchronized (mutex) {
//            Log.e("All workers done", ". workData=" + workData);
//            publishResults("await", true);
//        }
//    }


    private void publishResults(String outputPath, boolean result) {
        Log.e("onRecive", "publishResults called");
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        intent.putExtra("msg", outputPath);
        sendBroadcast(intent);
    }

//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
//        restartServiceIntent.setAction(ACTION_NETWORK_OPR);
//        PendingIntent restartServicePendingIntent = PendingIntent.getService(
//                getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
//        AlarmManager alarmService = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        alarmService.set(ELAPSED_REALTIME, elapsedRealtime() + 1000,
//                restartServicePendingIntent);
//
//        Log.e("SERVICE ", "REMOVED -------------------------------------------------");
//
//        super.onTaskRemoved(rootIntent);
//    }


    public int imageDownloadAndSave(final Handler handler, final Context context, final String imageUrl, final int i, final String... id) throws InterruptedException {

        final boolean result = false;
        Thread downloadingThread = new Thread(new Runnable() {

            @Override
            public void run() {

                Log.e("imageDownloadAndSave ", imageUrl + "< chapterId " + id[0] + " < ");
                if (id.length == 2)
                    Log.e("imageDownloadAndSave ", imageUrl + "< pageId " + id[1] + " < ");

                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL(imageUrl);
                    Bitmap bitmap;
                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    bitmap = BitmapFactory.decodeStream(in);
                    ByteArrayOutputStream oStream = new ByteArrayOutputStream();
                    try {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
                        byte[] buffer = oStream.toByteArray();
                        Log.e("buffer", "id " + id[0] + "buffer---------- " + buffer);
                        upDateChapterData(context, buffer, i, id);
                        Bundle bundle = new Bundle();
                        bundle.putInt("temp", 1);

                        Message message = new Message();
                        message.setData(bundle);
                        Log.e("handler ", "sending msg");
                        handler.sendMessage(message);

//                        synchronized (mutex) {
//                            workData++;
//                        }
//                        countDownLatch.countDown();
                        oStream.flush();
                        oStream.close();
                    } catch (IOException e) {
                        Log.e("image error", e.getLocalizedMessage());
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
            }
        });

        downloadingThread.start();
        return i;
    }


    public static void upDateChapterData(Context context, byte[] buffer, int j, String... id) {

        ContentValues values = new ContentValues();
        if (id.length == 1) {
            values.put(DbContract.Chapter.CHAPTER_THUMB, buffer);
            values.put(DbContract.Chapter.IS_CHAPTER_THUMB_UPDATED, 1);
            String where = DbContract.Chapter.CHAPTER_ID + "=?";
            String[] whereArgs = {id[0]};
            int i = context.getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, where, whereArgs);
            Log.e("chapter chapterId ", j + "=" + id[0] + " ,  updated row " + i);
        } else {
            values.put(DbContract.ChapterPage.PAGE_IMAGE, buffer);
            String where = CHAPTER_PAGE_ID + "=? AND " + PAGE_ID + "=?";
            String[] whereArgs = {id[0], id[1]};
            int i = context.getContentResolver().update(DbContract.ChapterPage.CONTENT_URI, values, where, whereArgs);
            Log.e("page chapterId ", j + "=" + id[0] + " and " + id[1] + " ,  insert status" + i);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("service ", "destroy");
    }*/
}
