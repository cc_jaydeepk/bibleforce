package cpm.qbslearning.bibleforce.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.TestActivity;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.ActionResponse;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.util.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.AUDIO_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.CHAPTER_PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IMAGE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IS_PAGE_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_AUDIO;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_IMAGE;
import static cpm.qbslearning.bibleforce.util.Constants.EXTRA_ITEM_POSITION;
import static cpm.qbslearning.bibleforce.util.Constants.IMAGE_BASE_URL;
import static cpm.qbslearning.bibleforce.util.Constants.getAudioFilePath;
import static cpm.qbslearning.bibleforce.util.Constants.getImageFilePath;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ChapterDownloadService extends IntentService {

    public static String RESULT;
    private static Context mContext;
    private static final String ACTION_DOWNOAD_CHAPTER = "cpm.regenapps.bibleforce.service.action.download.chapter";
    private static final String ACTION_BAZ = "cpm.regenapps.bibleforce.service.action.BAZ";
    public static final String NOTIFICATION = "cpm.regenapps.bibleforce.testamentActivity";
    private static final String EXTRA_PARAM1 = "cpm.regenapps.bibleforce.service.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "cpm.regenapps.bibleforce.service.extra.PARAM2";
    private static TestActivity.MyHandler myHandler;
    private int result = Activity.RESULT_CANCELED;
    private static int temp = 0;
    LocalHandler handler;
    static int size = 0;
    static String mCId;
    private static int mCaller = -1;

    public ChapterDownloadService() {
        super("ChapterDownloadService");
    }

    public static void startChapterDownloading(Context context, TestActivity.MyHandler handler, String chapterId, int position, int caller) {
        mContext = context;
        mCaller = caller;
        Log.e("downloading", "called");
        myHandler = handler;
        mCId = chapterId;
        Intent intent = new Intent(context, ChapterDownloadService.class);
        intent.setAction(ACTION_DOWNOAD_CHAPTER);
        intent.putExtra(EXTRA_PARAM1, chapterId);
        intent.putExtra(EXTRA_ITEM_POSITION, position);
        context.startService(intent);
    }

    static class LocalHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            temp++;
            Bundle bundle = new Bundle();
            Message message = new Message();

            String msgRec = msg.getData().getString("msg");
            int dataRec = msg.getData().getInt("data");
            Log.e("handler", temp + "");
            if (temp >= size) {
                Log.e(this.getClass().getName(), "Message received: " + (String) msg.obj);
                bundle.putInt("data", 1);
                bundle.putString("cId", mCId);
                message.setData(bundle);
                // myHandler.sendMessage(message);
            }
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            int position = -1;
            handler = new LocalHandler();
            if (ACTION_DOWNOAD_CHAPTER.equals(action)) {
                Log.e("downloading", action + "called");
                final String chapterId = intent.getStringExtra(EXTRA_PARAM1);
                position = intent.getIntExtra(EXTRA_ITEM_POSITION, -1);
                handleChapterDownloading(mContext, chapterId, position);
                //chapterDownloadingMethod(mContext, chapterId);
            }
        }
    }

    private void chapterDownloadingMethod(Context context, String chapterId) {
        String selection = null;
        String[] args = new String[1];
        if (chapterId != null) {
            selection = CHAPTER_PAGE_ID + "=?";
            args[0] = chapterId;
        } else {
            args = null;
        }


        Log.e("downloading", "handle called chapterId : ->" + args);
        Cursor cursor = context.getContentResolver().query(DbContract.ChapterPage.CONTENT_URI, null, selection, args, null);
        if (cursor != null && cursor.getCount() > 0) {

        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
                localBroadcastManager.sendBroadcast(new Intent());
            }
        }).start();


        String[] fileNames = {"a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9"};

        Log.e("start", "called");
//        ExecutorService pool = Executors.newFixedThreadPool(5);
//        for (String name : fileNames) {
//            pool.submit(new DownloadTask(name, "toPath"));
//        }

        int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                NUMBER_OF_CORES * 2,
                NUMBER_OF_CORES * 2,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>()
        );


        Log.e("stop", "called");
        // pool.shutdown();
        //pool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);


    }

    private static class DownloadTask implements Runnable {

        private String name;
        private final String toPath;

        public DownloadTask(String name, String toPath) {
            this.name = name;
            this.toPath = toPath;
        }

        @Override
        public void run() {
            android.os.Process
                    .setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            Log.e("run", name + "");

            for (int i = 0; i < 5; i++) {
                Log.e("name : ", "" + i);
            }

        }
    }

    private void handleChapterDownloading(Context context, String chapterId, int position) {
        String selection = null;
        String[] args = new String[1];
        if (chapterId != null) {
            selection = CHAPTER_PAGE_ID + "=?";
            args[0] = chapterId;
        } else {
            args = null;
        }

        Log.e("downloading", "handle called chapterId : ->" + args);
        String[] projection = {PAGE_ID, IMAGE_URL, AUDIO_URL, CHAPTER_PAGE_ID, IS_PAGE_DOWNLOADED};

        Cursor cursor = context.getContentResolver().query(DbContract.ChapterPage.CONTENT_URI, null, selection, args, null);
        if (cursor != null && cursor.getCount() > 0) {
            Log.e("handleChapterDown.. ", "cursor not null");
            int size = 0;
//            Log.e("file size", size + "");
            int i = 0;
            PrefManager manager = new PrefManager(context);
            String baseUrl = manager.getSharedPref().getString(IMAGE_BASE_URL, null);
            Bitmap bitmap;
            boolean[] status = new boolean[2];
            if (baseUrl != null)
                while (cursor.moveToNext()) {
                    size = size + (int) (100 / cursor.getCount());
                    Log.e("file", i++ + "downloading");
                    int isPageDownloaded = cursor.getInt(cursor.getColumnIndex(IS_PAGE_DOWNLOADED));
                    if (isPageDownloaded == mContext.getResources().getInteger(R.integer.NOT_DOWNLOADED)) {
                        final String pageId = cursor.getString(cursor.getColumnIndex(PAGE_ID));
                        final String cId = cursor.getString(cursor.getColumnIndex(CHAPTER_PAGE_ID));
                        Log.e("CHAPTER_PAGE_ID", cId + "");
                        Log.e("PAGE_ID", pageId + "");
                        final String imageUrl = cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.IMAGE_URL));
                        final String audioUrl = cursor.getString(cursor.getColumnIndex(AUDIO_URL));

//                        Log.e("imageUrl", imageUrl + "");
//                        Log.e("audioUrl", audioUrl + "");
//                        Log.e("imagePathurl", baseUrl + "" + imageUrl + "");

                        HttpURLConnection conn;
                        URL url;
                        try {
                            url = new URL(baseUrl + "" + imageUrl);
                            conn = (HttpURLConnection) url.openConnection();
                            InputStream in = new BufferedInputStream(conn.getInputStream());
                            byte[] buffer = new byte[1500];
                            FileOutputStream output = openFileOutput(getImageFilePath(mContext).concat(imageUrl), Context.MODE_PRIVATE);
                            try {
                                int bytesRead = 0;
                                while ((bytesRead = in.read(buffer, 0, buffer.length)) >= 0) {
                                    output.write(buffer, 0, bytesRead);
                                }
                                Log.e("Image", "downloading complete");
                                updateImage(pageId, 1);
                            } finally {
                                output.close();
                                buffer = null;
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        status[1] = downloadAudioFile(baseUrl + "" + audioUrl, audioUrl);
                        if (status[1])
                            updateAudio(pageId, getResources().getInteger(R.integer.DOWNLOADED));
                        else
                            updateAudio(pageId, getResources().getInteger(R.integer.NOT_DOWNLOADED));

                        Log.e("AUDIO ", "STATUS " + status[1]);
                        if (status[1] && status[0]) {
                            result = 1;
                        }
                        updatePercentage(size);

                        ContentValues values1 = new ContentValues();
                        values1.put(IS_PAGE_DOWNLOADED, mContext.getResources().getInteger(R.integer.DOWNLOADED));
                        mContext.getContentResolver().update(DbContract.ChapterPage.CONTENT_URI, values1, CHAPTER_PAGE_ID + "=?", new String[]{cId});

                        ContentValues values = new ContentValues();
                        values.put(IS_DOWNLOADED, mContext.getResources().getInteger(R.integer.DOWNLOADED));
                        mContext.getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_ID + "=?", new String[]{cId});

                    } else {
                        Log.e("already", "downloaded");
                    }
                }

        } else {
            Log.e("downloading", "cursor null called");
        }

        Log.e("printing ", "page's each row data");
        cursor.close();
        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL)
                .create(RetrofitInterface.class);
        PrefManager manager = new PrefManager(mContext);
        SharedPreferences pref = manager.getSharedPref();
        String uId = pref.getString(mContext.getResources().getString(R.string.uId), null);
        String token = pref.getString(mContext.getResources().getString(R.string.token), null);
        String dId = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);

        Call<ActionResponse> call = retrofitInterface.downloadUsingAccessCode(token, uId, "markedAllChaptersDownload", dId);
        call.enqueue(new Callback<ActionResponse>() {
            @Override
            public void onResponse(Call<ActionResponse> call, Response<ActionResponse> response) {

            }

            @Override
            public void onFailure(Call<ActionResponse> call, Throwable t) {

            }
        });
        publishResults(chapterId, 1, position);
    }

    boolean downloadAudioFile(String download_file_path, String fileName) {

        String pathToSave = getAudioFilePath(mContext);
        Log.e("downloading Audio", "path " + download_file_path + " fileName " + fileName + " pathToSave " + pathToSave);

        int downloadedSize = 0;
        int totalSize = 0;

        try {
            URL url = new URL(download_file_path);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            File myDir = new File(pathToSave);
            myDir.mkdirs();
            Log.e("downloadAudioFile ", myDir.getAbsolutePath());
            File file = new File(myDir, fileName);
            FileOutputStream fileOutput = openFileOutput(getImageFilePath(mContext).concat(fileName), Context.MODE_PRIVATE);
            InputStream inputStream = urlConnection.getInputStream();

            totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
            }

            Log.e("audio", "downloading complete");
            fileOutput.close();

            return true;
        } catch (final MalformedURLException e) {
            Log.e("Error :", " MalformedURLException " + e);
            e.printStackTrace();
        } catch (final IOException e) {
            Log.e("Error", " : IOException " + e);
            e.printStackTrace();
        } catch (final Exception e) {
            Log.e("Error ", ": Please check your internet connection " + e);
        }

        return false;
    }

    private boolean updateImage(String id, int status) {
        String where = PAGE_ID + "=?";
        String[] args = {id};

        ContentValues values = new ContentValues();
        values.put(PAGE_IMAGE, status);
        int result = mContext.getContentResolver().update(DbContract.ChapterPage.CONTENT_URI, values, where, args);
        Log.e("Image", "------result------" + result);
        if (result > 0)
            return true;
        else
            return false;
    }

    private boolean updateAudio(String id, int status) {
        String where = PAGE_ID + "=?";
        String[] args = {id};
        ContentValues values = new ContentValues();
        values.put(PAGE_AUDIO, status);
        int result = mContext.getContentResolver().update(DbContract.ChapterPage.CONTENT_URI, values, where, args);
        return result > 0;
    }

    private void publishResults(String chapterId, int result, int position) {
        Log.e("receiver", "called " + result);
        Log.e("receiver", "called chapterId" + chapterId);
        Intent intent = new Intent(NOTIFICATION);
//        intent.putExtra(RESULT, result);
        intent.putExtra(Constants.DOWNLOAD_SERVICE_REQUEST_CODE, Constants.DOWNLOAD_COMPLETE);
        intent.putExtra(Constants.EXTRA_CHAPTER_ID, chapterId);
        intent.putExtra(Constants.EXTRA_ITEM_POSITION, position);
        intent.putExtra(Constants.EXTRA_CALLER, mCaller);

        sendBroadcast(intent);
    }

    private void updatePercentage(int percentage) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(Constants.DOWNLOAD_SERVICE_REQUEST_CODE, Constants.PERCENTAGE);
        intent.putExtra(Constants.DOWNLOAD_PERCENTAGE, percentage);
        intent.putExtra(Constants.EXTRA_CALLER, mCaller);
        sendBroadcast(intent);
    }
}
