package cpm.qbslearning.bibleforce.service;

public class NetworkOperationService {
//        extends IntentService {
//    // TODO: Rename actions, choose action names that describe tasks that this
//    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
//    private static final String ACTION_NETWORK_OPR = "cpm.regenapps.bibleforce.action.FOO";
//    private static final String ACTION_BAZ = "cpm.regenapps.bibleforce.action.BAZ";
//    public static final String NOTIFICATION = "cpm.regenapps.bibleforce.dashboardActivity";
//    public static final String RESULT = "RESULT";
//    private boolean status = false;
//
//    // TODO: Rename parameters
//    private static final String EXTRA_PARAM1 = "cpm.regenapps.bibleforce.extra.PARAM1";
//    private static final String EXTRA_PARAM2 = "cpm.regenapps.bibleforce.extra.PARAM2";
//
//    private static Context mContext;
//
//    public NetworkOperationService() {
//        super("NetworkOperationService");
//        Log.e("service", "start");
//    }
//
//
//    public static void startNetworkOperation(Context context) {
//        Log.e("startNetworkOperation","called");
//        Intent intent = new Intent(context, NetworkOperationService.class);
//        intent.setAction(ACTION_NETWORK_OPR);
//        mContext = context;
//        context.startService(intent);
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//        Log.e("service", "start");
//        boolean eventStatus = false;
//        if (intent != null) {
//            Log.e("service", "intent not null");
//            Log.e("service", "action " + intent.getAction());
//            final String action = intent.getAction();
//            if (ACTION_NETWORK_OPR.equals(action)) {
//                Log.e("service", "called " + intent.getAction());
//                eventStatus=handleNetworkOperation();
//             }
//        }
//    }
//
//    /**
//     * Handle action Foo in the provided background thread with the provided
//     * parameters.
//     */
//
//    private boolean handleNetworkOperation() {
//        Log.e("service", "start handleNetworkOperation");
//        boolean dataBackupStatus = false;
//        PrefManager manager = new PrefManager(this);
//        SharedPreferences pref = manager.getSharedPref();
//        String uId = pref.getString(getString(R.string.uId), null);
//        String token = pref.getString(getString(R.string.token), null);
//        String dId = pref.getString(getString(R.string.dId), null);
//        Log.e("service", "service getData " + uId + ".," + token + ", " + dId);
//
//        Call<ChapterResponse> responseCall = RetrofitClient.getRetrofitClient(BASE_URL)
//                .create(RetrofitInterface.class)
//                .getChapters(token, uId, "getAllChapters", dId);
//
//        responseCall.enqueue(new Callback<ChapterResponse>() {
//            @Override
//            public void onResponse(Call<ChapterResponse> call, Response<ChapterResponse> response) {
//                Log.e("service", "response" + response.toString());
//                ChapterResponse chapterResponse = response.body();
//                if (chapterResponse != null) {
//                    Log.e("service", "service message" + chapterResponse.getMessage());
//                    Log.e("service", "service status" + chapterResponse.getStatus());
//
//                    if (!chapterResponse.getStatus().equals("0") || !chapterResponse.getStatus().equals("Session Expired")) {
//                        Log.e("service", "service message" + chapterResponse.getMessage());
//                        dataBackUp(chapterResponse);
//                        Log.e("PRINTING ", "CHAPTER TABLE");
//                        CommonMethods.printData(mContext, DbContract.Chapter.CONTENT_URI, false);
//                        Log.e("PRINTING ", "CHAPTER PAGE TABLE");
//                        CommonMethods.printData(mContext, DbContract.ChapterPage.CONTENT_URI, true);
//                    } else {
//                        Log.e("service", "service message" + chapterResponse.getStatus() + ",");
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ChapterResponse> call, Throwable t) {
//                //   Log.e("onFailure", t.getMessage());
//                call.cancel();
//            }
//        });
//
//
//           return true;
//    }
//
//    /**
//     * Handle action Baz in the provided background thread with the provided
//     * parameters.
//     */
//
//    public void dataBackUp(ChapterResponse response) {
//        List<Chapter> chapterList = response.getResultData();
//        PrefManager manager = new PrefManager(mContext);
//        String token = "";
//        Uri uri = null;
//        manager.getEditor().putString(IMAGE_BASE_URL, response.getServerUrl()).apply();
//        for (int i = 0; i < chapterList.size(); i++) {
//
//            Chapter chapter = chapterList.get(i);
//            ContentValues ChapterValues = new ContentValues();
//            ChapterValues.put(CHAPTER_ID, chapter.getChapterId());
//            ChapterValues.put(CHAPTER_NAME, chapter.getChapterName());
//            ChapterValues.put(CHAPTER_THUMB, chapter.getChapterThumb());
//            ChapterValues.put(TESTAMENT_TYPE, chapter.getTestamentType());
//            ChapterValues.put(IS_PAID, chapter.getIsPaid());
//            ChapterValues.put(PAGE_COUNT, chapter.getNoOfPages());
//            ChapterValues.put(IS_DOWNLOADED, getResources().getInteger(R.integer.NOT_DOWNLOADED));
//            uri = getContentResolver().insert(DbContract.Chapter.CONTENT_URI, ChapterValues);
//            if (uri == null)
//                token = token.concat("(0)");
//            else
//                token = token.concat("(1)");
//
//            Log.e("service", "chapterId " + chapter.getChapterId() + " testamentType " + chapter.getTestamentType() + "> " + chapter.getChapterThumb());
//            CommonMethods.imageDownloadAndSave(mContext, response.getServerUrl() + "" + chapter.getChapterThumb(), chapter.getChapterId());
//            List<ChapterPage> pageList = chapter.getChapterPages();
//            for (int j = 0; j < pageList.size(); j++) {
//                ChapterPage chapterPage = pageList.get(j);
//                ContentValues chapterPageValues = new ContentValues();
//                chapterPageValues.put(PAGE_ID, chapterPage.getPageId());
//                chapterPageValues.put(CHAPTER_PAGE_ID, chapter.getChapterId());
//                chapterPageValues.put(PAGE_TITLE, chapterPage.getPageTitle());
//                chapterPageValues.put(PAGE_ORDER, chapterPage.getPageOrder());
//                chapterPageValues.put(IMAGE_URL, chapterPage.getUrl());
//                chapterPageValues.put(PAGE_IMAGE, chapterPage.getUrl());
//                chapterPageValues.put(AUDIO_URL, chapterPage.getAudioName());
//                chapterPageValues.put(PAGE_AUDIO, chapterPage.getAudioName());
//                uri = getContentResolver().insert(DbContract.ChapterPage.CONTENT_URI, chapterPageValues);
//                if (uri == null)
//                    token.concat("(0)");
//                else
//                    token.concat("(1)");
//             }
//            //           List<ChapterPage> chapterPages=chapter.getChapterPages();
////            for (int j=0;j<chapterPages.size();j++){
////                ChapterPage page=chapterPages.get(j);
////                ContentValues pageValues=new ContentValues();
////                pageValues.put(CHAPTER_PAGE_ID,chapter.getChapterId());
////                pageValues.put(PAGE_TITLE,page.getPageTitle());
////                pageValues.put(PAGE_ORDER,page.getPageOrder());
////                pageValues.put(PAGE_URL,page.getUrl());
////                pageValues.put(AUDIO_NAME,page.getAudioName());
////                getContentResolver().insert(DbContract.ChapterPage.CONTENT_URI,pageValues);
////            }
//        }
//
//        if (token.contains("(0)")) {
//            Log.e("service", "token contains " + "(0)");
//            status = false;
//        } else {
//            Log.e("service", "token not contains " + "(0)");
//            status = true;
//        }
//        publishResults("rmn", status);
//    }
//
//
//    private void publishResults(String outputPath, boolean result) {
//        Log.e("onRecive", "publishResults called");
//        Intent intent = new Intent(NOTIFICATION);
//        intent.putExtra(RESULT, result);
//        sendBroadcast(intent);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Log.e("service", "destroy");
//    }
}
