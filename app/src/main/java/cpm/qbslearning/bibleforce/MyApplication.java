package cpm.qbslearning.bibleforce;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import io.alterac.blurkit.BlurKit;
import io.fabric.sdk.android.Fabric;

/**
 * Created by rmn on 21-09-2017.
 */

public class MyApplication extends Application {

    private static MyApplication singleton;
    KProgressHUD hud;

    public static MyApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        Fabric.with(this, new Crashlytics());

        super.onCreate();
        singleton = this;
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        Picasso.setSingletonInstance(built);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        registerActivityLifecycleCallbacks(mCallBack);
        BlurKit.init(this);
    }

    private ActivityLifecycleCallbacks mCallBack = new ActivityLifecycleCallbacks() {


        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivityStarted(Activity activity) {
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Log.i("Current Screen", activity.toString());
        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }

        @Override
        public void onActivityCreated(Activity context, Bundle arg1) {

        }
    };
}
