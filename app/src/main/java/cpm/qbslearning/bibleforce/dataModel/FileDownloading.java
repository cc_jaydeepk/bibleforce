package cpm.qbslearning.bibleforce.dataModel;

/**
 * Created by rmn on 07-09-2017.
 */

public class FileDownloading {
    String pagId;
    String  column;
    public FileDownloading(String column,String pagId){
        this.column=column;
        this.pagId=pagId;
    }

    public String getColumn() {
        return column;
    }

    public String getPagId() {
        return pagId;
    }
}
