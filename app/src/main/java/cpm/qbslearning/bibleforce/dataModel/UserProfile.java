package cpm.qbslearning.bibleforce.dataModel;

import android.content.Context;
import android.util.Log;

import cpm.qbslearning.bibleforce.helperMethods.PrefManager;

/**
 * Created by rmn on 20-09-2017.
 */

public class UserProfile {

    String firstName;
    String middleName;
    String lastName;
    String professionalHeadLine;
    String contactNo;
    String address;
    String city;
    String country;
    String email;
    String facebookId;
    String twitterId;
    String linkedInId;
    String otherId;
    String password;
    String dpUrl;
    String dpUrlLocal;
    Context mContext;
    PrefManager mPrefManager;

    public UserProfile(Context context) {
        mContext = context;
        mPrefManager = new PrefManager(context);
    }

    public void removeProfile(){
        setEmail("");
        setFirstName("");
        setMiddleName("");
        setProfessionalHeadLine("");
        setContactNo("");
        setAddress("");
        setCity("");
        setCountry("");
        setEmail("");
        setFacebookId("");
        setTwitterId("");
        setLinkedInId("");
        setOtherId("");
        setPassword("");
        setDpUrl("");
        setDpUrlLocal("");
     }

    public String getDpUrlLocal() {
        return mPrefManager.getSharedPref().getString("dpLocalUrl", "");
    }

    public void setDpUrlLocal(String dpUrlLocal) {
        mPrefManager.getEditor().putString("dpLocalUrl", dpUrlLocal).apply();
    }

    public void setFirstName(String firstName) {
        mPrefManager.getEditor().putString("firstName", firstName).apply();
    }

    public void setMiddleName(String middleName) {
        mPrefManager.getEditor().putString("middleName", middleName).apply();
    }

    public void setLastName(String lastName) {
        mPrefManager.getEditor().putString("lastName", lastName).apply();
    }

    public void setProfessionalHeadLine(String professionalHeadLine) {
        mPrefManager.getEditor().putString("professionalHeadLine", professionalHeadLine).apply();
    }

    public void setContactNo(String contactNo) {
        mPrefManager.getEditor().putString("contactNo", contactNo).apply();
    }

    public void setAddress(String address) {
        mPrefManager.getEditor().putString("address", address).apply();
    }

    public void setCity(String city) {
        mPrefManager.getEditor().putString("city", city).apply();
    }

    public void setCountry(String country) {
        mPrefManager.getEditor().putString("countryName", country).apply();
    }

    public void setEmail(String email) {
        mPrefManager.getEditor().putString("email", email).apply();
    }

    public void setFacebookId(String facebookId) {
        mPrefManager.getEditor().putString("facebookId", facebookId).apply();
    }

    public void setTwitterId(String twitterId) {
        mPrefManager.getEditor().putString("twitterId", twitterId).apply();
    }

    public void setLinkedInId(String linkedInId) {
        mPrefManager.getEditor().putString("linkedInId", linkedInId).apply();
    }

    public void setOtherId(String otherId) {
        mPrefManager.getEditor().putString("otherId", otherId).apply();
    }

    public void setPassword(String password) {
        mPrefManager.getEditor().putString("password", password).apply();
    }

    public void setDpUrl(String dpUrl) {
        mPrefManager.getEditor().putString("dpUrl", dpUrl).apply();
    }

    public String getFirstName() {
        return mPrefManager.getSharedPref().getString("firstName", "");
    }

    public String getMiddleName() {
        return mPrefManager.getSharedPref().getString("middleName", "");
    }

    public String getLastName() {
        return mPrefManager.getSharedPref().getString("lastName", "");
    }

    public String getProfessionalHeadLine() {
        return mPrefManager.getSharedPref().getString("professionalHeadLine", "");
    }

    public String getContactNo() {
        return mPrefManager.getSharedPref().getString("contactNo", "");
    }

    public String getAddress() {
        return mPrefManager.getSharedPref().getString("address", "");
    }

    public String getCity() {
        return mPrefManager.getSharedPref().getString("city", "");
    }

    public String getCountry() {
        if (!mPrefManager.getSharedPref().contains("country"))
            return "";
        Log.e("getCountry",mPrefManager.getSharedPref().getString("countryName", ""));
        return mPrefManager.getSharedPref().getString("countryName", "");
    }

    public String getEmail() {
        return mPrefManager.getSharedPref().getString("email", "");
    }

    public String getFacebookId() {
        return mPrefManager.getSharedPref().getString("facebookId", "");
    }

    public String getTwitterId() {
        return mPrefManager.getSharedPref().getString("twitterId", "");
    }

    public String getLinkedInId() {
        return mPrefManager.getSharedPref().getString("linkedInId", "");
    }

    public String getOtherId() {
        return mPrefManager.getSharedPref().getString("otherId", "");
    }

    public String getPassword() {
        return mPrefManager.getSharedPref().getString("password", "");
    }

    public String getDpUrl() {
        return mPrefManager.getSharedPref().getString("dpUrl", "");
    }
}
