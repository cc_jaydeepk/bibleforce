package cpm.qbslearning.bibleforce.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rmn on 27-09-2017.
 */

public class ForgetPasswordResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    /*
    @SerializedName("sessionToken")
    @Expose
    private String sessionToken;
    @SerializedName("userId")
    @Expose
    private String userId;*/

    @SerializedName("option")
    @Expose
    private String option;

    @SerializedName("apiVer")
    @Expose
    private String apiVer;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getApiVer() {
        return apiVer;
    }

    public void setApiVer(String apiVer) {
        this.apiVer = apiVer;
    }
}
