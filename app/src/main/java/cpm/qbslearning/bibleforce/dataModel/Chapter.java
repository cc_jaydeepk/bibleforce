package cpm.qbslearning.bibleforce.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rmn on 08-08-2017.
 */

public class Chapter {

    @SerializedName("chapterId")
    @Expose
    private String chapterId;
    @SerializedName("chapterName")
    @Expose
    private String chapterName;
    @SerializedName("chapterThumb")
    @Expose
    private String chapterThumb;
    @SerializedName("noOfPages")
    @Expose
    private String noOfPages;
    @SerializedName("testamentType")
    @Expose
    private String testamentType;
    @SerializedName("isPaid")
    @Expose
    private String isPaid;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("isActive")
    @Expose
    private String isActive;
    @SerializedName("chapterThumbImage")
    @Expose
    private String chapterThumbImage;
    @SerializedName("isFav")
    @Expose
    private String isFav;
    @SerializedName("isPurchased")
    @Expose
    private String isPurchased;
    @SerializedName("purchasedVia")
    @Expose
    private String purchasedVia;

    @SerializedName("isView")
    @Expose
    private String isView;
    @SerializedName("isDownloaded")
    @Expose
    private String isDownloaded;
    @SerializedName("chapterPages")
    @Expose
    private List<ChapterPage> chapterPages = null;


    public String getPurchasedVia() {
        return purchasedVia;
    }

    public void setPurchasedVia(String purchasedVia) {
        this.purchasedVia = purchasedVia;
    }

    public String getIsDownloaded() {
        return isDownloaded;
    }

    public String getIsView() {
        return isView;
    }

    public void setIsDownloaded(String isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

    public void setIsView(String isView) {
        this.isView = isView;
    }

    public String getChapterThumbImage() {
        return chapterThumbImage;
    }

    public String getIsActive() {
        return isActive;
    }

    public String getIsFav() {
        return isFav;
    }

    public String getIsPurchased() {
        return isPurchased;
    }

    public String getPrice() {
        return price;
    }

    public void setChapterThumbImage(String chapterThumbImage) {
        this.chapterThumbImage = chapterThumbImage;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public void setIsPurchased(String isPurchased) {
        this.isPurchased = isPurchased;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterThumb() {
        return chapterThumb;
    }

    public void setChapterThumb(String chapterThumb) {
        this.chapterThumb = chapterThumb;
    }

    public String getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(String noOfPages) {
        this.noOfPages = noOfPages;
    }

    public String getTestamentType() {
        return testamentType;
    }

    public void setTestamentType(String testamentType) {
        this.testamentType = testamentType;
    }

    public String getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(String isPaid) {
        this.isPaid = isPaid;
    }

    public List<ChapterPage> getChapterPages() {
        return chapterPages;
    }

    public void setChapterPages(List<ChapterPage> chapterPages) {
        this.chapterPages = chapterPages;
    }

}
