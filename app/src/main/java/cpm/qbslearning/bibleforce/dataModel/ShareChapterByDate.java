package cpm.qbslearning.bibleforce.dataModel;

import java.util.List;

/**
 * Created by rmn on 21-09-2017.
 */

public class ShareChapterByDate {
    String date;
    List<String> imageList;
    public ShareChapterByDate(String date,List<String> imageList){
        this.date=date;
        this.imageList=imageList;
    }

    public String getDate() {
        return date;
    }
    public List<String> getImageList() {
        return imageList;
    }

}
