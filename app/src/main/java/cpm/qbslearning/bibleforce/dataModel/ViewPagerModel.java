package cpm.qbslearning.bibleforce.dataModel;

import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingData;

public class ViewPagerModel {
    public String mTitle;
    public int mImageResourceId;
    public String mChapterID;

    public static List<ViewPagerModel> createSampleMemes(ArrayList<TrendingData> trendingData) {
        final List<ViewPagerModel> memes = new ArrayList<ViewPagerModel>();
        memes.add(new ViewPagerModel(trendingData.get(0).getChapterName(), R.drawable.bg_dashboard_img_one,trendingData.get(0).getChapterId()));
        memes.add(new ViewPagerModel(trendingData.get(1).getChapterName(), R.drawable.bg_dashboard_img_two,trendingData.get(1).getChapterId()));
        memes.add(new ViewPagerModel(trendingData.get(2).getChapterName(), R.drawable.bg_dashboard_img_three,trendingData.get(2).getChapterId()));
        return memes;
    }

    public ViewPagerModel(final String title, final int imageResourceId,final String chapterID) {
        mTitle = title;
        mImageResourceId = imageResourceId;
        mChapterID=chapterID;
    }
}
