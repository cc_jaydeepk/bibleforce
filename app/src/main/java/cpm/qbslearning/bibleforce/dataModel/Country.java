package cpm.qbslearning.bibleforce.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rmn on 21-09-2017.
 */

public class Country {

    @SerializedName("countryId")
    @Expose
    private String countryId;
    @SerializedName("userName")
    @Expose
    private String userName;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
