
package cpm.qbslearning.bibleforce.dataModel.tending;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrendingResponse {

    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sessionToken")
    @Expose
    private String sessionToken;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("option")
    @Expose
    private String option;
    @SerializedName("trendingData")
    @Expose
    private List<TrendingData> trendingData = null;
    @SerializedName("serverUrl")
    @Expose
    private String serverUrl;
    @SerializedName("apiVer")
    @Expose
    private String apiVer;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public List<TrendingData> getTrendingData() {
        return trendingData;
    }

    public void setTrendingData(List<TrendingData> trendingData) {
        this.trendingData = trendingData;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getApiVer() {
        return apiVer;
    }

    public void setApiVer(String apiVer) {
        this.apiVer = apiVer;
    }

    @Override
    public String toString() {
        return "TrendingResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", sessionToken='" + sessionToken + '\'' +
                ", userId='" + userId + '\'' +
                ", option='" + option + '\'' +
                ", trendingData=" + trendingData +
                ", serverUrl='" + serverUrl + '\'' +
                ", apiVer='" + apiVer + '\'' +
                '}';
    }
}
