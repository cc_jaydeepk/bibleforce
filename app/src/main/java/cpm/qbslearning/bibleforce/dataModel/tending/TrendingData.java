
package cpm.qbslearning.bibleforce.dataModel.tending;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrendingData {

    @SerializedName("chapterId")
    @Expose
    private String chapterId;
    @SerializedName("chapterName")
    @Expose
    private String chapterName;
    @SerializedName("chapterThumb")
    @Expose
    private String chapterThumb;
    @SerializedName("testamentType")
    @Expose
    private String testamentType;
    @SerializedName("trendingURL")
    @Expose
    private String trendingURL;

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterThumb() {
        return chapterThumb;
    }

    public void setChapterThumb(String chapterThumb) {
        this.chapterThumb = chapterThumb;
    }

    public String getTestamentType() {
        return testamentType;
    }

    public void setTestamentType(String testamentType) {
        this.testamentType = testamentType;
    }

    public String getTrendingURL() {
        return trendingURL;
    }

    public void setTrendingURL(String trendingURL) {
        this.trendingURL = trendingURL;
    }

    @Override
    public String toString() {
        return "TrendingData{" +
                "chapterId='" + chapterId + '\'' +
                ", chapterName='" + chapterName + '\'' +
                ", chapterThumb='" + chapterThumb + '\'' +
                ", testamentType='" + testamentType + '\'' +
                ", trendingURL='" + trendingURL + '\'' +
                '}';
    }
}
