package cpm.qbslearning.bibleforce.dataModel;

/**
 * Created by rmn on 08-08-2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ChapterPage {
        @SerializedName("pageId")
        @Expose
        private String pageId;
        @SerializedName("pageTitle")
        @Expose
        private String pageTitle;
        @SerializedName("audioName")
        @Expose
        private String audioName;
        @SerializedName("pageOrder")
        @Expose
        private String pageOrder;
        @SerializedName("url")
        @Expose
        private String url;

        public String getPageId() {
            return pageId;
        }

        public void setPageId(String pageId) {
            this.pageId = pageId;
        }

        public String getPageTitle() {
            return pageTitle;
        }

        public void setPageTitle(String pageTitle) {
            this.pageTitle = pageTitle;
        }

        public String getAudioName() {
            return audioName;
        }

        public void setAudioName(String audioName) {
            this.audioName = audioName;
        }

        public String getPageOrder() {
            return pageOrder;
        }

        public void setPageOrder(String pageOrder) {
            this.pageOrder = pageOrder;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

