package cpm.qbslearning.bibleforce.interfaceSet;

import cpm.qbslearning.bibleforce.dataModel.AccessCodeVerifyResponse;
import cpm.qbslearning.bibleforce.dataModel.ActionResponse;
import cpm.qbslearning.bibleforce.dataModel.AudioUploadResponse;
import cpm.qbslearning.bibleforce.dataModel.ChapterResponse;
import cpm.qbslearning.bibleforce.dataModel.DeleteAccountResponse;
import cpm.qbslearning.bibleforce.dataModel.ForgetPasswordResponse;
import cpm.qbslearning.bibleforce.dataModel.GetCountriesResponse;
import cpm.qbslearning.bibleforce.dataModel.LoginResponse;
import cpm.qbslearning.bibleforce.dataModel.PaymentResponse;
import cpm.qbslearning.bibleforce.dataModel.ProfilePicUpdateResponse;
import cpm.qbslearning.bibleforce.dataModel.ProfileRemoveResponse;
import cpm.qbslearning.bibleforce.dataModel.ProfileUpdateResponse;
import cpm.qbslearning.bibleforce.dataModel.SignupResponse;
import cpm.qbslearning.bibleforce.dataModel.UpdatePasswordResponse;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by rmn on 06-08-2017.
 */

public interface RetrofitInterface {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("email") String email, @Field("password") String password,
                              @Field("option") String option, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST("users")
    Call<PaymentResponse> sendPaymentId(@Field("sessionToken") String token, @Field("userId") String userId,
                                        @Field("option") String option, @Field("deviceId") String deviceId,
                                        @Field("deviceType") String deviceType,
                                        @Field("payToken") String payToken, @Field("payAmount") String payAmount,
                                        @Field("payCurrency") String payCurrency, @Field("chapterId") String chapterId
            , @Field("chapterName") String chapterName);

    @FormUrlEncoded
    @POST("users")
    Call<ChapterResponse> getChapters(@Field("sessionToken") String token, @Field("userId") String userId,
                                      @Field("option") String option, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST("chapters")
    Call<ActionResponse> markWatched(@Field("sessionToken") String token, @Field("userId") String userId,
                                     @Field("option") String option /*markViewed*/, @Field("deviceId") String deviceId
            , @Field("chapterId") String chapterId);

    @FormUrlEncoded
    @POST("chapters")
    Call<ActionResponse> markFavourite(@Field("sessionToken") String token, @Field("userId") String userId,
                                       @Field("option") String option, @Field("deviceId") String deviceId
            , @Field("chapterId") String chapterId);


    @FormUrlEncoded
    @POST("chapters")
    Call<ActionResponse> markUnFavourite(@Field("sessionToken") String token, @Field("userId") String userId,
                                         @Field("option") String option, @Field("deviceId") String deviceId
            , @Field("chapterId") String chapterId);


    @FormUrlEncoded
    @POST("chapters")
    Call<ActionResponse> markShared(@Field("sessionToken") String token, @Field("userId") String userId,
                                    @Field("option") String option, @Field("deviceId") String deviceId
            , @Field("chapterId") String chapterId);

    @Multipart
    @POST("chapters")
    Call<AudioUploadResponse> uploadAudio(@Part MultipartBody.Part file, @Part("sessionToken") RequestBody token, @Part("userId") RequestBody userId,
                                          @Part("option") RequestBody option, @Part("deviceId") RequestBody deviceId
            , @Part("chapterId") RequestBody chapterId, @Part("pageId") RequestBody pageId/*, @Part("file") String filePath*/);


    @FormUrlEncoded
    @POST("signup")
    Call<SignupResponse> signUp(@Field("signupFrom") String signupFrom, @Field("email") String email,
                                @Field("password") String password, @Field("deviceId") String deviceId
            , @Field("firstName") String firstName, @Field("lastName") String lastName
            , @Field("mobileNumber") String mobileNumber, @Field("deviceType") String deviceType, @Field("socialMediaId") String socialMediaId);

    @FormUrlEncoded
    @POST("users")
    Call<ProfileUpdateResponse> updateProfile(@Field("sessionToken") String token, @Field("userId") String userId,
                                              @Field("option") String option, @Field("deviceId") String deviceId,
                                              @Field("deviceType") String deviceType, @Field("firstName") String firstName,
                                              @Field("middleName") String middleName, @Field("lastName") String lastName,
                                              @Field("professionalHeadline") String professionalHeadline, @Field("address") String address,
                                              @Field("city") String city, @Field("country") String country,
                                              @Field("facebook") String facebook, @Field("linkedin") String linkedin,
                                              @Field("twitter") String twitter, @Field("other_link") String other_link
            , @Field("mobileNumber") String mobileNumber);

    @Multipart
    @POST("users")
    Call<ProfilePicUpdateResponse> updateProfilePic(@Part MultipartBody.Part file,
                                                    @Part("sessionToken") RequestBody token,
                                                    @Part("userId") RequestBody userId,
                                                    @Part("option") RequestBody option,
                                                    @Part("deviceId") RequestBody deviceId
            , @Part("deviceType") RequestBody deviceType);


    @FormUrlEncoded
    @POST("users")
    Call<ProfileRemoveResponse> removeProfile(@Field("sessionToken") String token, @Field("userId") String userId,
                                              @Field("option") String option, @Field("deviceId") String deviceId
            , @Field("deviceType") String deviceType);

    @FormUrlEncoded
    @POST("users")
    Call<DeleteAccountResponse> deleteAccount(@Field("deviceId") String deviceId, @Field("option") String option,
                                              @Field("sessionToken") String token, @Field("userId") String userId);

    @FormUrlEncoded
    @POST("users")
    Call<UpdatePasswordResponse> updatePassword(@Field("sessionToken") String token, @Field("userId") String userId,
                                                @Field("option") String option, @Field("deviceId") String deviceId
            , @Field("deviceType") String deviceType, @Field("currentPassword") String currentPassword
            , @Field("newPassword") String newPassword);

    @FormUrlEncoded
    @POST("users")
    Call<GetCountriesResponse> getCountries(@Field("sessionToken") String token, @Field("userId") String userId,
                                            @Field("option") String option, @Field("deviceId") String deviceId
            , @Field("deviceType") String deviceType);

    @FormUrlEncoded
    @POST("login")
    Call<ForgetPasswordResponse> forgotPassword(@Field("option") String option/*forgetPwd*/, @Field("email") String email);

    @FormUrlEncoded
    @POST("chapters")
    Call<AccessCodeVerifyResponse> verifyAccessCode(@Field("sessionToken") String token, @Field("userId") String userId,
                                                    @Field("option") String option /*checkAccessCode*/, @Field("deviceId") String deviceId
            , @Field("accessCode") String accessCode);

    /*@Multipart
    @POST("chapters")
    Call<AccessCodeVerifyResponse> verifyAccessCode(@Part("sessionToken") String token, @Part("userId") String userId,
                                                    @Part("option") String option *//*checkAccessCode*//*, @Part("deviceId") String deviceId
            , @Part("accessCode") String accessCode);*/


    @FormUrlEncoded
    @POST("chapters")
    Call<ActionResponse> downloadUsingAccessCode(@Field("sessionToken") String token, @Field("userId") String userId,
                                                 @Field("option") String option /*markedAllChaptersDownload*/, @Field("deviceId") String deviceId);
    /*@Multipart
    @POST("chapters")
    Call<ActionResponse> downloadUsingAccessCode(@Part("sessionToken") String token, @Part("userId") String userId,
                                                 @Part("option") String option *//*markedAllChaptersDownload*//*, @Part("deviceId") String deviceId);*/

    @FormUrlEncoded
    @POST("chapters")
    Call<TrendingResponse> getTrendingChapters(@Field("sessionToken") String token, @Field("userId") String userId,
                                               @Field("option") String option, @Field("deviceId") String deviceId);

}
