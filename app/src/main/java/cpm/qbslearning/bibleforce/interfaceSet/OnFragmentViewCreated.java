package cpm.qbslearning.bibleforce.interfaceSet;

import android.view.View;

import androidx.fragment.app.Fragment;

public interface OnFragmentViewCreated {
    void onBindView(Fragment fragment, View view);
}
