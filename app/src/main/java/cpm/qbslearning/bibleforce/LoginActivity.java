package cpm.qbslearning.bibleforce;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import cpm.qbslearning.bibleforce.dataModel.ForgetPasswordResponse;
import cpm.qbslearning.bibleforce.dataModel.LoginResponse;
import cpm.qbslearning.bibleforce.dataModel.UserProfile;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import io.alterac.blurkit.BlurLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.util.CommonMethods.hideSoftKeyboard;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isEmailValid;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isNetworkAvailable;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isPasswordValid;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, CommonMethods.CommonMethodsCallback
        , CommonMethods.ActionCallback {
    private static final int RC_SIGN_IN = 1;
    EditText mEmailView, mPasswordView;
    ProgressBar mProgressBar;
    public static final String BASE_URL = Constants.ACTION_BASE_URL;
    PrefManager mPrefManager;
    SharedPreferences.Editor mEditor;
    String deviceId;
    private static final String EMAIL = "email";
    //    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions mGso;
    /*private CallbackManager callbackManger;
    private AccessTokenTracker accessTokenTracker;
    private AccessToken accessToken;*/
    LinearLayout linearLayout;
    RelativeLayout rootLinearLayout;
    KProgressHUD hud;
    /*SignInButton googleButton;*/
//    private LoginButton btnFacebookLogin;
    /*private Button btnFacebookLogin;*/
//    private CallbackManager callbackManager;
    BlurLayout blurLayout;
    ImageView ivLoginPulsEffect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen_new);
        mEmailView = (EditText) findViewById(R.id.v_email);
        mPasswordView = (EditText) findViewById(R.id.v_pass);
        blurLayout = findViewById(R.id.blurLayout);
        ivLoginPulsEffect=findViewById(R.id.bibl);
//        btnFacebookLogin = findViewById(R.id.fb_login_button);

        /*Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse_effect);
        ivLoginPulsEffect.startAnimation(pulse);*/
/*
        RotateAnimation anim = new RotateAnimation(30, 90, ivLoginPulsEffect.getWidth(), ivLoginPulsEffect.getHeight());
        anim.setFillAfter(true);
        anim.setRepeatCount(99999999);
        anim.setDuration(10000);
        ivLoginPulsEffect.startAnimation(anim);*/
        /*ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.flipping);
        anim.setTarget(ivLoginPulsEffect);
        anim.setRepeatCount(123256464);
        anim.setDuration(4000);
        anim.start();*/
        /*ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(ivLoginPulsEffect,
                PropertyValuesHolder.ofFloat("scaleX", 0.2f),
                PropertyValuesHolder.ofFloat("scaleY", 0.2f));
        scaleDown.setDuration(1000);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();*/

        Glide.with(this)
                .load(R.raw.logo_animation)
                .into(ivLoginPulsEffect);

        mProgressBar = new ProgressBar(this);
        mPrefManager = new PrefManager(this);
        mEditor = mPrefManager.getEditor();

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        linearLayout = (LinearLayout) findViewById(R.id.parent);
        setupUI(linearLayout);
        rootLinearLayout = (RelativeLayout) findViewById(R.id.root);
        setupUI(rootLinearLayout);

//        googleButton = (SignInButton) findViewById(R.id.google_sign_in_button);

        mGso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, mGso)
                .build();*/
        /*FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManger = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
            }
        };
        accessToken = AccessToken.getCurrentAccessToken();*/

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "cpm.qbslearning.bibleforce",
                    PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        /*LoginManager.getInstance().registerCallback(callbackManger, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);
                hud.show();
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        getFacebookData(object, loginResult.getAccessToken().getUserId());
                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("fields", "id, first_name, last_name, email");
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                if (hud.isShowing()) {
                    hud.dismiss();
                }
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });*/

        /*btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                CommonMethods.showAlert(getString(R.string.app_name),"Test Message","Yes","No",LoginActivity.this,LoginActivity.this);
                CommonMethods.showAlert(getString(R.string.app_name),getString(R.string.facebook_login_alert),getString(R.string.btn_ok),getString(R.string.btn_cancel),LoginActivity.this,LoginActivity.this);
//                faceBookLogin();
            }
        });*/


//        accessTokenTracker.startTracking();
        /*googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleLoginButton();
            }
        });*/
//        emailUserIdAlert();
    }

    public void signUp(View v) {
        startActivity(new Intent(this, SignUpActivity.class));
       // finish();
    }

    public void Login(View v) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mProgressBar.setVisibility(View.VISIBLE);

        if (CommonMethods.isNetworkAvailable(this)) {
            loginProcess(mEmailView.getText().toString(), mPasswordView.getText().toString(), deviceId);
        } else {
            Toast.makeText(this, "Internet not connected", Toast.LENGTH_SHORT).show();
        }
    }

    /*public void googleLoginButton() {

        hud.show();
        Log.e("google login", "called");
        if (isNetworkAvailable(this)) {
            Intent googleSignInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(googleSignInIntent, RC_SIGN_IN);
        } else {
            Toast.makeText(this, "Internet Not Connected", Toast.LENGTH_SHORT).show();
        }
    }*/

    /*public void googleLogin(View view) {
        Log.e("google login", "called");
        if (isNetworkAvailable(this)) {
            Intent googleSignInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(googleSignInIntent, RC_SIGN_IN);
        } else {
            Toast.makeText(this, "Internet Not Connected", Toast.LENGTH_SHORT).show();
        }
    }*/


    /*public void facebookLogin(View v) {
        Log.e("facebook", "called");
        LoginManager.getInstance().logInWithReadPermissions(
                LoginActivity.this,
                Arrays.asList("email")
        );
//        CommonMethods.showAlert(getString(R.string.app_name),getString(R.string.facebook_login_alert),getString(R.string.btn_ok),getString(R.string.btn_cancel),LoginActivity.this,LoginActivity.this);
//        return;
    }*/

    /*private void faceBookLogin() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(EMAIL,"public_profile"));

        *//*btnFacebookLogin.setPermissions(Arrays.asList(EMAIL, "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        btnFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                *//**//*AccessToken accessToken = AccessToken.getCurrentAccessToken();
                boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
                Log.e(Constants.TAG, "facebook " + String.valueOf(isLoggedIn));
                Log.e(Constants.TAG, "facebook " + loginResult.getAccessToken().getUserId());
                getUserProfile(accessToken);*//**//*

                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);
                hud.show();
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        getFacebookData(object, loginResult.getAccessToken().getUserId());
                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("fields", "id, first_name, last_name, email");
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                if (hud.isShowing()) {
                    hud.dismiss();
                }
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                exception.printStackTrace();
            }
        });*//*
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            Log.e("facebook", "resultcode " + resultCode);
            hud.show();
            callbackManger.onActivityResult(requestCode, resultCode, data);
        }*/
    }

    /*private void handleSignInResult(GoogleSignInResult result) {
        hud.setLabel("Wait a while...");

        hud.setCancellable(true);
        Log.e("Handle sign result", result.getStatus() + "");
        Log.e("Handle sign result", result.isSuccess() + "");
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String name = acct.getDisplayName();
            String email = acct.getEmail();
            String userIdToken = acct.getIdToken();
            String id = acct.getId();
            Log.e("name", name + ", email " + email + ",userIdToken  " + userIdToken + " ,id " + id);
            String[] nameArray = name.split(" ");
            String fName = nameArray[0];
            String lName = "";
            if (nameArray.length > 1)
                lName = nameArray[1];
            hud.show();
            if (isNetworkAvailable(this)) {
                List<Boolean> status = CommonMethods.signUp(LoginActivity.this, this, "3", email, "", deviceId, fName, lName, "", id);
                // if (status.size()>0)

            } else {
                hud.dismiss();
                Toast.makeText(this, "Internet Not Connected", Toast.LENGTH_SHORT).show();
            }
        } else {
            hud.dismiss();
            Log.e("handleSignResult", "Logout");
        }
    }*/

    public void loginProcess(String email, String password, String deviceId) {
        if (isEmailValid(email) && isPasswordValid(password)) {

            hud.setLabel("Logging..");
            hud.show();

            Call<LoginResponse> responseCall = RetrofitClient.getRetrofitClient(BASE_URL)
                    .create(RetrofitInterface.class).login(email, password, "login", deviceId);
            Log.e("email", email);
            Log.e("password", password);
            Log.e("dId", deviceId);
            responseCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        LoginResponse loginResponse = response.body();
                        Log.e("loginResponse", loginResponse.getStatus() + "");
                        Log.e("loginResponse", loginResponse.getMessage() + "");
                        if (loginResponse.getStatus() > 0) {
                            UserProfile userProfile = new UserProfile(LoginActivity.this);
                            Log.e("uid", loginResponse.getUserId());
                            mPrefManager.getEditor().putString(getString(R.string.uId), loginResponse.getUserId()).apply();
                            mPrefManager.getEditor().putString(getString(R.string.token), loginResponse.getSessionToken()).apply();
                            mPrefManager.getEditor().putString(getString(R.string.login_type), "1").apply();
                            userProfile.setFirstName(loginResponse.getFirstName());
                            userProfile.setMiddleName(loginResponse.getMiddleName());
                            userProfile.setLastName(loginResponse.getLastName());
                            userProfile.setProfessionalHeadLine(loginResponse.getProfessionalHeadline());
                            userProfile.setContactNo(loginResponse.getMobileNumber());
                            userProfile.setAddress(loginResponse.getAddress());
                            userProfile.setCity(loginResponse.getCity());
                            userProfile.setCountry(loginResponse.getCountry());
                            userProfile.setEmail(loginResponse.getEmail());
                            userProfile.setFacebookId(loginResponse.getFacebook());
                            userProfile.setTwitterId(loginResponse.getTwitter());
                            userProfile.setLinkedInId(loginResponse.getLinkedin());
                            userProfile.setOtherId(loginResponse.getOtherLink());
                            userProfile.setDpUrl(loginResponse.getServerUrl() + loginResponse.getProfilePic());
                            mEditor.apply();
                            CommonMethods.fetchChapter(LoginActivity.this, LoginActivity.this);
                            //goToNextActivity(LoginActivity.this);
                        } else {
                            mEmailView.setError("Wrong Email");
                            mPasswordView.setError("Wrong Password");
                            hud.dismiss();
//                            Toast.makeText(LoginActivity.this, "wrong user name or password", Toast.LENGTH_SHORT).show();
                            if (!TextUtils.isEmpty(loginResponse.getMessage())) {
                                Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LoginActivity.this, "wrong user name or password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    call.cancel();
                    mProgressBar.setVisibility(View.GONE);
//                    if (t!=null)
//                        Log.e("error",t.getMessage());
                    Log.e("error", "failure");
                    hud.dismiss();
                    Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            if (!isEmailValid(email))
                mEmailView.setError(getResources().getString(R.string.invalid_email));
            if (!isPasswordValid(password))
                mPasswordView.setError(getResources().getString(R.string.invalid_pass));
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void goToNextActivity() {

        hud.show();
        if (mPrefManager.getSharedPref().getString(getString(R.string.dId), null) != null) {
            startActivity(new Intent(this, DashboardActivity.class));
            hud.dismiss();

        } else {
            mEditor.putString(getString(R.string.dId), deviceId).apply();
            startActivity(new Intent(this, DemoActivity.class));
            hud.dismiss();
        }
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("connection error", connectionResult.getErrorMessage() + " ");
        Toast.makeText(LoginActivity.this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

//    public void logout(){
//        LoginManager.
//    }

    private Bundle getFacebookData(JSONObject object, String id) {

        String fName = "", lName = "", email = "";

        try {
//            try {
//                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
//                Log.i("profile_pic", profile_pic + "");
//                bundle.putString("profile_pic", profile_pic.toString());
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//                return null;
//            }
            if (object.has("first_name"))
                fName = object.getString("first_name");
            if (object.has("last_name"))
                lName = object.getString("last_name");
            if (object.has("email"))
                email = object.getString("email");
            if (object.has("email"))
                email = object.getString("email");

            Log.e("facebook userId", id + "   ");

            CommonMethods.signUp(LoginActivity.this, this, "2", email, "", deviceId, fName, lName, "", id);

        } catch (JSONException e) {
            Log.d("LOGIN_ACTIVITY", "Error parsing JSON");
        }
        return null;
    }

    public void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(LoginActivity.this);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void forgotPassword(View v) {
        // if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

        View view = LayoutInflater.from(this).inflate(R.layout.forgot_password_view, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setView(view);
        final TextView msgView = view.findViewById(R.id.v_message);
        final TextInputEditText emailView = view.findViewById(R.id.v_email);
        final Button cancelView = view.findViewById(R.id.v_cancel);
        final Button submitView = view.findViewById(R.id.v_submit);

        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submitView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressBar.setVisibility(View.VISIBLE);
                hud.show();

                if (CommonMethods.isEmailValid(emailView.getText().toString().trim())) {

                    if (CommonMethods.isNetworkAvailable(LoginActivity.this)) {
                        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(BASE_URL).create(RetrofitInterface.class);
                        Call<ForgetPasswordResponse> call = retrofitInterface.forgotPassword("forgetPwd", emailView.getText().toString());
                        call.enqueue(new Callback<ForgetPasswordResponse>() {
                            @Override
                            public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                                ForgetPasswordResponse passwordResponse = response.body();
                                Log.e("response", response.message() + "  ");
                                msgView.setText(passwordResponse.getMessage());
                                mProgressBar.setVisibility(View.GONE);
                                hud.dismiss();
                                if (passwordResponse.getStatus() == 1) {
                                    dialog.dismiss();
                                    CommonMethods.showAlert("Sucess!", passwordResponse.getMessage(), "Ok", null, null, LoginActivity.this);
//                                    Toast.makeText(LoginActivity.this, passwordResponse.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                                mProgressBar.setVisibility(View.GONE);
                                hud.dismiss();
                            }

                        });
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "Internet not available", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // msgView.setText("Please Enter Valid Email Address");
                    emailView.setError("Please Enter Valid Email Address");
                    mProgressBar.setVisibility(View.GONE);
                    hud.dismiss();
                }

            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        accessTokenTracker.stopTracking();
    }

    @Override
    public void callbackFromCommonMethods(boolean resultOk) {
        hud.show();
        Log.e("callFromCommonMethods", "called");
        Log.e("resultOk", resultOk + "");
        if (resultOk) {
            if (!mPrefManager.getSharedPref().getBoolean("country", false)) {
                Log.e("getCountry", "list called");
                CommonMethods.getCountryList(LoginActivity.this, LoginActivity.this);
                hud.dismiss();
            } else {
                Log.e("goToNextActivity", " called");
                goToNextActivity();
            }
        } else {
            logoutFromAllInstance();
            hud.dismiss();
        }
    }

    public void logoutFromAllInstance() {

        PrefManager manager = new PrefManager(this);
        UserProfile userProfile = new UserProfile(this);
        manager.getEditor().remove(getString(R.string.uId)).apply();
        manager.getEditor().remove(getString(R.string.token)).apply();
        String loginType = manager.getSharedPref().getString(getString(R.string.login_type), null);

        /*Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
                AccessToken.setCurrentAccessToken(null);
                Log.e("facebook ", "logout");

            }
        }).executeAsync();*/

        userProfile.removeProfile();
    }

    public void emailUserIdAlert() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this, R.style.AlertDialogTheme);
        dialog.setCancelable(false);
        dialog.setTitle("Alert");
        dialog.setMessage("Email/user id must be the same as the one used to access the code!");

        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
            }
        });
        final AlertDialog alert = dialog.create();
        alert.show();
    }

    @Override
    public void onPositiveBtnClick() {
        /*faceBookLogin();*/
    }

    @Override
    public void onNegativeBtnClick() {

    }

    /**
     * @param title              String title for the Dialog
     * @param message            String message for the Dialog
     * @param positiveButtonText String text for the positive button
     * @param negativeButtonText String text for the positive button
     * @param callback           {@link ActionCallback} callbacks for positive and negative button clicks
     */
    /*private final void showAlert(String title, String message, String positiveButtonText, String negativeButtonText, final ActionCallback callback) {
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this).setTitle(title).setMessage(message)
                .setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (callback != null) {
                            callback.onPositiveBtnClick();
                        }
                    }
                });
        if (negativeButtonText != null) {
            alertDialogBuilder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (callback != null) {
                        callback.onNegativeBtnClick();
                    }
                }
            });
        }

        android.app.AlertDialog alertDialog = alertDialogBuilder.show();

        alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
        alertDialog.getButton(android.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(android.R.color.darker_gray));
    }

    *//**
     * Interface used to throw callback for alert dialogs positive and negative
     * buttons via {@link ActionCallback#onPositiveBtnClick()} and {@link ActionCallback#onNegativeBtnClick()} ()}
     *//*
    protected interface ActionCallback {
        void onPositiveBtnClick();

        void onNegativeBtnClick();

    }*/

    @Override
    protected void onStart() {
        super.onStart();
        blurLayout.startBlur();
        blurLayout.lockView();
    }

    @Override
    protected void onStop() {
        blurLayout.pauseBlur();
        super.onStop();
    }
}