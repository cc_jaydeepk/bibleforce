package cpm.qbslearning.bibleforce;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import cpm.qbslearning.bibleforce.adapter.ViewPagerAdapter;
import me.relex.circleindicator.CircleIndicator;

public class DemoActivity extends AppCompatActivity {

    ViewPager mViewPager;
    LinearLayout mLinearLayout;
    Button mNextButton, mSkipButton;
    ViewPagerAdapter mPagerAdapter;
    TextView[] dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_demo);

        mViewPager = (ViewPager) findViewById(R.id.v_pager);
        mLinearLayout = (LinearLayout) findViewById(R.id.v_layoutDots);
        mSkipButton = (Button) findViewById(R.id.v_btn_skip);
        mNextButton = (Button) findViewById(R.id.v_btn_next);

        mPagerAdapter = new ViewPagerAdapter(this, mLinearLayout, 3);

        mViewPager.setAdapter(mPagerAdapter);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                dots = new TextView[3];
                for (int i = 0; i < 3; i++) {
                    dots[i] = new TextView(DemoActivity.this);
                    dots[i].setBackgroundResource(R.drawable.light_dot);
                    mLinearLayout.addView(dots[i]);
                }
                if (dots.length > 0)
                    dots[position].setBackgroundResource(R.drawable.dark_dot);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < 3) {
                    // move to next screen
                    mViewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });

        mSkipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchHomeScreen();
            }
        });
    }

    private void launchHomeScreen() {
        //startActivity(new Intent(this,AccessCodeActivity.class));
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        // startActivity(new Intent(this, DashboardActivity.class));
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

}
