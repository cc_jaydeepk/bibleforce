package cpm.qbslearning.bibleforce;

import android.database.Cursor;
import android.os.Handler;
import android.os.Message;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.service.ChapterDownloadService;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.TESTAMENT_TYPE;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.AUDIO_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.CHAPTER_PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IMAGE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_AUDIO;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_IMAGE;

public class TestActivity extends AppCompatActivity {

    MyHandler handler;
    String  mCId;
    private static final int CALLER = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        handler=new MyHandler();
        dataOperation();
    }

    public void printData(View view){
        printChapter(mCId);
    }


    public void dataOperation(){

        Cursor cursor=getContentResolver().query(DbContract.Chapter.CONTENT_URI,new String[]{CHAPTER_ID,TESTAMENT_TYPE},null,null,null);
        if (cursor!=null){
            cursor.moveToFirst();
            String cId=cursor.getString(cursor.getColumnIndex(CHAPTER_ID));
            Log.e("cId",cId);
            mCId=cId;
            int testamentType=cursor.getInt(cursor.getColumnIndex(TESTAMENT_TYPE));
            cursor.close();
            printChapter(cId);
            ChapterDownloadService.startChapterDownloading(this,handler,cId,1,CALLER);
        }
    }

    public void printChapter(String cId){

        Cursor pageCursor=getContentResolver().query(DbContract.ChapterPage.CONTENT_URI,null,CHAPTER_PAGE_ID+"=?",new String[] {cId},null );
        if (pageCursor!=null){
            int i=0;
            while (pageCursor.moveToNext()){

                String pageId=pageCursor.getString(pageCursor.getColumnIndex(PAGE_ID));
                String chapterPageId=pageCursor.getString(pageCursor.getColumnIndex(CHAPTER_PAGE_ID));
                String audio=pageCursor.getString(pageCursor.getColumnIndex(AUDIO_URL));
                String image=pageCursor.getString(pageCursor.getColumnIndex(IMAGE_URL));
                String imageThumb=pageCursor.getString(pageCursor.getColumnIndex(PAGE_IMAGE));
                String audioPlay=pageCursor.getString(pageCursor.getColumnIndex(PAGE_AUDIO));
                Log.e("pagedata",i+" : "+pageId);
                Log.e("pagedata",i+" : "+chapterPageId);
                Log.e("pagedata",i+" : "+audio);
                Log.e("pagedata",i+" : "+audioPlay);
                Log.e("pagedata",i+" : "+image);
                Log.e("pagedata",i+" : "+imageThumb);
                i++;
            }
            pageCursor.close();
        }
    }

    public class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.getData().getInt("data")==1){
                printChapter(msg.getData().getString("cId"));
            }
        }
    }

}
