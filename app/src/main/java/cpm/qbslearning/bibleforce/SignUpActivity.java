package cpm.qbslearning.bibleforce;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.List;

import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.util.CommonMethods;

import static cpm.qbslearning.bibleforce.util.CommonMethods.hideSoftKeyboard;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isEmailValid;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isNameValid;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isNetworkAvailable;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isPasswordValid;

public class SignUpActivity extends AppCompatActivity implements CommonMethods.CommonMethodsCallback {

    public EditText firstNameView, lastNameView, emailView, passwordView, cnfrmPassView;
    public KProgressHUD mProgressDialog;
    PrefManager mPrefManager;
    SharedPreferences.Editor mEditor;
    String deviceId;
    LinearLayout linearLayout, llSignupBack;
    RelativeLayout rootFrameLayout;
    ImageView ivLoginPulsEffect;
    TextView txt_privacypolicy;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        firstNameView = (EditText) findViewById(R.id.v_f_name);
        lastNameView = (EditText) findViewById(R.id.v_l_name);
        emailView = (EditText) findViewById(R.id.v_email);
        passwordView = (EditText) findViewById(R.id.v_pass);
        cnfrmPassView = (EditText) findViewById(R.id.v_cnfrm_pass);
        ivLoginPulsEffect = findViewById(R.id.bibl);
        llSignupBack = findViewById(R.id.ll_signup_back);



        txt_privacypolicy = findViewById(R.id.txt_privacypolicy);
        txt_privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog myDialog = new Dialog(SignUpActivity.this);
                myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                myDialog.setContentView(R.layout.privacy_policy_dialog);
                myDialog.setCancelable(false);
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();
                Button yes = (Button) myDialog.findViewById(R.id.okBtn);
                TextView policyText = (TextView) myDialog.findViewById(R.id.content);

                policyText.setText(Html.fromHtml(getString(R.string.text_with_colored_underline)));
                Button no = (Button) myDialog.findViewById(R.id.cancelBtn);
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

            }
        });
/*
        ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.flipping);
        anim.setTarget(ivLoginPulsEffect);
        anim.setRepeatCount(123256464);
        anim.setDuration(4000);
        anim.start();*/
        Glide.with(this)
                .load(R.raw.logo_animation)
                .into(ivLoginPulsEffect);

        mProgressDialog = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Signup...")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        linearLayout = (LinearLayout) findViewById(R.id.parent);
        rootFrameLayout = (RelativeLayout) findViewById(R.id.root);
        mPrefManager = new PrefManager(this);
        mEditor = mPrefManager.getEditor();

        setupUI(linearLayout);
        setupUI(rootFrameLayout);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        llSignupBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//        emailUserIdAlert();
    }

    public void onSignUp(View view) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        String firstName = firstNameView.getText().toString();
        String lastName = lastNameView.getText().toString();
        String email = emailView.getText().toString();
        String mobile = "";
        String pass = passwordView.getText().toString();
        String cnfrmPass = cnfrmPassView.getText().toString();

        Log.e("firstName", firstName);
        Log.e("lastName", lastName);
        Log.e("email", email);
        Log.e("mobile", mobile);
        Log.e("pass", pass);
        Log.e("cnfrmPass", cnfrmPass);

        int e = 0, f = 0, l = 0, m = 0, p = 0, c = 0;
        if (isNameValid(firstName))
            f = 1;
        else
            firstNameView.setError(getResources().getString(R.string.invalid_name));

        if (isNameValid(lastName))
            l = 1;
        else
            lastNameView.setError(getResources().getString(R.string.invalid_name));

        if (isEmailValid(email))
            e = 1;
        else
            emailView.setError(getResources().getString(R.string.invalid_email));

        //if (isMobileValid(mobile))
        m = 1;

        if (isPasswordValid(pass))
            p = 1;
        else
            passwordView.setError(getResources().getString(R.string.invalid_pass));

        if (pass.equals(cnfrmPass))
            c = 1;
        else
            cnfrmPassView.setError(getResources().getString(R.string.invalid_cnfrm_pass));
        Log.e("onSignup", (f + l + e + m + p + c) + "");
        if ((f + l + e + m + p + c) == 6) {

            if (isNetworkAvailable(this)) {
                mProgressDialog.show();
                deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                List<Boolean> status = CommonMethods.signUp(SignUpActivity.this, this, "1", email, pass, deviceId, firstName, lastName, mobile, "");

                // mProgressDialog.dismiss();
                Log.e("iuo", "jkhkj");
            } else {
                Toast.makeText(this, "Internet Not Connected", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void backToLogin(View v) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume", "called");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("signup_action");
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("onPause", "called");
        unregisterReceiver(receiver);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(SignUpActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void goToNextActivity() {
        /*if (mPrefManager.getSharedPref().getString(getString(R.string.dId), null) != null) {
            startActivity(new Intent(this, DashboardActivity.class));
        } else {*/
        mEditor.putString(getString(R.string.dId), deviceId).apply();
        startActivity(new Intent(this, DemoActivity.class));
        // }
        finish();
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra("msg");
            Log.e("msg1", msg + "");
            if (msg.equals("done")) {
                Log.e("msg2", "done");
            }
        }
    };

    @Override
    public void callbackFromCommonMethods(boolean resultOk) {
        Log.e("callFromCommonMethods", "called");
        Log.e("resultOk", resultOk + "");
        if (resultOk) {
            if (!mPrefManager.getSharedPref().getBoolean("country", false)) {
                Log.e("getCountry", "list called");
                CommonMethods.getCountryList(SignUpActivity.this, SignUpActivity.this);
            } else {
                mProgressDialog.dismiss();
                Log.e("goToNextActivity", " called");
                goToNextActivity();
            }
        } else {
            mProgressDialog.dismiss();
//            Toast.makeText(SignUpActivity.this, "Something went wrong \n please try again later", Toast.LENGTH_SHORT).show();
        }
    }

    public void emailUserIdAlert() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(SignUpActivity.this, R.style.AlertDialogTheme);
        dialog.setCancelable(false);
        dialog.setTitle("Alert");
        dialog.setMessage("Email/user id must be the same as the one used to access the code!");

        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        final AlertDialog alert = dialog.create();
        alert.show();
    }
}