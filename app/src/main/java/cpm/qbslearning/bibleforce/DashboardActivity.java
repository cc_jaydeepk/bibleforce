package cpm.qbslearning.bibleforce;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import cpm.qbslearning.bibleforce.crausal.CarouselPagerAdapter;
import cpm.qbslearning.bibleforce.data.DbHelper;
import cpm.qbslearning.bibleforce.dataModel.UserProfile;
import cpm.qbslearning.bibleforce.fragment.AboutFragment;
import cpm.qbslearning.bibleforce.fragment.DashBoardFragemnt;
import cpm.qbslearning.bibleforce.fragment.FavouriteFragment;
import cpm.qbslearning.bibleforce.fragment.ProfileFragment;
import cpm.qbslearning.bibleforce.fragment.SettingFragment;
import cpm.qbslearning.bibleforce.fragment.ShareFragment;
import cpm.qbslearning.bibleforce.fragment.TestamentFragment;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.OnFragmentViewCreated;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import io.fabric.sdk.android.Fabric;

//ProfileFragment.onProfileFragmentListener
public class DashboardActivity extends AppCompatActivity
        implements DashBoardFragemnt.OnFragmentInteractionListener, TestamentFragment.OnTestamentFragmentInteractionListener,
        AboutFragment.OnAboutFragmentInteractionListener, SettingFragment.OnSettingFragmentInteractionListener,
        ProfileFragment.OnProfileFragmentInteractionListener, FavouriteFragment.OnFavouriteFragmentInteractionListener, ShareFragment.OnShareFragmentInteractionListener, ProfileFragment.onProfileFragmentListener, TestamentFragment.OnChildFragmentCallback, OnFragmentViewCreated {

    RelativeLayout dashboardView, logoutView;
    ImageView oldTestamentView, newTestamentView;
    public final static int LOOPS = 1000;
    public CarouselPagerAdapter adapter;
    public ViewPager pager;
    public static int FIRST_PAGE = 3;
    public static int count = 3;
    DbHelper mHelper;
    ProgressDialog dialog;
    GoogleApiClient mGoogleApiClient;
    PrefManager manager;
    private GoogleSignInOptions mGso;
    static boolean mIsTestamentAdded = false, isWebOpen = false;
    static boolean mIsDashboard = true;
    private LinearLayout rootView;
    Bundle mSavedInstanceState;
    private int mTestamentType = 0;
    private static boolean isSettingChildAdded = false;
    boolean isMenu = false;
    public DrawerLayout drawerLayout;
    public FrameLayout headerLayout;
    public ImageView profileImageView;
    public TextView profileEmailTextview, profileNameTextview;
    public UserProfile userProfile;


    //DashBoardFragemnt mBlueFragment;
    ProfileFragment mGreenFragment;
    //public ImageView fragmentTextView;
    ImageView mLogo;
    DashBoardFragemnt fragment;
    DashBoardFragemnt dashBoardFragemnt;
    public ImageView ivNavHeaderDp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fabric.with(this, new Crashlytics());
        super.onCreate(savedInstanceState);
        mSavedInstanceState = savedInstanceState;
        setContentView(R.layout.main_activity);
        Log.e("onCreate A ", "called");
        rootView = (LinearLayout) findViewById(R.id.root);
        CommonMethods.setupUI(this, rootView);

        userProfile = new UserProfile(this);

        mLogo = findViewById(R.id.iv_nav_header_dp);
        //  mLogo.setVisibility(View.GONE);

        /*dashBoardFragemnt = new DashBoardFragemnt();
        View view1 = dashBoardFragemnt.getView();
        if (view1 != null) {
            ivNavHeaderDp = view1.findViewById(R.id.iv_nav_header_dp);
        }*/

        if (getIntent() != null) {
            Log.e("activity", "intent not null");
            mIsTestamentAdded = getIntent().getBooleanExtra(Constants.EXTRA_IS_TESTAMENT_ADDED, false);
            mTestamentType = getIntent().getIntExtra(Constants.EXTRA_TESTAMENT_TYPE, -1);
        }
        Log.e("mTestamentType", mTestamentType + "");
        if (savedInstanceState == null) {
            Log.e("savedInstance ", "not null " + mIsTestamentAdded);
            if (!mIsTestamentAdded) {
                Fragment fragment = DashBoardFragemnt.newInstance("a", "b");
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.container, fragment);
                mIsDashboard = true;
                mIsTestamentAdded = false;
                transaction.commit();
            } else {
                Fragment fragment = DashBoardFragemnt.newInstance("1", Integer.toString(mTestamentType));
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.commit();
                mIsDashboard = false;
                mIsTestamentAdded = true;
            }
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .detectAll()
//                .penaltyLog()
                .build();
//        StrictMode.setThreadPolicy(policy);

    }

    @Override
    public void onBindView(Fragment fragment, View view) {
        if (fragment instanceof DashBoardFragemnt) {
            // here i know that i sent a TextView
            ivNavHeaderDp = ((ImageView) view);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume A", "called");
    }

    @Override
    protected void onPause() {
        Log.e("onPause A", "called");
        super.onPause();
    }

    @Override
    public void onBackPressed() {

        Log.e("mIsTestamentAdded", mIsTestamentAdded + "  " + mIsDashboard + "   " + isMenu);
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }

        Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("a");

        if (fragment1 instanceof AboutFragment) {
            Log.e("about fragment", "called");
            ((AboutFragment) fragment1).onButtonPressed(null);
        }

        if (isWebOpen) {
            isWebOpen = false;
            return;
        }

        if (isMenu) {
            if (mIsTestamentAdded) {
                Fragment fragment = DashBoardFragemnt.newInstance("1", Integer.toString(mTestamentType));
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.commit();
                handleSettingChildFragment();
                mIsTestamentAdded = true;
                mIsDashboard = false;
                isMenu = false;
                return;
            } else {
                Fragment fragment = DashBoardFragemnt.newInstance("a", "b");
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.commit();
                handleSettingChildFragment();
                mIsTestamentAdded = false;
                mIsDashboard = true;
                isMenu = false;
                return;
            }
        } else {
            if (mIsTestamentAdded) {
                Fragment fragment = DashBoardFragemnt.newInstance("a", "b");
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.commit();
                mIsDashboard = true;
                mIsTestamentAdded = false;
                handleSettingChildFragment();
            } else {
                if (!mIsDashboard) {
                    Fragment fragment = DashBoardFragemnt.newInstance("a", "b");
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, fragment);
                    transaction.commit();
                    mIsDashboard = true;
                    // addDashboardFragment();
                    handleSettingChildFragment();
                } else {
                    super.onBackPressed();
                }
            }
        }

        Log.e("mIsTestamentAdded", mIsTestamentAdded + "" + mIsDashboard);
    }

    private void handleSettingChildFragment() {
        if (isSettingChildAdded) {
            Fragment fragment = SettingFragment.newInstance("a", "b");
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
            isSettingChildAdded = false;
            mIsDashboard = false;
        }
//        mIsDashboard = false;
//        if (mIsDashboard) {
//            mIsTestamentAdded = false;
//        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "--------" + requestCode);
    }

    @Override
    public void onTestamentFragmentAdd(int testamentType) {
        Log.e("onTestamentFragmentAdd", "called");
        mIsTestamentAdded = true;
        mTestamentType = testamentType;
    }

    @Override
    public void onAddFragment(Fragment fragment, boolean isDashboard) {
        Log.e("onAddFragment", "called isDashboard : " + isDashboard + "  " + mIsTestamentAdded + "  ");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, "a");
        transaction.commit();
        mIsDashboard = isDashboard;
        isMenu = true;
        if (mIsDashboard) {
            isMenu = false;
            mIsTestamentAdded = false;
        }

    }

    public void addDashboardFragment() {
        isMenu = false;
        // if (mSavedInstanceState == null) {
        String pram1 = "a";
        String pram2 = "b";
        Log.e("addDashboardFragment", "mIsTestamentAdded " + mIsTestamentAdded);
        if (mIsTestamentAdded) {
            pram1 = "1";
            pram2 = Integer.toString(mTestamentType);
            mIsDashboard = false;
            mIsTestamentAdded = true;
        }
        Fragment fragment = DashBoardFragemnt.newInstance(pram1, pram2);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
        // }
    }

    @Override
    public void setImage(String text) {
        Log.e("TEXT", text);
        // mBlueFragment.youveGotMail(file);
        // DashBoardFragemnt mBlueFragment = new DashBoardFragemnt();
        // mBlueFragment.getProfileImage(text);
        // mGreenFragment.getProfileImage(text);
        // mCallback.setImage(dpUrl);
        //Fragment callingFragment = getSupportFragmentManager().findFragmentById(R.id.iv_nav_header_dp);
        /*Picasso.with(DashboardActivity.this).load(text).placeholder(R.drawable.ic_user_placeholder).
                error(R.drawable.ic_user_placeholder).into(mLogo);*/

    }

    @Override
    public void onTestamentFragmentInteraction(Uri uri) {
        Log.e("onTestamentFragIraction", "called");
        Fragment fragment = DashBoardFragemnt.newInstance("a", "b");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }

    @Override
    public void onAboutFragmentInteraction(Uri uri) {
        addDashboardFragment();
    }

    @Override
    public void onWebAttached() {
        isWebOpen = true;
    }

    @Override
    public void onSettingFragmentInteraction(Uri uri) {
        addDashboardFragment();
    }

    @Override
    public void onSettingFragmentChildAdded(boolean isAdded) {
        isSettingChildAdded = isAdded;
    }

    @Override
    public void onProfileFragmentInteraction(Uri uri) {
        addDashboardFragment();
    }

    @Override
    public void onFavouriteFragmentInteraction(Uri uri) {
        addDashboardFragment();
    }

    @Override
    public void onShareFragmentInteraction(Uri uri) {
        addDashboardFragment();
    }


    public void openDrawer() {
        ((DashboardActivity.this)).drawerLayout.openDrawer(GravityCompat.START);
        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                Log.e("open drawer", "open drawer");
                if (!TextUtils.isEmpty(((DashboardActivity.this)).userProfile.getDpUrl())) {
                    Picasso.with(DashboardActivity.this).load(((DashboardActivity.this)).userProfile.getDpUrl()).placeholder(R.drawable.ic_user_placeholder).
                            error(R.drawable.ic_user_placeholder).into(mLogo);
                }
            }
        }, 5000);

    }
}

