package cpm.qbslearning.bibleforce;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import java.util.List;

import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.util.Constants;

import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.AUDIO_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IMAGE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_TITLE;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.P_Id;
import static cpm.qbslearning.bibleforce.util.CommonMethods.getImageBitmap;


public class PagerFragment extends Fragment implements View.OnTouchListener {

    private static final String TITLE = "title";
    private static final String PAGE_ID = "pageId";
    private static final String CHAPTER_ID = "image";
    ImageView pageImageView;
    private String mTitle;
    private String mPageId;
    private String mChapterId;
    private String mAudio;
    private float scale = 1f, minScale = .4f, maxScale = 1.5f;
    private ScaleGestureDetector scaleGestureDetector;
    int pos;
    Cursor mCursor;
    MediaPlayer mediaPlayer;
    List<Integer> pages;
    float scaleX, scaleY;
    private OnFragmentInteractionListener mListener;
    static Activity activity;
    String title, imageUrl, audioUrl;
    int i = 0;
    View view;
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    PageActivity.listnerCallback callback;
    private PointF start = new PointF();
    private PointF mid = new PointF();
    LinearLayout linearLayout;
    private int mode = Constants.NONE;
    private float oldDist = 1f;

    public static float maxZoom;
    public static float minZoom;
    private float dx; // postTranslate X distance
    private float dy; // postTranslate Y distance
    private float[] matrixValues = new float[9];


    float matrixX = 0; // X coordinate of matrix inside the ImageView
    float matrixY = 0; // Y coordinate of matrix inside the ImageView
    float width = 0; // width of drawable
    float height = 0; // height of drawable

    public PagerFragment() {
    }

    public static PagerFragment newInstance(int pos, String chapterId, String pageId, String title, String audio) {
        PagerFragment fragment = new PagerFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(PAGE_ID, pageId);
        args.putString(CHAPTER_ID, chapterId);
        args.putString("au", audio);
        args.putInt("pos", pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString(TITLE);
            mPageId = getArguments().getString(PAGE_ID);
            mChapterId = getArguments().getString(CHAPTER_ID);
            pos = getArguments().getInt("pos");
            mAudio = getArguments().getString("au");
            String[] projection = {P_Id, PAGE_ID, IMAGE_URL, AUDIO_URL, PAGE_TITLE};
            String selection = PAGE_ID + " =? ";
            String[] args = {mPageId};
            mCursor = getContext().getContentResolver().query(DbContract.ChapterPage.CONTENT_URI, projection, selection, args, null);
            if (mCursor != null) {
                Log.e("cursor ", "not null and size " + mCursor.getCount());
                mCursor.moveToFirst();
                imageUrl = mCursor.getString(mCursor.getColumnIndex(IMAGE_URL));
                audioUrl = mCursor.getString(mCursor.getColumnIndex(AUDIO_URL));
                mCursor.close();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_page, container, false);
        scaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        pageImageView = view.findViewById(R.id.v_page_image);
        Bitmap imageBitmap = getImageBitmap(getContext(),imageUrl);
        if (imageBitmap != null) {
            Log.e("imageBitmap", "not null");
            pageImageView.setImageBitmap(imageBitmap);
        } else {
            Log.e("imageBitmap", "null");
        }
//        pageImageView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                mListener.onFragmentTap(1);
//                scaleGestureDetector.onTouchEvent(motionEvent);
//                Log.e("onTouch","called");
//                return true;
//            }
//        });

        pageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener != null){
                    mListener.onFragmentTap(1);
                }
            }
        });

        scaleX = pageImageView.getScaleX();
        scaleY = pageImageView.getScaleY();
        mListener.onFragmentInteraction(title);
        return view;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        ImageView view1 = (ImageView) view;
        view1.setScaleType(ImageView.ScaleType.MATRIX);


        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN: //first finger down only
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());

                Log.e(Constants.TAG, "mode = DRAG");

                mode = Constants.DRAG;

                break;

            case MotionEvent.ACTION_UP: //first finger lifted

            case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                mode = Constants.NONE;

                Log.e(Constants.TAG, "mode = NONE");

                break;

            case MotionEvent.ACTION_POINTER_DOWN: //second finger down
                oldDist = spacing(event);

                Log.e(Constants.TAG, "oldDist = " + oldDist);

                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = Constants.ZOOM;

                    Log.e(Constants.TAG, "mode = ZOOM");
                }

                break;

            case MotionEvent.ACTION_MOVE:

                if (mode == Constants.DRAG) {
                    matrix.set(savedMatrix);

                    matrix.getValues(matrixValues);
                    matrixX = matrixValues[2];
                    matrixY = matrixValues[5];
                    width = matrixValues[0] * (view1.getDrawable().getIntrinsicWidth());
                    height = matrixValues[4] * (view1.getDrawable().getIntrinsicHeight());

                    dx = event.getX() - start.x;
                    dy = event.getY() - start.y;

                    //if image will go outside left bound
                    if (matrixX + dx > 0) {
                        Log.e("dx", "left bound " + dx);
                        dx = -matrixX;
                    }
                    //if image will go outside right bound
                    if (matrixX + dx + width < view.getWidth()) {
                        dx = view.getWidth() - matrixX - width;
                    }
                    //if image will go oustside top bound
                    if (matrixY + dy > 0) {
                        dy = -matrixY;
                    }
                    //if image will go outside bottom bound
                    if (matrixY + dy + height < view.getHeight()) {
                        dy = view.getHeight() - matrixY - height;
                    }

                    matrix.postTranslate(dx, dy);
                } else if (mode == Constants.ZOOM) {
                    Float newDist = spacing(event);

                    Log.e(Constants.TAG, "newDist = " + newDist);

                    if (newDist > 10f) {
                        matrix.set(savedMatrix);
                        float scale = newDist / oldDist;
                        float[] values = new float[9];
                        matrix.getValues(values);
                        float currentScale = values[Matrix.MSCALE_X];

                        if (scale * currentScale > maxZoom)
                            scale = maxZoom / currentScale;

                        else if (scale * currentScale < minZoom)
                            scale = minZoom / currentScale;

                        matrix.postScale(scale, scale, mid.x, mid.y);

                    }
                    break;
                }
        }
        view1.setImageMatrix(matrix);
        return true;
    }


//    @Override
//    public void reScale() {
//        Log.e("reScale ","called");
//        if (pageImageView!=null) {
//            Log.e("reScale ","null");
//            pageImageView.setScaleX(1f);
//            pageImageView.setScaleY(1f);
//        }
//    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        float onScaleBegin = 0;
        float onScaleEnd = 0;

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            if (scale > minScale && scale < maxScale) {
                scale *= scaleGestureDetector.getScaleFactor();
                Log.e("updated Scale ", scale + " ");
            }


            Log.e("scale getScaleFactor", scaleGestureDetector.getScaleFactor() + "===== ");
            Log.e("scale", scale + "------- ");
            pageImageView.setScaleX(scale);
            pageImageView.setScaleY(scale);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            onScaleBegin = scale;
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
            onScaleEnd = scale;
            Log.e("onScaleEnd ", scaleGestureDetector.getScaleFactor() + "-----" + scale + "--------**");
            if (scale <= minScale) {
                Log.e("scale <", minScale + "");
                scale = .5f;

            }
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume ", scale + "  pos " + pos);


        // playAudio(mAudio);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("setUserVisibleHint ", " " + isVisibleToUser);
        if (!isVisible()) {
            Log.e("setUserVisibleHint ", "called");
            onResume();
            if (pageImageView != null) {
                Log.e("setUserVisibleHint ", " scaleing");

                pageImageView.setScaleX(1f);
                pageImageView.setScaleY(1f);
            }
        }

    }


    private float spacing(MotionEvent event) {

        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);

        return (float) Math.sqrt(x * x + y * y);
    }

    private void midPoint(PointF point, MotionEvent event) {

        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String uri);

        void onFragmentTap(int i);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e("FRAGMENT ", "PAUSE" + "  pos " + pos);
        Log.e("Scale", scaleX + " " + scaleY);
        pageImageView.setScaleX(1.0f);
        pageImageView.setScaleY(1.0f);
    }

}
