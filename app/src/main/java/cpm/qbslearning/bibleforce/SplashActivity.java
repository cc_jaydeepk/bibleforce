package cpm.qbslearning.bibleforce;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import cpm.qbslearning.bibleforce.helperMethods.PrefManager;

public class SplashActivity extends AppCompatActivity {

    int _splashTime=3000;
    final PrefManager manager=new PrefManager(this);
    String token=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       token=manager.getSharedPref().getString(getString(R.string.token),null);
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                          sleep(_splashTime);
                } catch (Exception e) {
                } finally {
                  if ( token!= null) {
                      Log.e("TOKEN",token);
                      startActivity(new Intent(SplashActivity.this,
                              DashboardActivity.class));
                  }else
                      startActivity(new Intent(SplashActivity.this,
                              LoginActivity.class));
                    finish();
                }
            };
        };

        splashTread.start();

    }
    }

