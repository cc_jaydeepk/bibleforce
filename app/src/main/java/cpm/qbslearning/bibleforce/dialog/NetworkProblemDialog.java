package cpm.qbslearning.bibleforce.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;


/**
 * Created by rmn on 13-08-2017.
 */

public class NetworkProblemDialog extends AlertDialog {
    protected NetworkProblemDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
