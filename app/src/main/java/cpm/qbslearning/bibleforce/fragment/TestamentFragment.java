package cpm.qbslearning.bibleforce.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.BuyActivity;
import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.adapter.SearchResultAdapter;
import cpm.qbslearning.bibleforce.adapter.TestamentAdapter;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.AccessCodeVerifyResponse;
import cpm.qbslearning.bibleforce.dataModel.FileDownloading;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.service.ChapterDownloadService;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import io.alterac.blurkit.BlurLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PAID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PURCHASED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.PURCHASED_VIA;


public class TestamentFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, SearchResultAdapter.DownloadSearchCallback, TestamentAdapter.DownloadCallback {

    private static final String TESTAMENT_TYPE = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int CALLER = 2;
    private String mParam1;
    private String mParam2;
    final static int STORAGE_PERMISSION_REQUEST_CODE = 1;
    String chapterIdDownloading = null, mChapterId = null;
    Cursor cursor;
    int testamentType = -1;
    RecyclerView mRecyclerView;
    TestamentAdapter mAdapter;
    TextView headerView;
    SearchView mSearchView;
    TestamentAdapter.Holder mHolder;
    RelativeLayout mOldRelativeLayout, mNewRelativeLayout, mOldRelativeOut, mNewRelativeOut;
    RecyclerView mRecyclerOldSearch, mRecyclerNewSearch;
    SearchResultAdapter mOldResultAdapter, mNewResultAdapter;
    //    KProgressHUD mProgressDialog;
    ProgressDialog mProgressDialog;
    Cursor searchCursor;
    ImageView oldImageIcon, newImageIcon;
    int oldImageIconCounter = 1, newImageIconCounter = 1;
    boolean rightDrawerCounter = false;
    DownloadManager mDownloadManager;
    Uri download_Uri;
    List<Long> downloadingRequest = new ArrayList<>();
    List<FileDownloading> downloadingRequestId = new ArrayList<>();
    ImageView searchView, drawerHandlerView;
    DrawerLayout drawer;
    PrefManager mPrefManager;
    private OnTestamentFragmentInteractionListener mListener;
    private static OnChildFragmentCallback mChildFragmentCallback;
    Cursor oldCursor, newCursor;
    TextView text;
    Dialog dialog;
    boolean isCallFromBuy = false, isChapterFree = false;
    private RelativeLayout relDrawerIcon;
    private BlurLayout blurLayout;

    TextView toolbarText;

    int isPaid = -9;
    int isPurchased = -1;


    public TestamentFragment() {
    }


    private InputMethodManager imm;
    private View.OnKeyListener onSoftKeyboardDonePress = new View.OnKeyListener() {
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getKeyCode() == KeyEvent.FLAG_EDITOR_ACTION) {
                // code to hide the soft keyboard
                imm = (InputMethodManager) getContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getApplicationWindowToken(), 0);
            }

            return false;
        }
    };


    public static TestamentFragment newInstance(int testamentType, String param2) {
        TestamentFragment fragment = new TestamentFragment();
        Bundle args = new Bundle();
        args.putInt(TESTAMENT_TYPE, testamentType);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            testamentType = getArguments().getInt(TESTAMENT_TYPE);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_testament, container, false);
        mPrefManager = new PrefManager(getContext());
        drawer = view.findViewById(R.id.drawer_layout);
        drawerHandlerView = view.findViewById(R.id.v_drawer);
        drawerHandlerView.setOnClickListener(this);
        headerView = view.findViewById(R.id.header);
        headerView.setOnClickListener(this);
        searchView = view.findViewById(R.id.v_search_chapter);
        toolbarText = view.findViewById(R.id.toolbarText);
        searchView.setOnClickListener(this);
        DrawerLayout drawer = view.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        mNewRelativeOut = view.findViewById(R.id.v_new_out);
        mOldRelativeOut = view.findViewById(R.id.v_old_out);
        searchViewSetup(view);
        //Log.e("TESTAMENT ", "called");
        oldImageIcon = view.findViewById(R.id.v_old_icon);
        newImageIcon = view.findViewById(R.id.v_new_icon);
        //blurLayout = view.findViewById(R.id.blurLayout);

        mOldRelativeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSearchClick(1, 1);
            }
        });
        mNewRelativeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSearchClick(2, 1);
            }
        });

        mOldRelativeLayout = view.findViewById(R.id.v_old);
        mNewRelativeLayout = view.findViewById(R.id.v_new);

        relDrawerIcon = view.findViewById(R.id.rel_drawer_icon);

        /*mProgressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Downloading...")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);*/

        mProgressDialog = new ProgressDialog(getContext(), ProgressDialog.THEME_DEVICE_DEFAULT_DARK);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setTitle(getString(R.string.downloading_chapter_title));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);

        mProgressDialog.setCancelable(false);

        NavigationView navigationViewRight = view.findViewById(R.id.nav_view_right);
        navigationViewRight.setNavigationItemSelectedListener(this);
        mAdapter = new TestamentAdapter(getContext(), this, testamentType);
        mRecyclerView = view.findViewById(R.id.recycle);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(getResources().getInteger(R.integer.column), StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerNewSearch = view.findViewById(R.id.v_new_recycle);
        mRecyclerOldSearch = view.findViewById(R.id.v_old_recycle);

        if (testamentType >= 0) {
            Cursor cursor = getCursor(testamentType);
            if (testamentType == 1) {
                //headerView.setText(Constants.OLD_TESTAMENT);
                toolbarText.setText(Constants.OLD_TESTAMENT);
            } else {
                // headerView.setText(Constants.NEW_TESTAMENT);
                toolbarText.setText(Constants.NEW_TESTAMENT);
            }
            if (cursor != null && cursor.getCount() > 0) {
                mAdapter.setData(cursor);

                Log.e("cursor", "cursor columns" + cursor.getColumnCount());
                /*int[] columncount = new int[cursor.getColumnCount()];
                for(int i=0;i<cursor.getColumnCount();i++)
                {
                    Log.e("cursor","column"+i+""+cursor.getColumnName(i));
                }

                for(int i=0;i<cursor.getCount()+1;i++)
                {
                    if(i==0)
                    {
                        for(int j=0;j<columncount.length;j++)
                        {
                            Log.e("cursor","column"+j+""+cursor.getColumnName(j));
                            cursor.moveToFirst();
                        }
                    }
                    else
                    {
                        for(int j=0;j<columncount.length;j++)
                        {
                            Log.e("cursor","item"+i+"-->"+cursor.getString(j));
//                            System.out.print("item"+i+"-->"+cursor.getString(j)+"---------");
                        }
                        cursor.moveToNext();
                    }
                }*/
            }
        }

        mOldResultAdapter = new SearchResultAdapter(getContext(), this, 1);
        mRecyclerOldSearch.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerOldSearch.setHasFixedSize(true);
        mRecyclerOldSearch.setAdapter(mOldResultAdapter);

        mNewResultAdapter = new SearchResultAdapter(getContext(), this, 2);
        mRecyclerNewSearch.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerNewSearch.setHasFixedSize(true);
        mRecyclerNewSearch.setAdapter(mNewResultAdapter);
        handleNewText("");

        relDrawerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getContext()).drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onTestamentFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTestamentFragmentInteractionListener) {
            mListener = (OnTestamentFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }


        // check if parent Fragment implements listener
        if (getParentFragment() instanceof OnChildFragmentCallback) {
            mChildFragmentCallback = (OnChildFragmentCallback) getParentFragment();
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void searchViewSetup(View view) {
        String text = "";
        mSearchView = view.findViewById(R.id.v_search);
        mSearchView.setQueryHint("Search");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                Log.e("onQueryTextSubmit", "called " + query);
                CommonMethods.hideSoftKeyboard(getActivity());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                handleNewText(newText);
                return true;
            }
        });
    }

    private void handleNewText(String newText) {
        //Log.e("text ", " " + newText);
        oldCursor = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, DbContract.Chapter.TESTAMENT_TYPE + "=? AND " + CHAPTER_NAME + " LIKE ?", new String[]{"1", (newText == null || newText.trim().equals("")) ? (newText + "%") : ("%" + newText.trim() + "%")}, CHAPTER_ID + " ASC");
        newCursor = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, DbContract.Chapter.TESTAMENT_TYPE + "=? AND " + CHAPTER_NAME + " LIKE ?", new String[]{"2", "%" + newText + "%"}, CHAPTER_ID + " ASC");
        if (newCursor != null) {
            handleSearchClick(2, 0);
            mNewResultAdapter.setData(newCursor);
        } else {
            //Log.e("newcursor", "is null");
        }
        if (oldCursor != null) {
            handleSearchClick(1, 0);
            mOldResultAdapter.setData(oldCursor);
        } else {
            //Log.e("oldcursor", "is null");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.e("onResume tf", "called");
        String chapterId = mPrefManager.getSharedPref().getString(Constants.PAID_CHAPTER_ID, null);
        if (chapterId != null) {
            isCallFromBuy = true;
            mAdapter.notifyDataSetChanged();
            mPrefManager.getEditor().putString(Constants.PAID_CHAPTER_ID, null).apply();
            mChapterId = chapterId;
            handlePermission(/*chapterId*/);
        }

        mOldResultAdapter.notifyDataSetChanged();
        mNewResultAdapter.notifyDataSetChanged();
        mAdapter.notifyDataSetChanged();
        getContext().registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(receiver);
    }

    public Cursor getCursor(int testamentType) {
        String selection = DbContract.Chapter.TESTAMENT_TYPE + "=?/* AND*/ "/*+ DbContract.Chapter.IS_ACTIVE+"=?"*/;
        String args[] = {Integer.toString(testamentType)/*,Integer.toString(getContext().getResources().getInteger(R.integer.IS_ACTIVE))*/};
        cursor = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, selection, args, CHAPTER_ID + " ASC");
        //Log.e("cursor", "setup" + "size " + cursor.getCount());
        return cursor;
    }

    public void handleSearchClick(int i, int j) {

        if (j == 1) {
            if (i == 1) {
                if (mRecyclerOldSearch.getVisibility() == View.GONE) {
                    oldImageIcon.setImageResource(R.drawable.arrow_down);
                    mRecyclerOldSearch.setVisibility(View.VISIBLE);
                } else {
                    oldImageIcon.setImageResource(R.drawable.arrow_left);
                    mRecyclerOldSearch.setVisibility(View.GONE);
                }
            } else if (i == 2) {
                if (mRecyclerNewSearch.getVisibility() == View.VISIBLE) {
                    newImageIconCounter = 1;
                    newImageIcon.setImageResource(R.drawable.arrow_left);
                    mRecyclerNewSearch.setVisibility(View.GONE);
                } else {
                    newImageIconCounter = 0;
                    newImageIcon.setImageResource(R.drawable.arrow_down);
                    mRecyclerNewSearch.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (i == 1) {
                oldImageIcon.setImageResource(R.drawable.arrow_down);
                mRecyclerOldSearch.setVisibility(View.VISIBLE);
            } else if (i == 2) {
                newImageIcon.setImageResource(R.drawable.arrow_down);
                mRecyclerNewSearch.setVisibility(View.VISIBLE);
            }
        }
    }

    public void handlePermission() {
        int permissionCheck = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.e("permission ", "not granted");
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
        } else {
            Log.e("permission-", "already granted");
            Log.e("isChapterFree", isChapterFree + " " + isCallFromBuy);
            Log.e("mChapterId", mChapterId + "");
//            mProgressDialog.setProgress(0);
            mProgressDialog.show();
            String cId = null;
            cId = mChapterId;
            if (isCallFromBuy || isChapterFree) {
                cId = mChapterId;
                isChapterFree = false;
                isCallFromBuy = false;
            }
            ChapterDownloadService.startChapterDownloading(getContext(), null, cId, 0, CALLER);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("STORAGE_PERMISSION", "" + STORAGE_PERMISSION_REQUEST_CODE);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission ", "granted");
//                    mProgressDialog.setProgress(0);
                    mProgressDialog.show();
                    String cId = null;
                    cId = mChapterId;
                    if (isCallFromBuy || isChapterFree) {
                        cId = mChapterId;
                        isChapterFree = false;
                        isCallFromBuy = false;
                    }
                    Log.e("isChapterFree", isChapterFree + " " + isCallFromBuy);
                    Log.e("mChapterId", mChapterId + "");
                    Log.e("cId", cId + "");
                    ChapterDownloadService.startChapterDownloading(getContext(), null, cId, 0, CALLER);
                } else {
                    // This is Case 1 again as Permission is not granted by user

                    //Now further we check if used denied permanently or not
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showDialogOK("Some Permissions are required for downloading books",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                handlePermission();
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                // proceed with logic by disabling the related features or quit the app.
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                });
                    } else {
                        explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                    }
                }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void explain(String msg) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        dialog.create().dismiss();

                    }
                });

        dialog.show();
    }

    public boolean VerifyAccessCode(Context context, String accessCode) {

        if (CommonMethods.isNetworkAvailable(context)) {
            mProgressDialog.show();
            final PrefManager pref = new PrefManager(context);
            String uId = pref.getSharedPref().getString(context.getResources().getString(R.string.uId), null);
            String token = pref.getSharedPref().getString(context.getResources().getString(R.string.token), null);
            String dId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            boolean result[] = {false};
            RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL).create(RetrofitInterface.class);
            Call<AccessCodeVerifyResponse> call = retrofitInterface.verifyAccessCode(token, uId, "checkAccessCode", dId, accessCode);
            call.enqueue(new Callback<AccessCodeVerifyResponse>() {
                @Override
                public void onResponse(Call<AccessCodeVerifyResponse> call, Response<AccessCodeVerifyResponse> response) {
                    mProgressDialog.dismiss();
                    if (response != null) {
                        Log.e("VerifyAccessCode", "response: " + response + "" + response);

                        AccessCodeVerifyResponse verifyResponse = response.body();
                        if (verifyResponse != null) {
                            String msg = verifyResponse.getMessage();
                            int status = verifyResponse.getStatus();
                            if (status > 0) {
                                dialog.dismiss();
                                ContentValues values = new ContentValues();
                                values.put(DbContract.Chapter.IS_PURCHASED, getResources().getInteger(R.integer.PURCHASED));
                                getContext().getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, null, null);
                                handlePermission();
                            } else {
                                text.setText("Invalid Access code!");
                            }
                        } else
                            Toast.makeText(getContext(), "Something went wrong Please try again", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getContext(), "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AccessCodeVerifyResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                    Toast.makeText(getContext(), "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                }
            });

        } else {

            Toast.makeText(getContext(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public void onSearchCallReceive(String chapterId, String chapterName, String price) {
        /*mChapterId = chapterId;
        if (!isPurchased) {
            showDialogForToken(chapterId);
            // showDialogForTokenHaveOrNot(chapterId, chapterName, price);
        } else {

            int purchasedVia = 0;
            Cursor cursor1 = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{PURCHASED_VIA}, CHAPTER_ID + "=?", new String[]{chapterId}, null);
            if (cursor1 != null) {
                //Log.e("cursor1", "" + "not null");
                while (cursor1.moveToNext()) {
                    purchasedVia = cursor1.getInt(cursor1.getColumnIndex(PURCHASED_VIA));
                    //Log.e("purchasedVia", "in while " + purchasedVia);
                    cursor1.close();
                    break;
                }
            }
            //Log.e("purchasedVia", "" + purchasedVia);

            if (purchasedVia == 1) {
                isCallFromBuy = true;
               // showDialogForToken(chapterId);
                // handlePermission();
            } else if (purchasedVia == 2) {
                //showDialogForToken(chapterId);
            }
        }*/
        // handlePermission(/*chapterId*/);

        Cursor cursor = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{DbContract.Chapter.IS_PAID,DbContract.Chapter.IS_PURCHASED}, DbContract.Chapter.CHAPTER_ID + "=?", new String[]{chapterId}, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    isPaid = cursor.getInt(cursor.getColumnIndex(IS_PAID));
                    isPurchased = cursor.getInt(cursor.getColumnIndex(IS_PURCHASED));
                }
            }
            cursor.close();
        }

        if (isPaid == getContext().getResources().getInteger(R.integer.FREE) || (isPurchased==1)) {
            isChapterFree = true;
            mChapterId = chapterId;
            Log.e("isChapterFree", "" + isChapterFree + " cId " + mChapterId);
            handlePermission();
        } else {
            mChapterId = chapterId;
            PrefManager prefManager = new PrefManager(getContext());
            if (prefManager.getIsAccessCodeAuthSuccess()) {
                handlePermission();
            } else {
                showDialogForToken(chapterId);
            }
        }


    }




    @Override
    public void onClick(View view) {
        //Log.e("onClick", "called");
        switch (view.getId()) {
            case R.id.v_drawer:
                mChildFragmentCallback.openDrawer();
                break;
            case R.id.v_search_chapter:
                //Log.e("v_search_chapter", "called");
                drawer.openDrawer(GravityCompat.END);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onCallReceive(String chapterId) {
       // int isPaid = -9;
        //int isPurchased = -1;
        Cursor cursor = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{DbContract.Chapter.IS_PAID,DbContract.Chapter.IS_PURCHASED}, DbContract.Chapter.CHAPTER_ID + "=?", new String[]{chapterId}, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    isPaid = cursor.getInt(cursor.getColumnIndex(IS_PAID));
                    isPurchased = cursor.getInt(cursor.getColumnIndex(IS_PURCHASED));
                }
            }
            cursor.close();
        }

        if (isPaid == getContext().getResources().getInteger(R.integer.FREE) || (isPurchased==1)) {
            isChapterFree = true;
            mChapterId = chapterId;
            Log.e("isChapterFree", "" + isChapterFree + " cId " + mChapterId);
            handlePermission();
        } else {
            mChapterId = chapterId;
            PrefManager prefManager = new PrefManager(getContext());
            if (prefManager.getIsAccessCodeAuthSuccess()) {
                handlePermission();
            } else {
                showDialogForToken(chapterId);
            }
        }
    }

    @Override
    public void showDialog(String chapterId, String title, String price) {
        mChapterId = chapterId;
        // showDialogForTokenHaveOrNot(chapterId, title, price);
        showDialogForToken("");
    }

    public void showDialogForToken(final String chapterId) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.accesstoken_dialog);
        text = (TextView) dialog.findViewById(R.id.v_text);
        final EditText editText = dialog.findViewById(R.id.v_token);
        TextView cancelButton = dialog.findViewById(R.id.v_cancel);
        TextView okButton = dialog.findViewById(R.id.v_ok);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                hideSoftKeyboard(getActivity());
                String enterToken = editText.getText().toString();

                Intent intent = new Intent(getContext(), BuyActivity.class);
                intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, testamentType);
                // intent.putExtra(Constants.EXTRA_CHAPTER_ID, id);
                // intent.putExtra(Constants.EXTRA_CHAPTER_TITLE, title);
                //  intent.putExtra(Constants.EXTRA_CHAPTER_PRICE, price);
                getContext().startActivity(intent);
                dialog.dismiss();

                // gotoBuyActivity(mChapterId, title, price);
                /*if (enterToken.trim().equals("")) {
                    text.setText("Access code can't be blank");
                } else {
                    //hideSoftKeyboard(getActivity());
                    VerifyAccessCode(getContext(), enterToken);
                }*/
            }
        });

        dialog.show();
    }

    public void showDialogForTokenHaveOrNot(final String chapterId, final String title, final String price) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.ask_for_code);

        TextView noButton = dialog.findViewById(R.id.v_no);
        TextView yesButton = dialog.findViewById(R.id.v_yes);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                gotoBuyActivity(mChapterId, title, price);
            }
        });

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                showDialogForToken("");
                // showDialogForToken(chapterId, title, price);
            }
        });

        dialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        oldCursor.close();
        newCursor.close();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            //Log.e("TESTAMENT ", "onReceive");
            if (bundle != null) {
                Log.e("onReceive", "onReceive called from TestamentFragment");
                //Log.e("onRecieve ", "intent not null");
                int resultCode = bundle.getInt(ChapterDownloadService.RESULT);
                String chapterId = bundle.getString(Constants.EXTRA_CHAPTER_ID);
                int position = bundle.getInt(Constants.EXTRA_ITEM_POSITION, -1);
                int requestCode = bundle.getInt(Constants.DOWNLOAD_SERVICE_REQUEST_CODE, -1);
                int percentage = bundle.getInt(Constants.DOWNLOAD_PERCENTAGE, 0);
//                if (/*resultCode == 1*/true) {
                int caller = bundle.getInt(Constants.EXTRA_CALLER, -1);
                if (caller == CALLER) {
                    if (requestCode == Constants.DOWNLOAD_COMPLETE) {
                        if (/*resultCode == 1*/true) {
                            ContentValues values = new ContentValues();
                            values.put(DbContract.Chapter.IS_DOWNLOADED, getResources().getInteger(R.integer.DOWNLOADED));
                            //Log.e("OnRecieve", resultCode + "");
                            int id = getContext().getContentResolver().update(DbContract.Chapter.CONTENT_URI,
                                    values, CHAPTER_ID + " =?", new String[]{chapterId});
                            if (id > 0) {
                                // mHolder.buttonImage.setImageResource(R.drawable.view);
                                mPrefManager.getEditor().putString(Constants.PAID_CHAPTER_ID, null).commit();
                                mAdapter.notifyDataSetChanged();
                                mOldResultAdapter.notifyDataSetChanged();
                                mNewResultAdapter.notifyDataSetChanged();

                                mAdapter.setData(getCursor(testamentType));
                            }
                            mOldResultAdapter.notifyDataSetChanged();
                            mNewResultAdapter.notifyDataSetChanged();
                            mAdapter.notifyDataSetChanged();
                            mAdapter.setData(getCursor(testamentType));
                            Toast.makeText(getContext(), "Download Completed",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getContext(), "Download failed",
                                    Toast.LENGTH_LONG).show();
                            mAdapter.notifyDataSetChanged();
                            mOldResultAdapter.notifyDataSetChanged();
                            mNewResultAdapter.notifyDataSetChanged();
                            mAdapter.setData(getCursor(testamentType));
//                            jkl
                        }
                        mProgressDialog.setProgress(0);
                        mProgressDialog.dismiss();
                    } else if (requestCode == Constants.PERCENTAGE) {
                        Log.e("PERCENTAGE", "PERCENTAGE_DOWNLOAD" + percentage);
                            /*mProgressDialog = new ProgressDialog(getContext());
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setMessage("DOWNLOADING CHAPTER");
                            mProgressDialog.setMax(100);*/
                        // mProgressDialog.setProgressPercentFormat("%");
                        mProgressDialog.setTitle(getString(R.string.downloading_chapter_title));
                        mProgressDialog.setProgress(percentage);

//                            mProgressDialog.setCancelable(false);
                        // mProgressDialog.show();
                    }
                }
            }
//            mHolder.buttonImage.setEnabled(true);
//            mProgressDialog.dismiss();
            // mHolder.progressBar.setVisibility(View.GONE);
        }
    };

    private void gotoBuyActivity(String id, String title, String price) {
        Log.e("mTestamentType", testamentType + "---------------fragment--------------------------");
        Intent intent = new Intent(getContext(), BuyActivity.class);
        intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, testamentType);
        intent.putExtra(Constants.EXTRA_CHAPTER_ID, id);
        intent.putExtra(Constants.EXTRA_CHAPTER_TITLE, title);
        intent.putExtra(Constants.EXTRA_CHAPTER_PRICE, price);
        getContext().startActivity(intent);
    }

    public interface OnTestamentFragmentInteractionListener {
        void onTestamentFragmentInteraction(Uri uri);
    }

    public interface OnChildFragmentCallback {
        public void openDrawer();
    }

    @Override
    public void onStart() {
        super.onStart();
        //  blurLayout.startBlur();
        //  blurLayout.lockView();
    }

    @Override
    public void onStop() {
        // blurLayout.pauseBlur();
        super.onStop();
    }
}
