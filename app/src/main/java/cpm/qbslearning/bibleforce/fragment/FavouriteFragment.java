package cpm.qbslearning.bibleforce.fragment;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.adapter.FavAdapter;
import cpm.qbslearning.bibleforce.data.DbContract;


public class FavouriteFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    RecyclerView mRecyclerView;
    ImageView backView;
    private OnFavouriteFragmentInteractionListener mListener;
    Cursor favCursor;
    Dialog dialog;
    private FavAdapter favAdapter;
    private RelativeLayout relDrawerIconFavorite;

    public FavouriteFragment() {
    }

    public static FavouriteFragment newInstance(String param1, String param2) {
        FavouriteFragment fragment = new FavouriteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        backView = view.findViewById(R.id.v_back);
        mRecyclerView = view.findViewById(R.id.v_recycle);
        relDrawerIconFavorite = view.findViewById(R.id.rel_drawer_icon_favorite);

        mRecyclerView.setHasFixedSize(true);
        favAdapter = new FavAdapter(getContext());
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));
        mRecyclerView.setAdapter(favAdapter);
        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("about", "fav click");
                mListener.onFavouriteFragmentInteraction(null);
            }
        });

        relDrawerIconFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getContext()).drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFavouriteFragmentInteraction(uri);
        }
    }

    public Cursor getFavouriteCursor() {
        favCursor = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, DbContract.Chapter.IS_FAVOURITE + "=?", new String[]{Integer.toString(getContext().getResources().getInteger(R.integer.FAVOURITE))}, null);
        if (favCursor != null) {
            Log.e("favCursor count", favCursor.getCount() + "");
            int count = favCursor.getCount();
            if (count <= 0)
                showDialogForTokenHaveOrNot();
            return favCursor;
        }
        showDialogForTokenHaveOrNot();
        return null;
    }


    public void showDialogForTokenHaveOrNot() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_for_no_fav);

        TextView yesButton = dialog.findViewById(R.id.v_yes);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        favAdapter.setData(getFavouriteCursor());
    }

    @Override
    public void onPause() {
        super.onPause();
        favCursor.close();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFavouriteFragmentInteractionListener) {
            mListener = (OnFavouriteFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFavouriteFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFavouriteFragmentInteraction(Uri uri);
    }

}
