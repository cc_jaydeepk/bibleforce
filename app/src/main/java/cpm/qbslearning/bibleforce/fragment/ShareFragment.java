package cpm.qbslearning.bibleforce.fragment;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.adapter.ShareAdapter;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.ShareChapterByDate;

import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.DATE;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.PAGE_PATH;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShareFragment.OnShareFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class ShareFragment extends Fragment {

    private static final String ARG_PARAM1 = "argparam1";
    private static final String ARG_PARAM2 = "argparam2";
    private OnShareFragmentInteractionListener mListener;
    ImageView backView;
    RecyclerView mRecyclerView;
    Cursor mCursor;
    Dialog dialog;
    private RelativeLayout relDrawerIconShare;

    public ShareFragment() {
    }

    public static ShareFragment newInstance(String param1, String param2) {
        ShareFragment fragment = new ShareFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_share, container, false);
        mRecyclerView = view.findViewById(R.id.v_recycle);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        backView = view.findViewById(R.id.v_back);
        relDrawerIconShare = view.findViewById(R.id.rel_drawer_icon_share);

        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("shareBack", "clicked");
                mListener.onShareFragmentInteraction(null);
            }
        });

        relDrawerIconShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getContext()).drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        ShareAdapter shareAdapter = new ShareAdapter(getContext());

        mCursor = getContext().getContentResolver().query(DbContract.SharePage.CONTENT_URI, new String[]{"DISTINCT " + DATE}, null, null, null);

        if (mCursor != null && mCursor.getCount() > 0) {
            shareAdapter.setData(convertCursorToList(mCursor));
        } else {
            Log.e("mCursor", "null");
        }

        if (mCursor != null)
            if (mCursor.getCount() <= 0)
                showDialogForTokenHaveOrNot();

        mRecyclerView.setAdapter(shareAdapter);

        return view;
    }

    public void showDialogForTokenHaveOrNot() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_for_no_fav);
        TextView title = dialog.findViewById(R.id.v_text);
        title.setText("No share page available");
        TextView yesButton = dialog.findViewById(R.id.v_yes);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onShareFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShareFragmentInteractionListener) {
            mListener = (OnShareFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }
    }


    public List<ShareChapterByDate> convertCursorToList(Cursor cursor) {
        int i = 3;
        List<ShareChapterByDate> shareChapterByDateList = new ArrayList<>();

        while (mCursor.moveToNext()) {
            String date = mCursor.getString(mCursor.getColumnIndex(DATE));
            Log.e("date", date);
            Cursor cursorImages = getContext()
                    .getContentResolver()
                    .query(DbContract.SharePage.CONTENT_URI, new String[]{PAGE_PATH}, DATE + "=?", new String[]{date}, null);
            if (cursorImages != null) {
                List<String> imagePathList = new ArrayList<>();
                while (cursorImages.moveToNext()) {
                    String imagePath = cursorImages.getString(cursorImages.getColumnIndex(PAGE_PATH));
                    imagePathList.add(imagePath);
                }
                cursorImages.close();
                ShareChapterByDate chapterByDate = new ShareChapterByDate(date, imagePathList);
                shareChapterByDateList.add(chapterByDate);
            }
        }
        mCursor.close();
//        while (i>0){
//            String date="date "+i;
//            List<String> imageList=new ArrayList<String >(){
//                {
//                    add(Integer.toString(R.drawable.p1));
//                    add(Integer.toString(R.drawable.p2));
//                    add(Integer.toString(R.drawable.p3));
//                    add(Integer.toString(R.drawable.p4));
//                    add(Integer.toString(R.drawable.p5));
//                    add(Integer.toString(R.drawable.p6));
//                    add(Integer.toString(R.drawable.p7));
//                    add(Integer.toString(R.drawable.p8));
//                }
//            };
//            ShareChapterByDate chapterByDate=new ShareChapterByDate(date,imageList);
//            shareChapterByDateList.add(chapterByDate);
//            i--;
//        }
        return shareChapterByDateList;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnShareFragmentInteractionListener {
        void onShareFragmentInteraction(Uri uri);
    }
}
