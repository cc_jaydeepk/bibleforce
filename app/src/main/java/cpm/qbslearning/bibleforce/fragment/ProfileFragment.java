package cpm.qbslearning.bibleforce.fragment;

import static android.content.Context.MODE_PRIVATE;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.graphics.BitmapCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.LoginActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.SignUpActivity;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.DeleteAccountResponse;
import cpm.qbslearning.bibleforce.dataModel.ProfilePicUpdateResponse;
import cpm.qbslearning.bibleforce.dataModel.ProfileRemoveResponse;
import cpm.qbslearning.bibleforce.dataModel.ProfileUpdateResponse;
import cpm.qbslearning.bibleforce.dataModel.UpdatePasswordResponse;
import cpm.qbslearning.bibleforce.dataModel.UserProfile;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.OnFragmentViewCreated;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.util.Constants;
import io.alterac.blurkit.BlurLayout;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_NAME;
import static cpm.qbslearning.bibleforce.fragment.TestamentFragment.STORAGE_PERMISSION_REQUEST_CODE;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isNetworkAvailable;
import static cpm.qbslearning.bibleforce.util.Constants.ACTION_BASE_URL;


public class ProfileFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    static String countryName;
    ImageView backView;
    TextView emailView, changeView, deleteView;
    EditText fNameView, mNameView, lNameView, profView;
    EditText contactView, addressView, cityView, facebookView, twitterView, linkedInView, otherView;
    Button updateProfileButtonView, updatePasswordButtonView;
    Spinner countryView;
    EditText oldPassView, newPassView, conformPassView;
    PrefManager prefManager;
    boolean isSwitchToProfile = true;
    LinearLayout profileView, showOptionView;
    RelativeLayout passwordView;
    UserProfile userProfile;
    KProgressHUD mProgressDialog;
    Spinner spinner;
    ImageView dotsView, dpView;
    int RESULT_LOAD_IMG = 1;
    String imagePath;
    LinearLayout rootView;
    List<String> countryList = new ArrayList<>();
    List<String> countryIdList = new ArrayList<>();
    int countrySelectionPos = 0;
    View profileSwitch, passwordSwitch;
    Button profileSwitchView, passwordSwitchView;
    private String mParam1;
    private String mParam2;
    private OnProfileFragmentInteractionListener mListener;
    Activity context;
    private RelativeLayout relDrawerIconProfile;
    private TextView txtProfileEmail, txtProfileName, txtProfileContactNumber, txtProfileAddress, txtProfileProfessionalHeadline, txtDeleteAcc;

    private RelativeLayout rootProfile;
    PrefManager manager;

    public Uri profile_upload_ImageUri;
    SharedPreferences prefrence;
    DashBoardFragemnt dashBoardFragemnt;


    Bitmap resizedBitmap;

    private onProfileFragmentListener mCallback;
    public ImageView ivNavHeaderDp;
    private BlurLayout blurLayout;
//    String dpUrl;
//    NavigationView navigationView;


    public ProfileFragment() {
    }


    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        countryList.add("Select Country");
        Cursor cursor = getContext().getContentResolver().query(DbContract.CountryTable.CONTENT_URI, null, null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String cID = cursor.getString(cursor.getColumnIndex(COUNTRY_ID));
                String cName = cursor.getString(cursor.getColumnIndex(COUNTRY_NAME));
                countryIdList.add(cID);
                countryList.add(cName);
            }
            cursor.close();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        prefManager = new PrefManager(getContext());
        userProfile = new UserProfile(getContext());
        mProgressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        context = getActivity();
        View view = inflater.inflate(R.layout.fragment_profile_new, container, false);

        manager = new PrefManager(getContext());
        OnFragmentViewCreated viewFragmentCreated = (OnFragmentViewCreated) requireActivity();
        // call the method in the activity
        //viewFragmentCreated.onBindView(this, ivNavHeaderDp);

        dpView = view.findViewById(R.id.v_dp);
        dotsView = view.findViewById(R.id.v_dots);
        dotsView.setOnClickListener(this);

        showOptionView = view.findViewById(R.id.v_show_option);
        rootProfile = view.findViewById(R.id.root_profile);

        changeView = view.findViewById(R.id.v_change);
        deleteView = view.findViewById(R.id.v_delete);
        changeView.setOnClickListener(this);
        deleteView.setOnClickListener(this);

        profileSwitchView = view.findViewById(R.id.v_prof_back);
        passwordSwitchView = view.findViewById(R.id.v_pass_back);
        //profileSwitch = view.findViewById(R.id.v_profile_switch);
        passwordSwitch = view.findViewById(R.id.v_pass_switch);
        profileSwitchView.setOnClickListener(this);
        passwordSwitchView.setOnClickListener(this);

        profileView = view.findViewById(R.id.v_user_profile_view);
        passwordView = view.findViewById(R.id.v_password_view);

        oldPassView = view.findViewById(R.id.v_pass_curr);
        newPassView = view.findViewById(R.id.v_pass_new);
        conformPassView = view.findViewById(R.id.v_pass_conf);

        fNameView = view.findViewById(R.id.v_f_name);
        mNameView = view.findViewById(R.id.v_m_name);
        lNameView = view.findViewById(R.id.v_l_name);
        profView = view.findViewById(R.id.v_proff);
        contactView = view.findViewById(R.id.v_contact);
        addressView = view.findViewById(R.id.v_address);
        cityView = view.findViewById(R.id.v_city);
        countryView = view.findViewById(R.id.v_country);
        emailView = view.findViewById(R.id.v_email);
        facebookView = view.findViewById(R.id.v_facebook);
        twitterView = view.findViewById(R.id.v_twitter);
        linkedInView = view.findViewById(R.id.v_linked);
        otherView = view.findViewById(R.id.v_other);
        updateProfileButtonView = view.findViewById(R.id.v_update_profile);
        updateProfileButtonView.setOnClickListener(this);
        updatePasswordButtonView = view.findViewById(R.id.v_update_password);
        updatePasswordButtonView.setOnClickListener(this);
        relDrawerIconProfile = view.findViewById(R.id.rel_drawer_icon_profile);
        relDrawerIconProfile.setOnClickListener(this);
        txtProfileEmail = view.findViewById(R.id.txt_profile_email);
        txtProfileName = view.findViewById(R.id.txt_profile_name);
        txtProfileContactNumber = view.findViewById(R.id.txt_profile_contact_number);
        txtProfileAddress = view.findViewById(R.id.txt_profile_address);
        txtProfileProfessionalHeadline = view.findViewById(R.id.txt_profile_professional_headline);
        blurLayout = view.findViewById(R.id.blurLayout);

        txtDeleteAcc = view.findViewById(R.id.txtDeleteAcc);
        txtDeleteAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   deleteAccount();
                final Dialog myDialog = new Dialog(getActivity());
                myDialog.setContentView(R.layout.delete_account_dialog);
                myDialog.setCancelable(false);
                myDialog.show();
                TextView yes = (TextView) myDialog.findViewById(R.id.txtYes);

                TextView no = (TextView) myDialog.findViewById(R.id.txtNo);
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteAccount();
                        myDialog.dismiss();
                    }
                });
            }
        });

        //((DashboardActivity) getContext()).profilePic = view.findViewById(R.id.iv_nav_header_dp);
        //backView.setOnClickListener(this);

        //  View navView = (NavigationView) view.findViewById(R.id.nav_view);
        //  mProfileImage = view.findViewById(R.id.iv_nav_header_dp);

        // NavigationView navigationView = (NavigationView) getView().findViewById(R.id.nav_view);
        // View headerView = navigationView.getHeaderView(0);
        //mProfileImage = view.getRootView().findViewById(R.id.iv_nav_header_dp);
        /*dashBoardFragemnt = new DashBoardFragemnt();
        View view1 = dashBoardFragemnt.getView();
        if (view1 != null) {
            mProfileImage = view.findViewById(R.id.iv_nav_header_dp);
        }*/
        // mProfileImage = view.getRootView().findViewById(R.id.iv_nav_header_dp);

        /*Code to set the background image in fragment*/
        /*Picasso.with(getActivity()).load(R.drawable.setting_bg).into(new Target() {

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                rootProfile.setBackground(new BitmapDrawable(context.getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
                Log.d("TAG", "FAILED");
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                Log.d("TAG", "Prepare Load");
            }
        });*/

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, countryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryView.setAdapter(adapter);
        countryView.setOnItemSelectedListener(this);
        updateProfileView(userProfile);
        return view;
    }

    public interface onProfileFragmentListener {
        // void messageFromGreenFragment(File file);
        void setImage(String text);
    }

    public void getProfileImage(String text) {
        /*Picasso.with(getContext()).load(text).placeholder(R.drawable.ic_user_placeholder).
                error(R.drawable.ic_user_placeholder).into(ivNavHeaderDp);*/
        // mCallback.setImage(text);
        //  mCallback.setImage(text);
    }

    public void updateProfileView(UserProfile userProfile) {
        fNameView.setText(userProfile.getFirstName());
        mNameView.setText(userProfile.getMiddleName());
        lNameView.setText(userProfile.getLastName());
        profView.setText(userProfile.getProfessionalHeadLine());
        contactView.setText(userProfile.getContactNo());
        addressView.setText(userProfile.getAddress());
        cityView.setText(userProfile.getCity());
        countryName = userProfile.getCountry();
        //countryView.setText(userProfile.getCountry());
        emailView.setText(userProfile.getEmail());
        facebookView.setText(userProfile.getFacebookId());
        twitterView.setText(userProfile.getTwitterId());
        linkedInView.setText(userProfile.getLinkedInId());
        otherView.setText(userProfile.getOtherId());
        txtProfileName.setText(userProfile.getFirstName() + " " + userProfile.getMiddleName() + " " +
                userProfile.getLastName());
        txtProfileEmail.setText(userProfile.getEmail());
        txtProfileContactNumber.setText(userProfile.getContactNo());
        txtProfileAddress.setText(userProfile.getAddress());
        txtProfileProfessionalHeadline.setText(userProfile.getProfessionalHeadLine());

        String countryId = userProfile.getCountry();
        Log.e("countryId", countryId + "");

        if (countryId != null && !countryId.equals(""))
            countrySelectionPos = countryIdList.indexOf(countryId);

        countryView.setSelection(countrySelectionPos);

        Log.e("countrySelectionPos", countrySelectionPos + "");
        if (!userProfile.getDpUrlLocal().equals("")) {
            setDpView(new File(userProfile.getDpUrlLocal()));
        } else {
            setDpView(null);
        }

    }

    public void setDpView(File file) {
        final String dpUrl = userProfile.getDpUrl();
        if (file != null) {
            Picasso.with(getContext())
                    .load(file)
                    .into(dpView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (mProgressDialog.isShowing())
                                //  mCallback.setImage(dpUrl);
                                mProgressDialog.dismiss();
                            return;
                        }

                        @Override
                        public void onError() {
                            if (dpUrl != null && !dpUrl.equals("")) {
                                Picasso.with(getContext())
                                        .load(dpUrl)
                                        .placeholder(R.drawable.ir)
                                        .networkPolicy(NetworkPolicy.OFFLINE)
                                        .into(dpView, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                if (mProgressDialog.isShowing())
                                                    mProgressDialog.dismiss();
                                                return;
                                            }

                                            @Override
                                            public void onError() {
                                                Picasso.with(getContext())
                                                        .load(dpUrl)
                                                        .error(R.drawable.ir)
                                                        .placeholder(R.drawable.ir)
                                                        .into(dpView, new com.squareup.picasso.Callback() {
                                                            @Override
                                                            public void onSuccess() {
                                                                if (mProgressDialog.isShowing())
                                                                    mProgressDialog.dismiss();
                                                                return;
                                                            }

                                                            @Override
                                                            public void onError() {
                                                                if (mProgressDialog.isShowing())
                                                                    mProgressDialog.dismiss();
                                                                Log.v("Picasso", "Could not fetch image");
                                                                return;
                                                            }
                                                        });
                                            }
                                        });
                            }
                        }

                    });
        } else if (dpUrl != null && !dpUrl.equals("")) {
            Picasso.with(getContext())
                    .load(dpUrl)
                    .placeholder(R.drawable.ir)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(dpView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            Picasso.with(getContext())
                                    .load(dpUrl)
                                    .error(R.drawable.ir)
                                    .placeholder(R.drawable.ir)
                                    .into(dpView, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            if (mProgressDialog.isShowing()) {
                                                mProgressDialog.dismiss();
                                            }
                                            return;
                                        }

                                        @Override
                                        public void onError() {
                                            if (mProgressDialog.isShowing()) {
                                                mProgressDialog.dismiss();
                                            }
                                            Log.v("Picasso", "Could not fetch image");
                                            return;
                                        }
                                    });
                        }
                    });
        } else {
            dpView.setImageResource(R.drawable.ir);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onProfileFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragmentInteractionListener) {
            mListener = (OnProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }

        if (context instanceof onProfileFragmentListener) {
            mCallback = (onProfileFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGreenFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_back:

                /*Log.e("about", "profile click");
                mListener.onProfileFragmentInteraction(null);*/
                break;
            case R.id.v_update_profile:
                updateProfileAPiHit();
                break;
            case R.id.v_update_password:
                updatePasswordAPiHit();
                break;
            case R.id.v_prof_back:
                //profileSwitchView.setBackgroundColor(Color.parseColor("#e1e1e1"));
                // profileSwitch.setBackgroundColor(getResources().getColor(R.color.toolbarColor));
                //passwordSwitchView.setBackgroundColor(Color.parseColor("#ffffff"));
                //passwordSwitch.setBackgroundColor(getResources().getColor(R.color.white));
                profileView.setVisibility(View.VISIBLE);
                profileSwitchView.setVisibility(View.GONE);
                passwordSwitchView.setVisibility(View.VISIBLE);
                passwordView.setVisibility(View.GONE);
                break;
            case R.id.v_pass_back:
//                profileSwitchView.setBackgroundColor(Color.parseColor("#ffffff"));
                //profileSwitch.setBackgroundColor(getResources().getColor(R.color.white));
                //passwordSwitchView.setBackgroundColor(Color.parseColor("#e1e1e1"));
                // passwordSwitch.setBackgroundColor(getResources().getColor(R.color.toolbarColor));
                profileView.setVisibility(View.GONE);
                profileSwitchView.setVisibility(View.VISIBLE);
                passwordSwitchView.setVisibility(View.GONE);
                passwordView.setVisibility(View.VISIBLE);
                break;
            case R.id.v_dots:
                if (showOptionView.getVisibility() == View.VISIBLE) {
                    showOptionView.setVisibility(View.GONE);
                } else {
                    showOptionView.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.v_delete:
                if (isNetworkAvailable(getContext()))
                    hitDpDeleteApi();
                else {
                    Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();
                }
                showOptionView.setVisibility(View.GONE);
                break;
            case R.id.v_change:
                if (isNetworkAvailable(getContext()))
                    handlePermission();
                else {
                    Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();
                }

                showOptionView.setVisibility(View.GONE);
                break;

            case R.id.rel_drawer_icon_profile:

                ((DashboardActivity) getContext()).drawerLayout.openDrawer(Gravity.LEFT);
                ((DashboardActivity) getContext()).drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

                    }

                    @Override
                    public void onDrawerOpened(@NonNull View drawerView) {

                        if (isAdded()) {
                            SharedPreferences pref = getActivity().getSharedPreferences("MySharedPref", MODE_PRIVATE);
                            String updatedPic = pref.getString("image", "");

                            ((DashboardActivity) getActivity()).profileNameTextview.setText(((DashboardActivity) getContext()).userProfile.getFirstName() + " " +
                                    ((DashboardActivity) getActivity()).userProfile.getMiddleName() + " " +
                                    ((DashboardActivity) getActivity()).userProfile.getLastName());

                            ((DashboardActivity) getActivity()).profileEmailTextview.setText(((DashboardActivity) getContext()).userProfile.getEmail());


                            Picasso.with(getActivity()).load((updatedPic)).resize(600, 200).placeholder(R.drawable.ic_user_placeholder).
                                    error(R.drawable.ic_user_placeholder).into(((DashboardActivity) getContext()).profileImageView);
                        }

                    }

                    @Override
                    public void onDrawerClosed(@NonNull View drawerView) {

                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {

                    }
                });
                break;
        }
    }

    /*@Override
    public void openDrawer() {
        ((DashboardActivity) getContext()).drawer.openDrawer(GravityCompat.START);
        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                Log.e("open drawer", "open drawer");
                if (!TextUtils.isEmpty(((DashboardActivity) getContext()).userProfile.getDpUrl())) {
                    Picasso.with(getContext()).load(((DashboardActivity) getContext()).userProfile.getDpUrl()).placeholder(R.drawable.ic_user_placeholder).
                            error(R.drawable.ic_user_placeholder).into(mProfileImage);
                }
            }
        }, 5000);

    }*/

    /*private void updateNavHeaderView(){
        View headerView = navigationView.inflateHeaderView(R.layout.nav_header);
        ImageView ivUserProfilePhoto = (ImageView) headerView.findViewById(R.id.ivUserProfilePhoto);
        TextView userName = (TextView) headerView.findViewById(R.id.userName);

        if (user != null && user.getUser_image() != null && !user.getUser_image().equals("")) {
            Picasso.with(MainActivity.this)
                    .load(Config.BASE_IMAGE_URL+"/"+user.getUser_image())
                    .placeholder(R.drawable.user_profile)
                    .resize(avatarSize, avatarSize)
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .into(ivUserProfilePhoto);
        } else {
            ivUserProfilePhoto.setImageDrawable(getResources().getDrawable(R.drawable.user_profile));
        }
    }*/


    private void hitDpChangeApi() {
        Log.e("hitDpChangeApi", "called");

        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.e("hitDpChangeApi", "kitkat");
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            Log.e("hitDpChangeApi", "< kitkat");
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        // intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");


/*        Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);*/

        startActivityForResult(Intent.createChooser(intent, "Select Image using"), RESULT_LOAD_IMG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String dataString = "";
        Log.e("onActivityResult", "called " + requestCode + "  : " + resultCode + "  " + data);
        if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK) {
            ClipData clipData = data.getClipData();
            Log.e("onActivityResult", "requestCode");
            try {
                Uri uri = null;
                if (clipData != null) {
                    ClipData.Item item = data.getClipData().getItemAt(0);
                    uri = item.getUri();
                    Log.e("if uri", uri.toString());
                } else {
                    dataString = data.getDataString();
                    if (dataString != null) {
                        uri = Uri.parse(dataString);
                        Log.e("else if uri", uri.toString());
                    }
                }
                /*if (uri == null) {
                    final InputStream imageStream;
                    try {
                        imageStream = getContext().getContentResolver().openInputStream(uri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        dpView.setImageBitmap(selectedImage);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }*/

                if (uri != null)
                    Log.e("uri final", uri + "");
                String filePath = getPath(getContext(), uri);
                Log.e("filePath", filePath + "");

                if (filePath != null) {
                    File file = new File(filePath);
                    //Toast.makeText(getContext(), "filePath :" + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
                    try {
                        uploadImage(file);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                } else {
                    // Toast.makeText(getContext(), "filePath : null", Toast.LENGTH_LONG).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            Log.e("uri called", "hfhgfg");
        }
    }


    private void uploadImage(final File file) {
        Log.e("imagePath", imagePath + "");
        Crashlytics.log(1, "image File", file.getAbsolutePath());

        if (isNetworkAvailable(getContext())) {
            mProgressDialog.setLabel("Profile Updating...");
            mProgressDialog.show();
            String token = prefManager.getSharedPref().getString(getResources().getString(R.string.token), null);
            String dId = prefManager.getSharedPref().getString(getResources().getString(R.string.dId), null);
            String uId = prefManager.getSharedPref().getString(getResources().getString(R.string.uId), null);

            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            RequestBody tokenBody = RequestBody.create(MediaType.parse("text/plain"), token);
            RequestBody uIdBody = RequestBody.create(MediaType.parse("text/plain"), uId);
            RequestBody dIdBody = RequestBody.create(MediaType.parse("text/plain"), dId);

            RequestBody optionBody = RequestBody.create(MediaType.parse("text/plain"), "updateProfilePic");
            RequestBody deviceTypeBody = RequestBody.create(MediaType.parse("text/plain"), getResources().getString(R.string.isTab));
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("file", file.getName(), requestFile);

            RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL).create(RetrofitInterface.class);
            Call<ProfilePicUpdateResponse> call = retrofitInterface.updateProfilePic(body, tokenBody, uIdBody, optionBody, dIdBody, deviceTypeBody);
            call.enqueue(new Callback<ProfilePicUpdateResponse>() {
                @Override
                public void onResponse(Call<ProfilePicUpdateResponse> call, Response<ProfilePicUpdateResponse> response) {
                    if (response != null) {
                        ProfilePicUpdateResponse profilePicUpdateResponse = response.body();
                        if (profilePicUpdateResponse != null) {
                            int status = profilePicUpdateResponse.getStatus();
                            String message = profilePicUpdateResponse.getMessage();
                            String url = profilePicUpdateResponse.getServerUrl();
                            String image = profilePicUpdateResponse.getProfilePic();
                            userProfile.setDpUrl(url + image + "");
                            userProfile.setDpUrlLocal(file.getAbsolutePath());
                            Log.e("url", url + image + "");
                            Log.e("message", message + ", status" + status);
                            //Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

                            String dpUrl = userProfile.getDpUrl();

                            SharedPreferences sharedPreferences = getContext().getSharedPreferences("MySharedPref", MODE_PRIVATE);
                            SharedPreferences.Editor myEdit = sharedPreferences.edit();
                            myEdit.putString("image", dpUrl);
                            myEdit.apply();

                            //Bitmap bitmap = decodeFile(new File(dpUrl));
                            //dpView.setImageBitmap(bitmap);

                            mProgressDialog.dismiss();
                            // setDpView(file);



                            /*Glide.with(getContext()).asBitmap()
                                    .load(bitmap)
                                    .dontTransform()
                                    .into(dpView);*/

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 8;

                            Bitmap bitmap = Bitmap.createBitmap(
                                    300, 400, Bitmap.Config.ARGB_8888
                            );

                            // Bitmap bm = BitmapFactory.decodeFile(dpUrl,options);
                            // dpView.setImageBitmap(bm);

                            // Glide.with(getContext()).load(dpUrl).into(dpView);

                            Picasso.with(getContext()).load(dpUrl).resize(150, 150).placeholder(R.drawable.ic_user_placeholder).
                                    error(R.drawable.ic_user_placeholder).into(dpView);


                        } else {
                            mProgressDialog.dismiss();
                            //  Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        mProgressDialog.dismiss();
                        //  Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        Log.e("response", "null");
                    }
                }

                @Override
                public void onFailure(Call<ProfilePicUpdateResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                    Log.e("Failure", "called ");
                    if (t != null)
                        Log.e("Failure", t.getMessage() + "");
                    //Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String getUriFilePath(Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(getContext(), uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(getContext(), contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };
                return getDataColumn(getContext(), contentUri, selection, selectionArgs);
            }
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String getFilePath(Uri uri) {
        String filePath = "";
        if ("com.android.externalstorage.documents".equals(uri.getAuthority())) {

            if (DocumentsContract.isDocumentUri(getContext(), uri)) {

                String wholeID = DocumentsContract.getDocumentId(uri);
                String id = wholeID.split(":")[0];
                String[] column = {MediaStore.Images.Media.DATA};

                String sel = MediaStore.Images.Media._ID + "=?";

                Cursor cursor = getContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);

                int columnIndex = cursor.getColumnIndex(column[0]);

                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }

                cursor.close();
                return filePath;
            }
        }
        return null;
    }

    private void deleteAccount() {
        if (isNetworkAvailable(getContext())) {

            mProgressDialog.setLabel("Remove account...");
            mProgressDialog.show();
            String token = prefManager.getSharedPref().getString(getResources().getString(R.string.token), null);
            String dId = prefManager.getSharedPref().getString(getResources().getString(R.string.dId), null);
            String uId = prefManager.getSharedPref().getString(getResources().getString(R.string.uId), null);

            RetrofitInterface retrofitInterface = RetrofitClient
                    .getRetrofitClient(Constants.ACTION_BASE_URL)
                    .create(RetrofitInterface.class);

            Call<DeleteAccountResponse> call = retrofitInterface.deleteAccount(dId, "deleteAccount", token, uId);
            call.enqueue(new Callback<DeleteAccountResponse>() {
                @Override
                public void onResponse(Call<DeleteAccountResponse> call, Response<DeleteAccountResponse> response) {
                    if (response != null) {
                        DeleteAccountResponse deleteAccountResponse = response.body();
                        if (deleteAccountResponse != null) {
                            final int status = deleteAccountResponse.getStatus();
                            String message = deleteAccountResponse.getMessage();


                            final Dialog myDialog = new Dialog(getActivity());
                            myDialog.setContentView(R.layout.delet_confrmation_dialog);
                            myDialog.setCancelable(false);
                            myDialog.show();
                            TextView okBtn = (TextView) myDialog.findViewById(R.id.txtOk);
                            TextView deleteMessage = (TextView) myDialog.findViewById(R.id.txtDeleteMsg);
                            deleteMessage.setText(message);

                            okBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfile userProfile = new UserProfile(getContext());
                                    manager.getEditor().remove(getString(R.string.uId)).apply();
                                    manager.getEditor().remove(getString(R.string.token)).apply();
                                    String loginType = manager.getSharedPref().getString(getString(R.string.login_type), null);
                                    /*if (manager.getSharedPref().getString(getString(R.string.uId), null) == null) {
                                        userProfile.removeProfile();
                                        myDialog.dismiss();
                                        getActivity().finish();
                                        startActivity(new Intent(getContext(), LoginActivity.class));
                                    }*/
                                   /* Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();*/
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });

                        } else {
                            mProgressDialog.dismiss();
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<DeleteAccountResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getContext(), "Internet Not available", Toast.LENGTH_SHORT).show();
        }


    }

    private void hitDpDeleteApi() {

        if (isNetworkAvailable(getContext())) {
            mProgressDialog.setLabel("Profile Picture removing...");
            mProgressDialog.show();
            String token = prefManager.getSharedPref().getString(getResources().getString(R.string.token), null);
            String dId = prefManager.getSharedPref().getString(getResources().getString(R.string.dId), null);
            String uId = prefManager.getSharedPref().getString(getResources().getString(R.string.uId), null);

            RetrofitInterface retrofitInterface = RetrofitClient
                    .getRetrofitClient(Constants.ACTION_BASE_URL)
                    .create(RetrofitInterface.class);

            Call<ProfileRemoveResponse> call = retrofitInterface.removeProfile(token, uId, "updateProfilePicRemove", dId, getResources().getString(R.string.isTab));
            call.enqueue(new Callback<ProfileRemoveResponse>() {
                @Override
                public void onResponse(Call<ProfileRemoveResponse> call, Response<ProfileRemoveResponse> response) {
                    if (response != null) {
                        ProfileRemoveResponse removeResponse = response.body();
                        if (removeResponse != null) {
                            int status = removeResponse.getStatus();
                            String message = removeResponse.getMessage();
                            Log.e("message", message + " , status " + status);
                            // if (status == 1) {
                            userProfile.setDpUrl("");
                            userProfile.setDpUrlLocal("");
                            setDpView(null);
                            //} else {
                            //  }
                            // Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                        } else {
                            mProgressDialog.dismiss();
                            // Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                            Log.e("removeResponse", "null");
                        }
                    } else {
                        mProgressDialog.dismiss();
                        // Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        Log.e("response", "null");
                    }
                }

                @Override
                public void onFailure(Call<ProfileRemoveResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                    // Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getContext(), "Internet Not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void updatePasswordAPiHit() {
        if (isNetworkAvailable(getContext())) {

            final String oldPass = oldPassView.getText().toString().trim();
            final String newPass = newPassView.getText().toString().trim();
            String conformPass = conformPassView.getText().toString().trim();

            if (oldPass.equals("") || newPass.equals("") || conformPass.equals("")) {
                Toast.makeText(getContext(), "Password can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (oldPass.length() < 5 || newPass.length() < 5 || conformPass.length() < 5) {
                Toast.makeText(getContext(), "Password should be greater than 4 digits", Toast.LENGTH_SHORT).show();
                return;
            }

            if (newPass.equals(conformPass)) {
                mProgressDialog.setLabel("Password Updating...");
                mProgressDialog.show();
                String token = prefManager.getSharedPref().getString(getResources().getString(R.string.token), null);
                String dId = prefManager.getSharedPref().getString(getResources().getString(R.string.dId), null);
                String uId = prefManager.getSharedPref().getString(getResources().getString(R.string.uId), null);

                RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(ACTION_BASE_URL).create(RetrofitInterface.class);
                Call<UpdatePasswordResponse> call = retrofitInterface.updatePassword(token, uId, "updatePassword", dId, getResources().getString(R.string.isTab), oldPass, newPass);
                call.enqueue(new Callback<UpdatePasswordResponse>() {
                    @Override
                    public void onResponse(Call<UpdatePasswordResponse> call, Response<UpdatePasswordResponse> response) {
                        if (response != null) {
                            UpdatePasswordResponse passwordResponse = response.body();
                            if (passwordResponse != null) {
                                int status = passwordResponse.getStatus();
                                String message = passwordResponse.getMessage();
                                String newPassword = passwordResponse.getStrNewPassword();
                                userProfile.setPassword(newPassword);
                                Log.e("message", message + " : status " + status + " : newPassword " + newPassword);
                                mProgressDialog.dismiss();
                                oldPassView.setText("");
                                newPassView.setText("");
                                conformPassView.setText("");

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("Password changed successfully!!")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(context, DashboardActivity.class);
                                                startActivity(intent);
                                                context.finish();
                                            }
                                        });
                                AlertDialog alertDialog = builder.create();
                                //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                alertDialog.show();
                                Button bg = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                                bg.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                //Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();


                            } else {
                                mProgressDialog.dismiss();
                                Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                Log.e("passwordResponse", "null");
                            }
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                            Log.e("response", "null");
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdatePasswordResponse> call, Throwable t) {
                        mProgressDialog.dismiss();
                    }
                });
            } else {
                conformPassView.setError("Password mismatched");
            }
        } else {
            Toast.makeText(getContext(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateProfileAPiHit() {
        if (isNetworkAvailable(getContext())) {
            final String fName = fNameView.getText().toString();
            final String mName = mNameView.getText().toString();
            final String lName = lNameView.getText().toString();
            final String proff = profView.getText().toString();
            final String contact = contactView.getText().toString();
            final String address = addressView.getText().toString();
            final String city = cityView.getText().toString();
            final String country = countrySelectionPos == 0 ? "" : countryIdList.get(countrySelectionPos);
            final String email = emailView.getText().toString();
            final String facebookId = facebookView.getText().toString();
            final String twitterId = twitterView.getText().toString();
            final String linkedInId = linkedInView.getText().toString();
            final String otherId = otherView.getText().toString();

            String token = prefManager.getSharedPref().getString(getResources().getString(R.string.token), null);
            String diD = prefManager.getSharedPref().getString(getResources().getString(R.string.dId), null);
            String uId = prefManager.getSharedPref().getString(getResources().getString(R.string.uId), null);

            mProgressDialog.setLabel("Profile updating...");
            mProgressDialog.show();

            RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(ACTION_BASE_URL).create(RetrofitInterface.class);
            Call<ProfileUpdateResponse> call = retrofitInterface.updateProfile(token, uId, "updateProfile", diD, getResources().getString(R.string.isTab), fName, mName, lName, proff, address, city,
                    country, facebookId, linkedInId, twitterId, otherId, contact);

            call.enqueue(new Callback<ProfileUpdateResponse>() {
                @Override
                public void onResponse(Call<ProfileUpdateResponse> call, Response<ProfileUpdateResponse> response) {
                    if (response != null) {
                        ProfileUpdateResponse updateResponse = response.body();
                        if (updateResponse != null) {
                            int status = updateResponse.getStatus();
                            String message = updateResponse.getMessage();
                            if (status == 1) {
                                userProfile.setFirstName(fName);
                                userProfile.setMiddleName(mName);
                                userProfile.setLastName(lName);
                                userProfile.setProfessionalHeadLine(proff);
                                userProfile.setContactNo(contact);
                                userProfile.setAddress(address);
                                userProfile.setCity(city);
                                userProfile.setCountry(countryIdList.get(countrySelectionPos));
                                userProfile.setEmail(email);
                                userProfile.setFacebookId(facebookId);
                                userProfile.setTwitterId(twitterId);
                                userProfile.setLinkedInId(linkedInId);
                                userProfile.setOtherId(otherId);
                                updateProfileView(userProfile);
                                mProgressDialog.dismiss();
                            }
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(getContext(), "Something went wrong... ", Toast.LENGTH_SHORT).show();
                            Log.e("updateResponse", "null");
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), "Something went wrong... ", Toast.LENGTH_SHORT).show();
                        Log.e("response", "null");
                    }
                }

                @Override
                public void onFailure(Call<ProfileUpdateResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(getContext(), "Internet not available", Toast.LENGTH_SHORT).show();
        }

    }

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public void handlePermission() {
        int permissionCheck = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            //Log.e("permission ", "not granted");
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
            //ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
        } else {
            hitDpChangeApi();
        }

        /*int permission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    getActivity(),
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }else {
            hitDpChangeApi();
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Log.e("permission ", "granted");
                    hitDpChangeApi();
                } else {
                    //Log.e("permission", "DENIED");
                }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();
        countrySelectionPos = i;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public interface OnProfileFragmentInteractionListener {
        void onProfileFragmentInteraction(Uri uri);
    }

    @Override
    public void onStop() {
        //blurLayout.pauseBlur();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        // blurLayout.startBlur();
        //  blurLayout.lockView();
    }
}
