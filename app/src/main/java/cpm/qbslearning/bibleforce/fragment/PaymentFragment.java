package cpm.qbslearning.bibleforce.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.fragment.app.Fragment;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardMultilineWidget;

import java.io.UnsupportedEncodingException;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.util.Constants;

public class PaymentFragment extends Fragment implements View.OnClickListener {
    private static final String CHAPTER_ID = "chapterId";
    private static final String CHAPTER_NAME = "chapterName";
    private String mChapterId;
    private String mChapterName;
    Card cardToSave;
    CardInputListener mCardInputListener;
    CardMultilineWidget mCardMultilineWidget;
    ViewFlipper mViewFlipper;
    ImageView cardView;
    RotateAnimation rotate;
    PrefManager mPrefManager;
    private OnFragmentInteractionListener mListener;

    public PaymentFragment() {
        // Required empty public constructor
    }

    public static PaymentFragment newInstance(String chapterId, String chapterName) {
        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        args.putString(CHAPTER_ID, chapterId);
        args.putString(CHAPTER_NAME, chapterName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPrefManager = new PrefManager(getContext());
            mChapterId = getArguments().getString(CHAPTER_ID);
            mChapterName = getArguments().getString(CHAPTER_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        mViewFlipper = view.findViewById(R.id.v_flipper);
        mCardMultilineWidget = view.findViewById(R.id.v_card_multiline_widget);
        mViewFlipper.setOnClickListener(this);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_flipper:
                handlePayment();
                break;
        }
    }

    public void handlePayment() {

        cardToSave = mCardMultilineWidget.getCard();
        if (cardToSave != null) {
            String cvc = cardToSave.getCVC();
            String number = cardToSave.getNumber();
            String expMonth = Integer.toString(cardToSave.getExpMonth());
            String expYear = Integer.toString(cardToSave.getExpYear());
            Log.e("cvc", cvc);
            Log.e("number", number);
            Log.e("expMonth", expMonth);
            Log.e("expYear", expYear);
            cardToSave = new Card(number, Integer.parseInt(expMonth), Integer.parseInt(expYear), cvc);
            mViewFlipper.showNext();
            Stripe stripe = new Stripe(getContext(), "pk_test_bZp0XABR2n7gR8WzUGcd7Z2d");

            stripe.createToken(cardToSave, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    Toast.makeText(getContext(), "Payment Unsuccessful", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(Token token) {
                    String currToken = token.toString();
                    Log.e("Id", token.getId());
                    Log.e("token value", String.valueOf(token));
                    Log.e("token", currToken);
                    byte[] data = new byte[0];
                    try {
                        data = token.getId().getBytes("UTF-8");
                        String base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getContext(), "Payment Successful", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void processPayment(String id) {
        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL)
                .create(RetrofitInterface.class);
        String token = mPrefManager.getSharedPref().getString(getContext().getResources().getString(R.string.token), null);
        String deviceId = mPrefManager.getSharedPref().getString(getContext().getResources().getString(R.string.dId), null);
        String userId = mPrefManager.getSharedPref().getString(getContext().getResources().getString(R.string.uId), null);
        String deviceType = getContext().getResources().getString(R.string.isTab);
        String payToken = id;
        String payAmount = "100";
        String payCurrency = "USD";
        String chapterId = mChapterId;
        String chapterName = mChapterName;
        String option = "checkout";
        retrofitInterface.sendPaymentId(token, userId, option, deviceId, deviceType, token, payAmount, payCurrency, chapterId, chapterName);

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}


