package cpm.qbslearning.bibleforce.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.dataModel.AccessCodeVerifyResponse;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.service.ChapterDownloadService;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.fragment.TestamentFragment.STORAGE_PERMISSION_REQUEST_CODE;

public class GetAccessTokenFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    WebView webView;
    Button doneView;
    KProgressHUD mProgressDialog;
    ImageView backView;
    TextView text;
    private String mParam1;
    private String mParam2;
    private final String url = Constants.ACTION_BASE_URL + "accesscode.php?key=";
    Dialog dialog;
    private OnAccessTokenFragmentInteractionListener mListener;
    private static final int CALLER = 7;

    public GetAccessTokenFragment() {
    }

    public static GetAccessTokenFragment newInstance(String param1, String param2) {
        GetAccessTokenFragment fragment = new GetAccessTokenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_get_access_token, container, false);
        webView = view.findViewById(R.id.v_web);
        backView = view.findViewById(R.id.v_back);
        doneView = view.findViewById(R.id.v_done);
        doneView.setOnClickListener(this);
        Log.e("url", url);

        mProgressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.7f);

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                getActivity().setProgress(progress * 1000);
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressDialog.dismiss();
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                mProgressDialog.dismiss();
                Toast.makeText(getContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onAccessTokenFragmentInteraction(null);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        String finalurl = url.concat(CommonMethods.getUserDeviceData(getContext()));
        Log.e("finalurl", finalurl);
        webView.loadUrl(finalurl);
        if (CommonMethods.isNetworkAvailable(getContext())) {
            mProgressDialog.show();
        }
        getContext().registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));

    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(receiver);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onAccessTokenFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof OnAccessTokenFragmentInteractionListener) {
            mListener = (OnAccessTokenFragmentInteractionListener) getParentFragment();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAccessTokenFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_done:
                mListener.onAccessTokenFragmentInteraction(null);
                // showDialogForToken(null);
                break;
        }
    }

    public void handlePermission() {
        int permissionCheck = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.e("permission ", "not granted");
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
        } else {
            Log.e("permission ", "granted");
            mProgressDialog.show();
            ChapterDownloadService.startChapterDownloading(getContext(), null, null, 0,CALLER);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission ", "granted");
                    mProgressDialog.show();
                    ChapterDownloadService.startChapterDownloading(getContext(), null, null, 0,CALLER);
                } else {
                    Log.e("permission", "DENIED");
                }
        }
    }

    public boolean VerifyAccessCode(Context context, String accessCode) {

        if (CommonMethods.isNetworkAvailable(context)) {
            mProgressDialog.show();
            final PrefManager pref = new PrefManager(context);
            String uId = pref.getSharedPref().getString(context.getResources().getString(R.string.uId), null);
            String token = pref.getSharedPref().getString(context.getResources().getString(R.string.token), null);
            String dId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            boolean result[] = {false};
            RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL).create(RetrofitInterface.class);
            Call<AccessCodeVerifyResponse> call = retrofitInterface.verifyAccessCode(token, uId, "checkAccessCode", dId, accessCode);
            call.enqueue(new Callback<AccessCodeVerifyResponse>() {
                @Override
                public void onResponse(Call<AccessCodeVerifyResponse> call, Response<AccessCodeVerifyResponse> response) {
                    mProgressDialog.dismiss();
                    if (response != null) {
                        AccessCodeVerifyResponse verifyResponse = response.body();
                        if (verifyResponse != null) {
                            String msg = verifyResponse.getMessage();
                            int status = verifyResponse.getStatus();
                            if (status > 0) {
                                if (dialog != null)
                                    dialog.dismiss();
                                handlePermission();
                            } else {
                                text.setText("Invalid Access code!");
                            }
                        } else
                            Toast.makeText(getContext(), "Something went wrong Please try again", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getContext(), "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AccessCodeVerifyResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                    Toast.makeText(getContext(), "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                }
            });

        } else {

            Toast.makeText(getContext(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    public void showDialogForToken(final String msg) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.accesstoken_dialog);

        text = dialog.findViewById(R.id.v_text);
        final EditText editText = dialog.findViewById(R.id.v_token);
        TextView cancelButton = dialog.findViewById(R.id.v_cancel);
        TextView okButton = dialog.findViewById(R.id.v_ok);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                hideSoftKeyboard(getActivity());
                String enterToken = editText.getText().toString();
                if (enterToken.trim().equals("")) {
                    text.setText("Access code can't be blank");
                } else{
                     //hideSoftKeyboard(getActivity());
                    VerifyAccessCode(getContext(), enterToken);
                }
            }
        });

        dialog.show();
    }

    public interface OnAccessTokenFragmentInteractionListener {
        void onAccessTokenFragmentInteraction(Uri uri);
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();

            int caller = bundle.getInt(Constants.EXTRA_CALLER, -1);
            if (bundle != null) {
                if(caller == CALLER) {
                    if (/*resultCode == 1*/true) {
                        Toast.makeText(getContext(), "Download Completed",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "Download failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
            mProgressDialog.dismiss();

        }
    };
}
