package cpm.qbslearning.bibleforce.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.kaopiz.kprogresshud.KProgressHUD;

import cpm.qbslearning.bibleforce.AboutDetailActivity;
import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.util.Constants;


public class AboutFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    TextView aboutContent;
    ImageView backView;
    TextView ameriEmail, ameriPrivacy, interEmail, interPrivacy, vEmail;
    WebView webView;
    boolean isWebOpen;
    FrameLayout v_about;
    private final String ABOUT_URL = "https://www.harpercollinschristian.com/privacy/";
    private OnAboutFragmentInteractionListener mListener;
    KProgressHUD mProgressDialog;
    private RelativeLayout relDrawerIconAbout;

    public AboutFragment() {
    }

    public static AboutFragment newInstance(String param1, String param2) {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_new, container, false);
        aboutContent = view.findViewById(R.id.v_about_content);
        aboutContent.setText(Html.fromHtml(getContext().getString(R.string.lorem)));
        webView = view.findViewById(R.id.v_about_web);
        v_about = view.findViewById(R.id.v_about);
        v_about.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
        isWebOpen = false;
        ameriEmail = view.findViewById(R.id.v_ameri_email);
        ameriPrivacy = view.findViewById(R.id.v_ameri_privacy);
        interEmail = view.findViewById(R.id.v_inter_email);
        interPrivacy = view.findViewById(R.id.v_inter_privacy);
        vEmail = view.findViewById(R.id.v_email);
        relDrawerIconAbout=view.findViewById(R.id.rel_drawer_icon_about);

        ameriEmail.setOnClickListener(this);
        ameriPrivacy.setOnClickListener(this);
        interEmail.setOnClickListener(this);
        interPrivacy.setOnClickListener(this);
        vEmail.setOnClickListener(this);
        relDrawerIconAbout.setOnClickListener(this);

        /*backView = view.findViewById(R.id.v_back);
        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("about", "back click");
                mListener.onAboutFragmentInteraction(null);
            }
        });*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onAboutFragmentInteraction(uri);
//        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutFragmentInteractionListener) {
            mListener = (OnAboutFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_ameri_email:
                handleClick("North_America_Email.html", "Email Policy");
                break;
            case R.id.v_ameri_privacy:
                handleClick("North_America_Privacy.html", "Privacy Policy");
                break;
            case R.id.v_inter_email:
                handleClick("International_Email.html", "Email Policy");
                break;
            case R.id.v_inter_privacy:
                handleClick("International_Privacy.html", "Privacy Policy");
                break;
            case R.id.v_email:
                handleClick("Terms_Of_Use.html", "Terms of use");
                break;
            case R.id.rel_drawer_icon_about:
                ((DashboardActivity) getContext()).drawerLayout.openDrawer(Gravity.LEFT);
                break;
        }
    }

    public void handleClick(String url, String title) {

        Intent intent = new Intent(getContext(), AboutDetailActivity.class);
        intent.putExtra(Constants.TITLE, title);
        intent.putExtra(Constants.ABOUT_URL, "file:///android_asset/" + url);
        startActivity(intent);
    }

    public interface OnAboutFragmentInteractionListener {
        void onAboutFragmentInteraction(Uri uri);

        void onWebAttached();
    }
}
