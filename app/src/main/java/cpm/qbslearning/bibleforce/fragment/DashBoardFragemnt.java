package cpm.qbslearning.bibleforce.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.LoginActivity;
import cpm.qbslearning.bibleforce.PageActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.adapter.CircularViewPagerAdapter;
import cpm.qbslearning.bibleforce.adapter.CoverFlowAdapter;
import cpm.qbslearning.bibleforce.adapter.DashboardViewPagerAdapter;
import cpm.qbslearning.bibleforce.carousal.CarrouselLayout;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.data.DbHelper;
import cpm.qbslearning.bibleforce.dataModel.UserProfile;
import cpm.qbslearning.bibleforce.dataModel.ViewPagerModel;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingData;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingResponse;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.service.ChapterDownloadService;
import cpm.qbslearning.bibleforce.util.CircularViewPagerHandler;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import cpm.qbslearning.bibleforce.util.FixedSpeedScroller;
import cpm.qbslearning.bibleforce.util.Game;
import de.hdodenhof.circleimageview.CircleImageView;
import io.alterac.blurkit.BlurLayout;
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.LoginActivity.BASE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PAID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.TESTAMENT_TYPE;
import static cpm.qbslearning.bibleforce.util.CommonMethods.isNetworkAvailable;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashBoardFragemnt.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashBoardFragemnt#newInstance} factory method to
 * create an instance of this fragment.
 */
//TestamentFragment.OnChildFragmentCallback
//, TestamentFragment.OnChildFragmentCallback
public class DashBoardFragemnt extends Fragment
        implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, TestamentFragment.OnChildFragmentCallback {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    RelativeLayout dashboardView, logoutView, aboutView, settingView, shareView, favouriteView, profileView;
    ImageView oldTestamentView, newTestamentView;
    public final static int LOOPS = 1000;
    private static final int CALLER = 1;

    //    public CarouselPagerAdapter adapter;
    private DashboardViewPagerAdapter dashboardViewPagerAdapter;
    public ViewPager pager;
    public static int FIRST_PAGE = 3;
    public static int count = 3;
    DbHelper mHelper;
    CarrouselLayout mCarrouselLayout;
    KProgressHUD dialog;
    ProgressDialog mProgressDialog;
    //    GoogleApiClient mGoogleApiClient;
    PrefManager manager;
    private GoogleSignInOptions mGso;
    private OnFragmentInteractionListener mListener;
    //    DrawerLayout drawer;
    ImageView drawerHandlerView;
    LinearLayout dashboardRootView;
    static boolean isChildAttached = false;
    View indicator1, indicator2, indicator3;
    private FeatureCoverFlow coverFlow;
    private ArrayList<Game> games;
    private ImageView trendingIvOne, trendingIvTwo, trendingIvThree;

    CoverFlowAdapter adapter;
    private String mChapterId;
    final static int STORAGE_PERMISSION_REQUEST_CODE = 1;
    private String mChapterName;
    private int mTestamentType = 0;
    BlurLayout blurLayout, blurDashboard;
    private CardView relDrawerIcon;
    private TextView txtNavDrawerName, txtNavDrawerEmail;
    public CircleImageView ivNavHeaderDp;
    private ArrayList<Drawable> arrPagerImageUrl = new ArrayList<android.graphics.drawable.Drawable>();
    private ArrayList<TrendingData> arrPagerTrendingData = new ArrayList<TrendingData>();

    SharedPreferences sharedPreferences;
    Bitmap bitmap;
    public String profileImage;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 1000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    private int currentItem;

    public ImageView updateProfileImage;

    public DashBoardFragemnt() {
        setReenterTransition(true);
    }

    public static DashBoardFragemnt newInstance(String param1, String param2) {
        DashBoardFragemnt fragment = new DashBoardFragemnt();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("onCreate f", "called");
        if (getArguments() != null) {

            Log.e("onCreate dashboard", "ARG_PARAM1" + getArguments().getString(ARG_PARAM1));

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        getContext().registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_dashboard, container, false);
        //  blurLayout = view.findViewById(R.id.blurLayout);
        pager = view.findViewById(R.id.viewPagerMain);


        //mLogo = (ImageView) getContext().view.findViewById(R.id.iv_nav_header_dp);

        mCarrouselLayout = view.findViewById(R.id.v_carrousal);
        indicator1 = view.findViewById(R.id.v1);
        indicator2 = view.findViewById(R.id.v2);
        indicator3 = view.findViewById(R.id.v3);

        trendingIvOne = view.findViewById(R.id.banner_iv_one);
        trendingIvTwo = view.findViewById(R.id.banner_iv_two);
        trendingIvThree = view.findViewById(R.id.banner_iv_three);

        relDrawerIcon = view.findViewById(R.id.rel_drawer_icon);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        float width = displayMetrics.widthPixels / 3;

        // width= mCarrouselLayout.getMeasuredWidth();
        //  mCarrouselLayout.setR(width);

        int pd = dp2px(getContext(), 4);
        mCarrouselLayout.setRotationX(-pd);
        mCarrouselLayout.refreshLayout();
        Log.e("pd", pd + "");

        dashboardView = view.findViewById(R.id.dashboard);
        logoutView = view.findViewById(R.id.logout);
        aboutView = view.findViewById(R.id.about);
        settingView = view.findViewById(R.id.setting);
        shareView = view.findViewById(R.id.share);
        favouriteView = view.findViewById(R.id.fav);
        profileView = view.findViewById(R.id.profile);
        blurLayout = view.findViewById(R.id.blurLayout);
        txtNavDrawerEmail = view.findViewById(R.id.txt_nav_drawer_email);
        txtNavDrawerName = view.findViewById(R.id.txt_nav_drawer_name);
        ivNavHeaderDp = view.findViewById(R.id.iv_nav_header_dp);
        //((DashboardActivity) getContext()).profilePic = view.findViewById(R.id.iv_nav_header_dp);
        //blurDashboard = view.findViewById(R.id.blur_dashboard);

        dashboardView.setOnClickListener(this);
        logoutView.setOnClickListener(this);
        aboutView.setOnClickListener(this);
        settingView.setOnClickListener(this);
        shareView.setOnClickListener(this);
        favouriteView.setOnClickListener(this);
        profileView.setOnClickListener(this);

        manager = new PrefManager(getContext());
        ((DashboardActivity) getContext()).drawerLayout = view.findViewById(R.id.drawer_layout);

        ((DashboardActivity) getContext()).profileImageView = ((DashboardActivity) getContext()).drawerLayout.findViewById(R.id.iv_nav_header_dp);
        ((DashboardActivity) getContext()).profileEmailTextview = ((DashboardActivity) getContext()).drawerLayout.findViewById(R.id.txt_nav_drawer_email);
        ((DashboardActivity) getContext()).profileNameTextview = ((DashboardActivity) getContext()).drawerLayout.findViewById(R.id.txt_nav_drawer_name);
        dashboardRootView = view.findViewById(R.id.v_dash_root);

        dialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        mProgressDialog = new ProgressDialog(getContext(), ProgressDialog.THEME_DEVICE_DEFAULT_DARK);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setTitle(getString(R.string.downloading_chapter_title));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);

        mProgressDialog.setCancelable(false);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        oldTestamentView = view.findViewById(R.id.v_old);
        newTestamentView = view.findViewById(R.id.v_new);

        trendingSetup(view);

//        FacebookSdk.sdkInitialize(getContext());

        /*mGso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();*/

        /*mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, mGso)
                .build();*/

//        final DrawerLayout drawer = view.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle =
                new ActionBarDrawerToggle(getActivity(), ((DashboardActivity) getContext()).drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        ((DashboardActivity) getContext()).drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        relDrawerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getContext()).drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        if (mParam1.equals("1")) {
//            Fragment fragment = TestamentFragment.newInstance(Integer.parseInt(mParam2), "b");
            Fragment fragment = TestamentFragment.newInstance(Integer.parseInt(mParam1), "b");
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.child_container, fragment);
            dashboardRootView.setVisibility(View.GONE);
            transaction.commit();
            isChildAttached = true;
            mListener.onTestamentFragmentAdd(Integer.parseInt(mParam2));
        }

        oldTestamentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("onclick", view.getId() + "");
                Fragment fragment = TestamentFragment.newInstance(1, "b");
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_container, fragment);
                transaction.commit();
                dashboardRootView.setVisibility(View.GONE);
                isChildAttached = true;
                mListener.onTestamentFragmentAdd(1);
            }
        });

        newTestamentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("onclick", view.getId() + "");
                Fragment fragment = TestamentFragment.newInstance(2, "b");
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_container, fragment);
                transaction.commit();
                dashboardRootView.setVisibility(View.GONE);
                isChildAttached = true;
                mListener.onTestamentFragmentAdd(2);
            }
        });

       /* pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        indicator1.setBackgroundResource(R.drawable.circle_white);
                        indicator2.setBackgroundResource(R.drawable.circle_gray);
                        indicator3.setBackgroundResource(R.drawable.circle_gray);
                        break;
                    case 1:
                        indicator3.setBackgroundResource(R.drawable.circle_gray);
                        indicator1.setBackgroundResource(R.drawable.circle_gray);
                        indicator2.setBackgroundResource(R.drawable.circle_white);
                        break;
                    case 2:
                        indicator1.setBackgroundResource(R.drawable.circle_gray);
                        indicator2.setBackgroundResource(R.drawable.circle_gray);
                        indicator3.setBackgroundResource(R.drawable.circle_white);
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/

      /*  mCarrouselLayout.setOnCarrouselItemSelectedListener(new OnCarrouselItemSelectedListener() {
            @Override
            public void selected(View view, int position) {
                Log.e("position", position + " selected");
                switch (position) {
                    case 0:
                        indicator1.setBackgroundResource(R.drawable.circle_white);
                        indicator2.setBackgroundResource(R.drawable.circle_gray);
                        indicator3.setBackgroundResource(R.drawable.circle_gray);
                        break;
                    case 1:
                        indicator3.setBackgroundResource(R.drawable.circle_gray);
                        indicator1.setBackgroundResource(R.drawable.circle_gray);
                        indicator2.setBackgroundResource(R.drawable.circle_white);
                        break;
                    case 2:
                        indicator1.setBackgroundResource(R.drawable.circle_gray);
                        indicator2.setBackgroundResource(R.drawable.circle_gray);
                        indicator3.setBackgroundResource(R.drawable.circle_white);
                        break;
                }
            }
        });*/

     /*   mCarrouselLayout.setOnCarrouselItemClickListener(new OnCarrouselItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getTag() != null && view.getTag() instanceof TrendingData) {
                    TrendingData trendingData = (TrendingData) view.getTag();
                    openTestament(trendingData.getChapterId());
                }
               *//* if (position == 0) {
                    openTestament(String.valueOf(1));
                } else {
                    openTestament(String.valueOf(2));
                }*//*
            }
        });*/

        processTrendingApi();

      /*  Blurry.with(getContext())
                .radius(50)
                .sampling(6)
                //.color(Color.argb(66, 0, 255, 255))
                .async()
                .onto(view.<ViewGroup>findViewById(R.id.drawer_layout));*/

        txtNavDrawerName.setText(((DashboardActivity) getContext()).userProfile.getFirstName() + " " +
                ((DashboardActivity) getContext()).userProfile.getMiddleName() + " " +
                ((DashboardActivity) getContext()).userProfile.getLastName());

        txtNavDrawerEmail.setText(((DashboardActivity) getContext()).userProfile.getEmail());

        if (!TextUtils.isEmpty(((DashboardActivity) getContext()).userProfile.getDpUrl())) {
            Picasso.with(getContext()).load(((DashboardActivity) getContext()).userProfile.getDpUrl()).placeholder(R.drawable.ic_user_placeholder).
                    error(R.drawable.ic_user_placeholder).into(ivNavHeaderDp);
        }

      /*  dashboardViewPagerAdapter = new DashboardViewPagerAdapter(getContext(), arrPagerImageUrl);

        // Adding the Adapter to the ViewPager
        pager.setAdapter(dashboardViewPagerAdapter);*/

        return view;
    }

    public void getProfileImage(String text) {
       /* Picasso.with(getContext()).load(text).placeholder(R.drawable.ic_user_placeholder).
                error(R.drawable.ic_user_placeholder).into(ivNavHeaderDp);*/
        // mCallback.setImage(text);
    }

    private void processTrendingApi() {
        if (isNetworkAvailable(getContext())) {
            dialog.show();
            final PrefManager pref = new PrefManager(getActivity());
            String uId = pref.getSharedPref().getString(getResources().getString(R.string.uId), null);
            String token = pref.getSharedPref().getString(getResources().getString(R.string.token), null);
            String dId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            Call<TrendingResponse> responseCall = RetrofitClient.getRetrofitClient(BASE_URL)
                    .create(RetrofitInterface.class).getTrendingChapters(token, uId, "trending", dId);
            responseCall.enqueue(new Callback<TrendingResponse>() {
                @Override
                public void onResponse(Call<TrendingResponse> call, Response<TrendingResponse> response) {
                    try {
                        TrendingResponse trendingResponse = response.body();
                        dialog.dismiss();
                        Log.e("trendingResponse", trendingResponse.getStatus() + "");
                        Log.e("trendingResponse", trendingResponse.getMessage() + "");
                        if (trendingResponse.getStatus() > 0 && trendingResponse.getTrendingData().size() > 0) {
                            for (int i = 0; i < trendingResponse.getTrendingData().size(); i++) {
                                TrendingData trendingData = trendingResponse.getTrendingData().get(i);
                                String url = trendingResponse.getServerUrl() + trendingData.getChapterThumb();
                                switch (i) {
                                    case 0:
                                        Picasso.with(getContext())
                                                .load(url)
                                                .into(trendingIvOne);
                                        arrPagerImageUrl.add(getContext().getDrawable(R.drawable.bg_dashboard_img_one));
                                        arrPagerTrendingData.add(trendingData);
                                        trendingIvOne.setTag(trendingData);
                                        break;
                                    case 1:
                                        Picasso.with(getContext())
                                                .load(url)
                                                .into(trendingIvTwo);
                                        arrPagerImageUrl.add(getContext().getDrawable(R.drawable.bg_dashboard_img_two));
                                        arrPagerTrendingData.add(trendingData);
                                        trendingIvTwo.setTag(trendingData);
                                        break;
                                    case 2:
                                        Picasso.with(getContext())
                                                .load(url)
                                                .into(trendingIvThree);
                                        arrPagerImageUrl.add(getContext().getDrawable(R.drawable.bg_dashboard_img_three));
                                        arrPagerTrendingData.add(trendingData);
                                        trendingIvThree.setTag(trendingData);
                                        break;

                                }
                            }
                            Log.e("trendingResponse", trendingResponse.toString());
                            setAdapter();
                        } else {

                            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onFailure(Call<TrendingResponse> call, Throwable t) {
                    call.cancel();
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
//            showDialogOK("Please check your internet connection",null);
        }
    }


    private int dp2px(Context context, float dip) {
        float m = context.getResources().getDisplayMetrics().density;
        return (int) (dip * m + 0.5f);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        getContext().unregisterReceiver(receiver);
    }

    @Override
    public void onClick(View view) {
        Fragment fragment = null;
        switch (view.getId()) {
            case R.id.dashboard:
                if (isChildAttached) {
                    fragment = DashBoardFragemnt.newInstance("a", "b");
                    mListener.onAddFragment(fragment, true);
                }
                ((DashboardActivity) getContext()).drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.profile:
                /*fragment = ProfileFragment.newInstance("a", "b");
                mListener.onAddFragment(fragment, false);
                ((DashboardActivity)getContext()).drawer.closeDrawer(GravityCompat.START);*/

                fragment = ProfileFragment.newInstance("a", "b");
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_container, fragment);
                transaction.commit();
                dashboardRootView.setVisibility(View.GONE);
                isChildAttached = true;
                ((DashboardActivity) getContext()).drawerLayout.closeDrawers();
                mListener.onTestamentFragmentAdd(-1);
                break;
            case R.id.share:
              /*  fragment = ShareFragment.newInstance("a", "b");
                mListener.onAddFragment(fragment, false);
                ((DashboardActivity) getContext()).drawer.closeDrawer(GravityCompat.START);*/
                fragment = ShareFragment.newInstance("a", "b");
                transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_container, fragment);
                transaction.commit();
                dashboardRootView.setVisibility(View.GONE);
                isChildAttached = true;
                ((DashboardActivity) getContext()).drawerLayout.closeDrawers();
                mListener.onTestamentFragmentAdd(-1);
                break;
            case R.id.fav:
                fragment = FavouriteFragment.newInstance("a", "b");
                transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_container, fragment);
                transaction.commit();
                dashboardRootView.setVisibility(View.GONE);
                isChildAttached = true;
               /* fragment = FavouriteFragment.newInstance("a", "b");
                mListener.onAddFragment(fragment, false);*/
                ((DashboardActivity) getContext()).drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.setting:
                /*fragment = SettingFragment.newInstance("a", "b");
                mListener.onAddFragment(fragment, false);
                ((DashboardActivity)getContext()).drawer.closeDrawer(GravityCompat.START);*/

                fragment = SettingFragment.newInstance("a", "b");
                transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_container, fragment);
                transaction.commit();
                dashboardRootView.setVisibility(View.GONE);
                isChildAttached = true;
                ((DashboardActivity) getContext()).drawerLayout.closeDrawers();
                mListener.onTestamentFragmentAdd(-1);

                break;
            case R.id.about:
               /* fragment = AboutFragment.newInstance("a", "b");
                mListener.onAddFragment(fragment, false);
                ((DashboardActivity) getContext()).drawer.closeDrawer(GravityCompat.START);*/
                fragment = AboutFragment.newInstance("a", "b");
                transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_container, fragment);
                transaction.commit();
                dashboardRootView.setVisibility(View.GONE);
                isChildAttached = true;
                ((DashboardActivity) getContext()).drawerLayout.closeDrawers();
                mListener.onTestamentFragmentAdd(-1);
                break;
            case R.id.logout:
                dialog.show();
                UserProfile userProfile = new UserProfile(getContext());
                manager.getEditor().remove(getString(R.string.uId)).apply();
                manager.getEditor().remove(getString(R.string.token)).apply();
                String loginType = manager.getSharedPref().getString(getString(R.string.login_type), null);

                if (loginType != null) {
                    Log.e("login Type", loginType);
                    switch (loginType) {
                        case "1":
                            if (manager.getSharedPref().getString(getString(R.string.uId), null) == null) {
                                userProfile.removeProfile();
                                dialog.dismiss();
                                getActivity().finish();
                                startActivity(new Intent(getContext(), LoginActivity.class));
                            }
                            break;

                        case "2":
                            /*if (AccessToken.getCurrentAccessToken() == null) {
                                Log.e("token ", "null");
                            }

                            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                                    .Callback() {
                                @Override
                                public void onCompleted(GraphResponse graphResponse) {
                                    LoginManager.getInstance().logOut();
                                    AccessToken.setCurrentAccessToken(null);
                                    Log.e("facebook ", "logout");
                                    if (manager != null && manager.getSharedPref().getString(getString(R.string.uId), null) == null) {
                                        startActivity(new Intent(getContext(), LoginActivity.class));
                                        dialog.dismiss();
                                        getActivity().finish();
                                    }
                                }
                            }).executeAsync();
                            userProfile.removeProfile();*/
                            break;

                        case "3":
                            /*Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                    new ResultCallback<Status>() {
                                        @Override
                                        public void onResult(Status status) {
                                            if (manager.getSharedPref().getString(getString(R.string.uId), null) == null) {
                                                startActivity(new Intent(getContext(), LoginActivity.class));
                                                dialog.dismiss();
                                                getActivity().finish();
                                            }
                                        }
                                    });
                            userProfile.removeProfile();*/
                            break;
                    }
                }
                ((DashboardActivity) getContext()).drawerLayout.closeDrawer(GravityCompat.START);
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        getContext().registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));

       /* Picasso.with(getContext()).load(((DashboardActivity) getContext()).userProfile.getDpUrl()).placeholder(R.drawable.ic_user_placeholder).
                error(R.drawable.ic_user_placeholder).into(ivNavHeaderDp);*/

        /*SharedPreferences sp = getContext().getSharedPreferences("profile", Context.MODE_PRIVATE);
        String path = sp.getString("imageurl", "fdgf");
        Picasso.with(getContext()).load(path).placeholder(R.drawable.ic_user_placeholder).
                error(R.drawable.ic_user_placeholder).into(ivNavHeaderDp);*/

        //  bitmap = BitmapFactory.decodeFile(path);
        //ivNavHeaderDp.setImageBitmap(bitmap);

        Log.e("onResume F", "called");
    }

    @Override
    public void onPause() {
        super.onPause();
//        getContext().unregisterReceiver(receiver);
        Log.e("onPause F", "called");
    }

    @Override
    public void onStop() {
        blurLayout.pauseBlur();
        //blurDashboard.pauseBlur();
        super.onStop();
        Log.e("onStop F", "called");
    }

    @Override
    public void onStart() {
        super.onStart();
        blurLayout.startBlur();
        blurLayout.lockView();
       /* Picasso.with(getContext()).load(((DashboardActivity) getContext()).userProfile.getDpUrl()).placeholder(R.drawable.ic_user_placeholder).
                error(R.drawable.ic_user_placeholder).into(ivNavHeaderDp);*/
       /* blurDashboard.startBlur();
        blurDashboard.lockView();*/
        Log.e("onStart F", "called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();*/
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void openDrawer() {
        ((DashboardActivity) getContext()).drawerLayout.openDrawer(GravityCompat.START);
        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                Log.e("open drawer", "open drawer");
                if (!TextUtils.isEmpty(((DashboardActivity) getContext()).userProfile.getDpUrl())) {
                    Picasso.with(getContext()).load(((DashboardActivity) getContext()).userProfile.getDpUrl()).placeholder(R.drawable.ic_user_placeholder).
                            error(R.drawable.ic_user_placeholder).into(ivNavHeaderDp);
                }
            }
        }, 5000);

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onTestamentFragmentAdd(int testamentType);

        void onAddFragment(Fragment fragment, boolean isDashboard);
    }

    public void trendingSetup(View v) {
    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
            }

            @Override
            public void onScrolling() {
                Log.i("MainActivity", "scrolling");
            }
        };
    }

    private void settingDummyData() {
        games = new ArrayList<>();
        games.add(new Game(R.drawable.card_1_2x, ""));
        games.add(new Game(R.drawable.card_2_2x, ""));
        games.add(new Game(R.drawable.card_3_2x, ""));
    }

    public void openTestament(String chapterId) {

        int isPaid = -9;
        Cursor cursor = getContext().getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{DbContract.Chapter.IS_PAID, DbContract.Chapter.CHAPTER_NAME, DbContract.Chapter.TESTAMENT_TYPE}, DbContract.Chapter.CHAPTER_ID + "=?", new String[]{chapterId}, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                if (cursor.moveToNext()) {
                    isPaid = cursor.getInt(cursor.getColumnIndex(IS_PAID));
                    mChapterName = cursor.getString(cursor.getColumnIndex(CHAPTER_NAME));
                    mTestamentType = cursor.getInt(cursor.getColumnIndex(TESTAMENT_TYPE));
                }
            }
            if (isPaid == getContext().getResources().getInteger(R.integer.FREE)) {
                mChapterId = chapterId;
                handlePermission();
            } else {
                int[] status = CommonMethods.getStatus(getActivity(), chapterId);
                if (status[0] == getResources().getInteger(R.integer.DOWNLOADED) /*|| status[0] == 1*/) {
                    gotoNextActivity(mChapterId, mChapterName);
                } else {
                    CommonMethods.showAlert("Premium chapter!", "You can download it from Testaments list.", "Open Testaments", null, new CommonMethods.ActionCallback() {
                        @Override
                        public void onPositiveBtnClick() {
                            if (mTestamentType == 0) {
                                mTestamentType = 1;
                            }
                            Fragment fragment = TestamentFragment.newInstance(mTestamentType, "b");
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            transaction.replace(R.id.child_container, fragment);
                            transaction.commit();
                            dashboardRootView.setVisibility(View.GONE);
                            isChildAttached = true;
                            mListener.onTestamentFragmentAdd(mTestamentType);
                        }

                        @Override
                        public void onNegativeBtnClick() {

                        }
                    }, getActivity());
                }
            }
            cursor.close();
        }
    }

    public void handlePermission() {
        int permissionCheck = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.e("permission ", "not granted");
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
        } else {
            processTestament();
        }
    }

    private void processTestament() {
        int[] status = CommonMethods.getStatus(getActivity(), mChapterId);
        dialog.dismiss();
        if (status[0] == getResources().getInteger(R.integer.DOWNLOADED) /*|| status[0] == 1*/) {
            gotoNextActivity(mChapterId, mChapterName);
        } else {
//            dialog.show();
//            mProgressDialog.setProgress(0);
            mProgressDialog.show();
            ChapterDownloadService.startChapterDownloading(getContext(), null, mChapterId, 0, CALLER);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("STORAGE_PERMISSION", "" + STORAGE_PERMISSION_REQUEST_CODE);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    processTestament();
                } else {
                    // This is Case 1 again as Permission is not granted by user

                    //Now further we check if used denied permanently or not
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showDialogOK("Some Permissions are required for downloading books",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                handlePermission();
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                // proceed with logic by disabling the related features or quit the app.
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                });
                    } else {
                        explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                    }

                }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void explain(String msg) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        dialog.create().dismiss();

                    }
                });
        dialog.show();
    }

    private void gotoNextActivity(String id, String title) {
        Intent intent = new Intent(getContext(), PageActivity.class);
        intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, 1);
        intent.putExtra(Constants.EXTRA_CHAPTER_ID, id);
        intent.putExtra(Constants.EXTRA_CHAPTER_TITLE, title);
        startActivity(intent);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            //Log.e("TESTAMENT ", "onReceive");
            Log.e("onReceive", "onReceive called from DashboardFragment");
            if (bundle != null) {
                //Log.e("onRecieve ", "intent not null");
                int resultCode = bundle.getInt(ChapterDownloadService.RESULT);
                String chapterId = bundle.getString(Constants.EXTRA_CHAPTER_ID);
                int position = bundle.getInt(Constants.EXTRA_ITEM_POSITION, -1);
                int caller = bundle.getInt(Constants.EXTRA_CALLER, -1);
                int requestCode = bundle.getInt(Constants.DOWNLOAD_SERVICE_REQUEST_CODE, -1);
                int percentage = bundle.getInt(Constants.DOWNLOAD_PERCENTAGE, 0);
                if (caller == CALLER) {
                    if (requestCode == Constants.DOWNLOAD_COMPLETE) {
                        if (/*resultCode == 1*/true) {
                            ContentValues values = new ContentValues();
                            values.put(DbContract.Chapter.IS_DOWNLOADED, getResources().getInteger(R.integer.DOWNLOADED));
                            //Log.e("OnRecieve", resultCode + "");
                            int id = getContext().getContentResolver().update(DbContract.Chapter.CONTENT_URI,
                                    values, CHAPTER_ID + " =?", new String[]{chapterId});
                            if (id > 0) {
                                // mHolder.buttonImage.setImageResource(R.drawable.view);
                                gotoNextActivity(mChapterId, mChapterName);
                            }
                            Toast.makeText(getContext(), "Download Completed",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getContext(), "Download failed",
                                    Toast.LENGTH_LONG).show();
                        }
                        mProgressDialog.setProgress(0);
                        mProgressDialog.dismiss();
                    } else if (requestCode == Constants.PERCENTAGE) {
                        Log.e("PERCENTAGE", "PERCENTAGE_DOWNLOAD" + percentage);
                            /*mProgressDialog = new ProgressDialog(getContext());
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setMessage("DOWNLOADING CHAPTER");
                            mProgressDialog.setMax(100);*/
                        // mProgressDialog.setProgressPercentFormat("%");
                        mProgressDialog.setTitle(getString(R.string.downloading_chapter_title));
                        mProgressDialog.setProgress(percentage);

//                            mProgressDialog.setCancelable(false);
                        // mProgressDialog.show();
                    }
                }
            }
//            mHolder.buttonImage.setEnabled(true);
            // mHolder.progressBar.setVisibility(View.GONE);
//            dialog.dismiss();
        }
    };

    private void setAdapter() {
       /* dashboardViewPagerAdapter = new DashboardViewPagerAdapter(getContext(), arrPagerImageUrl, arrPagerTrendingData, DashBoardFragemnt.this);
        pager.setAdapter(dashboardViewPagerAdapter);
        setViewPagerScroll();*/

//        final ViewPager viewPager =  (ViewPager) findViewById(R.id.activity_main_view_pager);
        pager.setAdapter(new CircularViewPagerAdapter(getContext(), ((DashboardActivity) getContext()).getSupportFragmentManager(), ViewPagerModel.createSampleMemes(arrPagerTrendingData), DashBoardFragemnt.this, arrPagerTrendingData));

        final CircularViewPagerHandler circularViewPagerHandler = new CircularViewPagerHandler(pager);
        circularViewPagerHandler.setOnPageChangeListener(createOnPageChangeListener());
        pager.setOnPageChangeListener(circularViewPagerHandler);

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        pager.setCurrentItem(currentItem, true);
                        Log.d("ViewPage 11", "current = " + currentItem);
                    }
                });
            }
        }, 2000, 3000);
    }

    private ViewPager.OnPageChangeListener createOnPageChangeListener() {
        //  final TextView currentPageText = (TextView) findViewById(R.id.activity_main_current_page_text);
        return new ViewPager.OnPageChangeListener() {
            @SuppressLint("StringFormatMatches")
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                //    currentPageText.setText(getString(R.string.current_page, position));

                currentItem = position;

            }

            @Override
            public void onPageSelected(final int position) {
                switch (position) {
                    case 1:
                    case 4:
                        indicator1.setBackgroundResource(R.drawable.circle_white);
                        indicator2.setBackgroundResource(R.drawable.circle_gray);
                        indicator3.setBackgroundResource(R.drawable.circle_gray);
                        break;
                    case 2:
                        indicator3.setBackgroundResource(R.drawable.circle_gray);
                        indicator1.setBackgroundResource(R.drawable.circle_gray);
                        indicator2.setBackgroundResource(R.drawable.circle_white);
                        break;
                    case 3:
                        indicator1.setBackgroundResource(R.drawable.circle_gray);
                        indicator2.setBackgroundResource(R.drawable.circle_gray);
                        indicator3.setBackgroundResource(R.drawable.circle_white);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(final int state) {
            }
        };
    }

    private void setViewPagerScroll() {

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            Interpolator sInterpolator = new AccelerateInterpolator();
            FixedSpeedScroller scroller = new FixedSpeedScroller(pager.getContext(), sInterpolator);
            // scroller.setFixedDuration(5000);
            mScroller.set(pager, scroller);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                pager.setCurrentItem(currentPage, true);
                if (currentPage == 3) {
                    currentPage = 0;
                } else {
                    currentPage++;
                }
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

}