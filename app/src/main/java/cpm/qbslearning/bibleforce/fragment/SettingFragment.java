package cpm.qbslearning.bibleforce.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import cpm.qbslearning.bibleforce.AccessCodeActivity;
import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.HowUseThisApp;
import cpm.qbslearning.bibleforce.NavigationHintsActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import io.alterac.blurkit.BlurKit;
import io.alterac.blurkit.BlurLayout;

import static cpm.qbslearning.bibleforce.util.Constants.IS_AUDIO_ON;
import static cpm.qbslearning.bibleforce.util.Constants.IS_NOTIFICATION_ON;


public class SettingFragment extends Fragment implements View.OnClickListener,
        GetAccessTokenFragment.OnAccessTokenFragmentInteractionListener, NavigationHintsFragment.OnNavigationHintFragmentInteractionListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    ImageView backView, notificationImage, audioImage;
    RelativeLayout getTokenView, rel_blur;
    LinearLayout settingsView;
    FrameLayout webContainerView;
    androidx.appcompat.widget.SwitchCompat audioSwitch, notificationSwitch;
    TextView audioView, notificationView;
    PrefManager prefManager;
    RelativeLayout navigationHints, howUseAppView;
    private OnSettingFragmentInteractionListener mListener;
    private BlurLayout blurLayoutSettings;
    private RelativeLayout relDrawerIconSettings;

    public SettingFragment() {
    }

    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        prefManager = new PrefManager(getContext());
        BlurKit.init(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings_new, container, false);
        //backView = view.findViewById(R.id.v_back);
        audioSwitch = view.findViewById(R.id.v_audio_switch);
        notificationImage = view.findViewById(R.id.iv_notification);
        audioImage = view.findViewById(R.id.iv_audioOff);
        notificationSwitch = view.findViewById(R.id.v_switch);
        audioView = view.findViewById(R.id.v_audio_text);
        notificationView = view.findViewById(R.id.v_notification_text);
        settingsView = view.findViewById(R.id.v_setting_view);
        getTokenView = view.findViewById(R.id.v_get_token);
        getTokenView.setOnClickListener(this);
        howUseAppView = view.findViewById(R.id.v_use_app);
        relDrawerIconSettings = view.findViewById(R.id.rel_drawer_icon_settings);

        // blurLayoutSettings = view.findViewById(R.id.blurLayout_settings);
        /*blurLayoutSettings.invalidate();
        blurLayoutSettings.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                blurLayoutSettings.setVisibility(View.VISIBLE);
            }
        }, 1000);*/

        howUseAppView.setOnClickListener(this);
        relDrawerIconSettings.setOnClickListener(this);
        /*backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("settings", "back click");
                mListener.onSettingFragmentInteraction(null);
            }
        });*/
        webContainerView = view.findViewById(R.id.token_container);
        navigationHints = view.findViewById(R.id.v_nav_hint);
        navigationHints.setOnClickListener(this);

        if (!prefManager.getSharedPref().

                contains(IS_AUDIO_ON)) {
            prefManager.getEditor().putBoolean(IS_AUDIO_ON, true).apply();
            audioSwitch.setChecked(true);
        } else {
            audioSwitch.setChecked(prefManager.getSharedPref().getBoolean(IS_AUDIO_ON, false));

            if (audioSwitch.isChecked()) {
                audioView.setText("Audio ON");
                audioImage.setImageResource(R.drawable.ic_audio_on);
            } else {
                audioView.setText("Audio OFF");
                audioImage.setImageResource(R.drawable.ic_audio_off);
            }
        }

        if (!prefManager.getSharedPref().

                contains(IS_NOTIFICATION_ON)) {
            prefManager.getEditor().putBoolean(IS_NOTIFICATION_ON, true).apply();
            notificationSwitch.setChecked(true);
        } else {
            notificationSwitch.setChecked(prefManager.getSharedPref().getBoolean(IS_NOTIFICATION_ON, false));
            if (notificationSwitch.isChecked()) {
                notificationView.setText("Notification ON");
                notificationImage.setImageResource(R.drawable.ic_notification_on);

            } else {
                notificationView.setText("Notification OFF");
                notificationImage.setImageResource(R.drawable.ic_notification_off);
            }
        }
//        notificationSwitch.setChecked(true);
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    notificationView.setText("Notification ON");
                    notificationImage.setImageResource(R.drawable.ic_notification_on);
                } else {
                    notificationView.setText("Notification OFF");
                    notificationImage.setImageResource(R.drawable.ic_notification_off);
                }
                prefManager.getEditor().putBoolean(IS_NOTIFICATION_ON, isChecked).apply();
            }
        });
        audioSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    audioView.setText("Audio ON");
                    audioImage.setImageResource(R.drawable.ic_audio_on);
                } else {
                    audioView.setText("Audio OFF");
                    audioImage.setImageResource(R.drawable.ic_audio_off);
                }
                prefManager.getEditor().putBoolean(IS_AUDIO_ON, b).apply();
            }
        });

       /* relDrawerIconSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getContext()).drawer.openDrawer(Gravity.LEFT);
            }
        });*/

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSettingFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSettingFragmentInteractionListener) {
            mListener = (OnSettingFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationHintFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_get_token:
                openWebViewForGettingToken();
                break;
            case R.id.v_nav_hint:
                navigationHints();
                break;
            case R.id.v_use_app:
                Intent intent = new Intent(getContext(), HowUseThisApp.class);
                getContext().startActivity(intent);
                break;
            case R.id.rel_drawer_icon_settings:
                ((DashboardActivity) getContext()).drawerLayout.openDrawer(Gravity.LEFT);
                break;
        }
    }

    private void navigationHints() {

        Intent intent = new Intent(getContext(), NavigationHintsActivity.class);
        getContext().startActivity(intent);
//        getChildFragmentManager()
//                .beginTransaction()
//                .add(R.id.token_container, NavigationHintsFragment.newInstance("a", "b"))
//                .commit();
//        mListener.onSettingFragmentChildAdded(true);
//
//        settingsView.setVisibility(View.GONE);
//        webContainerView.setVisibility(View.VISIBLE);
    }

    public void openWebViewForGettingToken() {
//                getChildFragmentManager()
//                        .beginTransaction()
//                        .add(R.id.token_container,GetAccessTokenFragment.newInstance("a","b"))
//                        .commit();
//        mListener.onSettingFragmentChildAdded(true);
//
//        settingsView.setVisibility(View.GONE);
//        webContainerView.setVisibility(View.VISIBLE);
        Intent intent = new Intent(getContext(), AccessCodeActivity.class);
        intent.putExtra("isThisFromSetting", true);
        getContext().startActivity(intent);
    }

    @Override
    public void onAccessTokenFragmentInteraction(Uri uri) {
        webContainerView.setVisibility(View.GONE);
        settingsView.setVisibility(View.VISIBLE);
        mListener.onSettingFragmentChildAdded(false);
    }

    @Override
    public void onNavigationHintFragmentInteraction(Uri uri) {
        webContainerView.setVisibility(View.GONE);
        settingsView.setVisibility(View.VISIBLE);
        mListener.onSettingFragmentChildAdded(false);
    }

    public interface OnSettingFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSettingFragmentInteraction(Uri uri);

        void onSettingFragmentChildAdded(boolean isAdded);

    }

    @Override
    public void onStart() {
        super.onStart();
        /*blurLayoutSettings.startBlur();
        blurLayoutSettings.lockView();*/
    }

    @Override
    public void onStop() {
//        blurLayoutSettings.pauseBlur();
        super.onStop();
    }
}
