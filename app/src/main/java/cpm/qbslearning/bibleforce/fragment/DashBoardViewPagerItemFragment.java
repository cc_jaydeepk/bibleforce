package cpm.qbslearning.bibleforce.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.dataModel.ViewPagerModel;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingData;
import cpm.qbslearning.bibleforce.util.CommonMethods;

/**
 * User: tobiasbuchholz
 * Date: 18.09.14 | Time: 11:02
 */
public class DashBoardViewPagerItemFragment extends Fragment {
    private static final String BUNDLE_KEY_TITLE = "bundle_key_title";
    private static final String BUNDLE_KEY_IMAGE_RESOURCE_ID = "bundle_key_image_resource_id";
    private static DashBoardFragemnt dashBoardFragemntMain;
    private String mTitle;
    private String mChapterID;
    private int mImageResourceId;
    private static ArrayList<TrendingData> arrTrendingData;

    public static Fragment instantiateWithArgs(final Context context, final ViewPagerModel meme, DashBoardFragemnt dashBoardFragemnt, ArrayList<TrendingData> trendingData) {
        final DashBoardViewPagerItemFragment fragment = (DashBoardViewPagerItemFragment) instantiate(context, DashBoardViewPagerItemFragment.class.getName());
        final Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_TITLE, meme.mTitle);
        args.putInt(BUNDLE_KEY_IMAGE_RESOURCE_ID, meme.mImageResourceId);
        args.putString("chapterID", meme.mChapterID);
        fragment.setArguments(args);
        dashBoardFragemntMain = dashBoardFragemnt;
        arrTrendingData = trendingData;
        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArguments();
    }

    private void initArguments() {
        final Bundle arguments = getArguments();
        if (arguments != null) {
            mTitle = arguments.getString(BUNDLE_KEY_TITLE);
            mImageResourceId = arguments.getInt(BUNDLE_KEY_IMAGE_RESOURCE_ID);
            mChapterID = arguments.getString("chapterID");
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.raw_dashboard_image, container, false);
        initViews(view);
        return view;
    }

    private void initViews(final View view) {
        initText(view);
        initImage(view);
        initLayoutDownload(view);
    }

    private void initText(final View view) {
        final TextView titleText = (TextView) view.findViewById(R.id.txtPagerTitle);
        titleText.setText(mTitle);
    }

    private void initImage(final View view) {
        final ImageView backgroundImage = (ImageView) view.findViewById(R.id.imageViewMain);
        backgroundImage.setImageResource(mImageResourceId);
    }

    private void initLayoutDownload(final View view) {

        final LinearLayout llDownload = (LinearLayout) view.findViewById(R.id.llDashboardRawRead);
        final ImageView ivdashboardRawRead = view.findViewById(R.id.ivdashboardRawRead);
        final TextView txtDashboardRead = view.findViewById(R.id.txtDashboardRead);

        int[] status = CommonMethods.getStatus(getContext(), mChapterID);
        if (status[0] == getContext().getResources().getInteger(R.integer.DOWNLOADED)) {
            ivdashboardRawRead.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_read));
            txtDashboardRead.setText("Read");
        } else {
            ivdashboardRawRead.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_download));
            txtDashboardRead.setText("Download");
        }
        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashBoardFragemnt) dashBoardFragemntMain).openTestament(mChapterID);
            }
        });
    }
}
