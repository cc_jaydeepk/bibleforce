package cpm.qbslearning.bibleforce.crausal;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by rmn on 08-08-2017.
 */

public class CarouselLayout extends LinearLayout {
        private float scale = CarouselPagerAdapter.BIG_SCALE;

        public CarouselLayout(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public CarouselLayout(Context context) {
            super(context);
        }

        public void setScaleBoth(float scale) {
            this.scale = scale;
            this.invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            // The main mechanism to display scale animation, you can customize it as your needs
            int w = this.getWidth();
            int h = this.getHeight();
            canvas.scale(scale, scale, w/3, h/3);
        }
    }


