package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import cpm.qbslearning.bibleforce.R;

/**
 * Created by rmn on 05-08-2017.
 */

public class ViewPagerAdapter extends PagerAdapter {

    int mSize;
    Context mContext;
    LinearLayout mLinearLayout;
     public ViewPagerAdapter(Context context, LinearLayout linearLayout,int size) {
         mContext=context;
         mLinearLayout=linearLayout;
         mSize=size;
     }

    @Override
    public int getCount() {
        return mSize;
    }

    /**
     * Create the page for the given position.  The adapter is responsible
     * for adding the view to the container given here, although it only
     * must ensure this is done by the time it returns from
     * {@link #finishUpdate(ViewGroup)}.
     *
     * @param container The containing View in which the page will be shown.
     * @param position  The page position to be instantiated.
     * @return Returns an Object representing the new page.  This does not
     * need to be a View, but can be some other container of the page.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater layoutInflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.demo_1,container,false);
        setImage(view, position);
        container.addView(view,position);
        return view;
    }

    public void setImage(View view, int position) {
        switch (position) {
            case 0:  view.setBackgroundResource(R.drawable.tutorial1_1x);
                break;
            case 1:  view.setBackgroundResource(R.drawable.tutorial2_1x);
                break;
            case 2:  view.setBackgroundResource(R.drawable.tutorial3_up_n_1x);
                break;

        }

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

             View view = (View) object;
            container.removeView(view);

    }
}
