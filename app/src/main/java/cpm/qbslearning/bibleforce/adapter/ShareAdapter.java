package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.dataModel.ShareChapterByDate;

/**
 * Created by rmn on 21-09-2017.
 */

public class ShareAdapter extends RecyclerView.Adapter<ShareAdapter.Holder> {

    private Context mContext;
    private List<ShareChapterByDate> mChapterByDateList;

    public ShareAdapter(Context context) {
        mContext = context;
        mChapterByDateList = new ArrayList<>();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.share_chapter, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ShareChapterByDate chapterByDate = mChapterByDateList.get(position);
        holder.dateView.setText(chapterByDate.getDate());
        holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        holder.recyclerView.setAdapter(holder.shareChildAdapter);
        holder.shareChildAdapter.setData(chapterByDate.getImageList());
    }

    public void setData(List<ShareChapterByDate> chapterByDateList) {
        mChapterByDateList = chapterByDateList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mChapterByDateList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView dateView;
        ShareChildAdapter shareChildAdapter;
        public Holder(View itemView) {
            super(itemView);
            shareChildAdapter =new ShareChildAdapter(mContext);
            recyclerView = itemView.findViewById(R.id.v_recycle_child);
            dateView = itemView.findViewById(R.id.v_date);
        }
    }
}
