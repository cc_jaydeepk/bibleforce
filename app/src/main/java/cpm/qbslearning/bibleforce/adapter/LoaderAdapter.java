package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;

import androidx.loader.content.CursorLoader;


/**
 * Created by rmn on 10-08-2017.
 */

public class LoaderAdapter extends CursorLoader {
    /**
     * Creates an empty unspecified CursorLoader.  You must follow this with
     * calls to {@link #setUri(Uri)}, {@link #setSelection(String)}, etc
     * to specify the query to perform.
     *
     * @param context
     */
    public LoaderAdapter(Context context) {
        super(context);
    }

}
