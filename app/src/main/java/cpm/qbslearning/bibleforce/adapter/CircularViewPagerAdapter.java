package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.dataModel.ViewPagerModel;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingData;
import cpm.qbslearning.bibleforce.fragment.DashBoardFragemnt;
import cpm.qbslearning.bibleforce.fragment.DashBoardViewPagerItemFragment;


public class CircularViewPagerAdapter extends BaseCircularViewPagerAdapter<ViewPagerModel> {
    private final Context mContext;
    DashBoardFragemnt dashBoardFragemnt;
    private ArrayList<TrendingData> trendingData;

    public CircularViewPagerAdapter(final Context context, final FragmentManager fragmentManager, final List<ViewPagerModel> memes, DashBoardFragemnt dashBoardFragemnt, ArrayList<TrendingData> trendingData) {
        super(fragmentManager, memes, fragmentManager, trendingData);
        mContext = context;
        this.dashBoardFragemnt = dashBoardFragemnt;
        this.trendingData = trendingData;
    }

    @Override
    protected Fragment getFragmentForItem(final ViewPagerModel meme) {
        return DashBoardViewPagerItemFragment.instantiateWithArgs(mContext, meme, dashBoardFragemnt, trendingData);
    }
}