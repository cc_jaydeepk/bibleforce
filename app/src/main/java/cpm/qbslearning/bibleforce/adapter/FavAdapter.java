package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

import cpm.qbslearning.bibleforce.PageActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.util.Constants;

/**
 * Created by rmn on 21-09-2017.
 */

public class FavAdapter extends RecyclerView.Adapter<FavAdapter.Holder> {

    Context mContext;
    List<Integer> favImageList;
    Cursor mCursor;
    Map<String, Integer> mFavThumbList;

    public FavAdapter(Context context) {
        mContext = context;
        mFavThumbList = Constants.FAV_CHAPTER_THUMB_LIST_MAP;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.fav_chapter, null));
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        mCursor.moveToPosition(position);
        String thumbFile = mCursor.getString(mCursor.getColumnIndex(DbContract.Chapter.CHAPTER_THUMB_IMAGE));
        String testamentType = mCursor.getString(mCursor.getColumnIndex(DbContract.Chapter.TESTAMENT_TYPE));
        String cId = mCursor.getString(mCursor.getColumnIndex(DbContract.Chapter.CHAPTER_ID));
        String title = mCursor.getString(mCursor.getColumnIndex(DbContract.Chapter.CHAPTER_NAME));
        holder.cId = cId;
        holder.testamentType = testamentType;
        holder.title = title;
        String thumbFileImage[] = thumbFile.split(".jpg");
        Log.e("thumbFileImage", thumbFile + " : " + thumbFileImage.length + " : " + thumbFileImage[0]);
        mFavThumbList.get(thumbFileImage[0]);
        if (mFavThumbList.size() > 0 && mFavThumbList.get(thumbFileImage[0]) != null) {
            holder.imageView.setImageResource(mFavThumbList.get(thumbFileImage[0]));
        }
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoNextActivity(holder.cId, holder.title, holder.testamentType);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mCursor != null) {
            Log.e("getItemCount", mCursor.getCount() + "");
            return mCursor.getCount();
        }

        Log.e("getItemCount", "0");
        return 0;
    }

    public void setData(Cursor cursor) {
        mCursor = cursor;
        if (mCursor != null)
            Log.e("mCursor", mCursor.getCount() + "");
        else
            Log.e("mCursor", "null");
        notifyDataSetChanged();
    }

    private void gotoNextActivity(String id, String title, String testamentType) {
        Intent intent = new Intent(mContext, PageActivity.class);
        intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, testamentType);
        intent.putExtra(Constants.EXTRA_CHAPTER_ID, id);
        intent.putExtra(Constants.EXTRA_CHAPTER_TITLE, title);
        mContext.startActivity(intent);
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        String cId, title, testamentType;

        public Holder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.v_fav_ch);
        }
    }


}
