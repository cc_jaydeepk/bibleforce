package cpm.qbslearning.bibleforce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.dataModel.ViewPagerModel;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingData;

abstract class BaseCircularViewPagerAdapter<Item> extends FragmentStatePagerAdapter {
    private List<Item> mItems;
    private ArrayList<TrendingData> arrTrending;

    public BaseCircularViewPagerAdapter(final FragmentManager fragmentManager, final List<Item> items, FragmentManager fragmentManager1, ArrayList<TrendingData> trendingData) {
        super(fragmentManager);
        mItems = items;
        arrTrending = trendingData;
    }

    protected abstract Fragment getFragmentForItem(final Item item);

    @Override
    public Fragment getItem(final int position) {
        final int itemsSize = mItems.size();
        if (position == 0) {
            return getFragmentForItem(mItems.get(itemsSize - 1));
        } else if (position == itemsSize + 1) {
            return getFragmentForItem(mItems.get(0));
        } else {
            return getFragmentForItem(mItems.get(position - 1));
        }
    }

    @Override
    public int getCount() {
        final int itemsSize = mItems.size();
        return itemsSize > 1 ? itemsSize + 2 : itemsSize;
    }

    public int getCountWithoutFakePages() {
        return mItems.size();
    }

    public void setItems(final List<Item> items) {
        mItems = items;
        notifyDataSetChanged();
    }
}