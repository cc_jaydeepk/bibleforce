package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import cpm.qbslearning.bibleforce.R;

/**
 * Created by rmn on 06-08-2017.
 */

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.Holder> {

    Context mContext;
    public RecycleAdapter(Context context){
        mContext=context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.trends_layout,null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        switch (position){
            case 0:holder.imageView.setImageResource(R.drawable.card_1_2x);
                Log.e("position",position+"card_1");
                break;
            case 1:holder.imageView.setImageResource(R.drawable.card_2_2x);
                Log.e("position",position+"card_2");
                break;
            case 2 : holder.imageView.setImageResource(R.drawable.card_3_2x);
                Log.e("position",position+"card_3");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class Holder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public Holder(View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.v_trend_image);
        }
    }
}
