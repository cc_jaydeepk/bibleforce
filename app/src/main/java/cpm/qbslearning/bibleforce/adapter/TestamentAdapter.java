package cpm.qbslearning.bibleforce.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

import cpm.qbslearning.bibleforce.PageActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.ActionResponse;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import io.alterac.blurkit.BlurKit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_FAVOURITE;
import static cpm.qbslearning.bibleforce.util.Constants.ACTION_BASE_URL;
import static cpm.qbslearning.bibleforce.util.Constants.CHAPTER_IMAGE_LIST;
import static cpm.qbslearning.bibleforce.util.Constants.CHAPTER_THUMB_LIST_MAP;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_FAV;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_SHARE;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_UN_FAV;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_WATCHED;

import com.google.android.material.imageview.ShapeableImageView;
import com.squareup.picasso.Picasso;
import com.stripe.android.model.Card;

/**
 * Created by rmn on 10-08-2017.
 */

public class TestamentAdapter extends RecyclerView.Adapter<TestamentAdapter.Holder> {

    private Context mContext;
    private Cursor mCursor;
    private int mTestamentType;
    private DownloadCallback mCallback;
    private ProgressDialog mProgressDialog;
    private List<Integer> chapterList;
    private Map<String, Integer> mThumbList;
    PrefManager prefManager;

    public TestamentAdapter(Context context, DownloadCallback callback, int testamentType) {
        mContext = context;
        mTestamentType = testamentType;
        mCallback = callback;
        mThumbList = CHAPTER_THUMB_LIST_MAP;
        chapterList = CHAPTER_IMAGE_LIST;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.testament_chapter, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, @SuppressLint("RecyclerView") final int position) {
        prefManager = new PrefManager(mContext);
        mCursor.moveToPosition(position);
        final String id = mCursor.getString(mCursor.getColumnIndex(CHAPTER_ID));
        String thumbFile = mCursor.getString(mCursor.getColumnIndex(DbContract.Chapter.CHAPTER_THUMB_IMAGE));
        final String thumbFileImage[] = thumbFile.split(".jpg");

        Log.e("thumbFileImage", thumbFile + " : " + thumbFileImage.length + " : " + thumbFileImage[0]);

        holder.chapterId = id;

        final String price = mCursor.getString(mCursor.getColumnIndex(DbContract.Chapter.CHAPTER_AMOUNT));
        holder.chapterName = mCursor.getString(mCursor.getColumnIndex(CHAPTER_NAME));
        Log.e("bindHolder pos", position + " cId> " + id);
        Log.e("bindHolder pos", position + " chapterName> " + holder.chapterName);
        Log.e("bindHolder pos", position + " price> " + price);

        if ((thumbFileImage[0] != null) && mThumbList.containsKey(thumbFileImage[0])) {
            Picasso.with(mContext).load(mThumbList.get(thumbFileImage[0])).into(holder.imageView);
            //holder.imageView.setImageResource(mThumbList.get(thumbFileImage[0]));
            //holder.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_corner));
        }

        BitmapFactory.Options dimensions = new BitmapFactory.Options();
        dimensions.inJustDecodeBounds = true;
        if (thumbFileImage[0] != null) {
            BitmapFactory.decodeResource(mContext.getResources(), mThumbList.get(thumbFileImage[0]), dimensions);
        }
        double height = dimensions.outHeight;
        double width = dimensions.outWidth;
        Log.d("TAG", "Image: " + height);
        Log.d("TAG", "Image: " + width);
        final double aspectRatio = height / width;
        Log.d("TAG", "aspectRatio: " + aspectRatio);

        holder.imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                holder.imageView.getViewTreeObserver().removeOnPreDrawListener(this);

                int widthImageView = holder.imageView.getWidth();
//                Log.d("TAG","ImageView: "+widthImageView);

                Double newImageHeightDouble = widthImageView * aspectRatio;
                Integer newImageHeightInt = newImageHeightDouble.intValue();
//                Log.d("TAG","newImageHeightInt: "+newImageHeightInt);

                CardView.LayoutParams layoutParams = new CardView.LayoutParams(widthImageView, newImageHeightInt);
                holder.imageView.setLayoutParams(layoutParams);
/*
                CardView.LayoutParams layoutParams1 = new CardView.LayoutParams(widthImageView, newImageHeightInt);
                layoutParams1.setMargins(10,10,10,10);
                holder.cardTestamentChapter.setLayoutParams(layoutParams1);*/

                return false;
            }
        });

        final int[] status = CommonMethods.getStatus(mContext, id);
        switch (status[0]) {
            case 0:
                holder.buttonImage.setText("VIEW");
                break;
            case 1:
                holder.buttonImage.setText("DOWNLOAD");
                break;
            case 2:
                holder.buttonImage.setText("BUY NOW $" + price);
                break;
        }
        Log.e("bindHolder pos", position + " purchase status> " + status[0]);

        if (status[1] == mContext.getResources().getInteger(R.integer.UN_WATCHED)) {
            holder.watchImage.setImageResource(R.drawable.ic_watch_inactive);
        } else {
            holder.watchImage.setImageResource(R.drawable.ic_watch_active);
        }

        if (status[2] == mContext.getResources().getInteger(R.integer.UN_FAVOURITE)) {
            holder.favImage.setImageResource(R.drawable.ic_fav_inactive);
        } else {
            holder.favImage.setImageResource(R.drawable.ic_fav_active);
        }

        holder.buttonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonClick(holder, status[0], id, position, price);
                Log.e("Onclick", position + " id> " + id);
                Log.e("Onclick", position + " price> " + price);
                Log.e("Onclick", position + " purchase status> " + status[0]);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int[] status = CommonMethods.getStatus(mContext, id);
                if (status[0] == mContext.getResources().getInteger(R.integer.DOWNLOADED) /*|| status[0] == 1*/) {
                    gotoNextActivity(id, holder.chapterName);
                }
            }
        });

        if (holder.relTestamentBlur.isShown()) {
            //BlurKit.getInstance().fastBlur(holder.relTestamentBlur, 50, 2);
        }

        holder.favImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleFavourite(holder.chapterId, holder.favImage);
            }
        });
    }

    private void onButtonClick(final TestamentAdapter.Holder holder, int status, final String id, int position, String price) {
        //Log.e("onButton", "click" + status);
        if (status == mContext.getResources().getInteger(R.integer.NOT_DOWNLOADED)) {
            // holder.buttonImage.setEnabled(false);
            // mProgressDialog.show();
            final String chapterId = id;
            mCallback.onCallReceive(holder.chapterId);
           // mCallback.onCallReceive(holder.chapterId, price);
           // mCallback.showDialog(id, holder.chapterName, price);
          // mCallback.onCallReceive(holder.chapterId, price);
            //Log.e("status", "NOT_DOWNLOADED");
        } else if (status == mContext.getResources().getInteger(R.integer.DOWNLOADED)) {
            //Log.e("status", "DOWNLOADED");
            gotoNextActivity(id, holder.chapterName);
        } else if (status == mContext.getResources().getInteger(R.integer.BUY)) {
            //Log.e("buy ","clicked");
            mCallback.showDialog(id, holder.chapterName, price);
            //gotoBuyActivity(id, holder.chapterName);
        }
    }

    /*private void onButtonClick(final SearchResultAdapter.Holder holder, int status, final String id, int position, String  price) {
        Log.e("onButton", "click : " + status);
        if (status == mContext.getResources().getInteger(R.integer.NOT_DOWNLOADED)) {
            final String chapterId = id;
            mCallback.onSearchCallReceive(holder.chapterId,holder.chapterName,true,price);
            Log.e("status", "NOT_DOWNLOADED");
        } else if (status == mContext.getResources().getInteger(R.integer.DOWNLOADED)) {
            Log.e("status", "DOWNLOADED");
            gotoNextActivity(id, holder.chapterTitle);
        } else if (status == mContext.getResources().getInteger(R.integer.BUY)) {
            Log.e("Buy","called");
            mCallback.onSearchCallReceive(id,holder.chapterName,false,price);
        }
    }*/

    public void refreshItem(int pos) {
    }

    private boolean downloadChapter(String id) {
        return false;
    }

    private void gotoNextActivity(String id, String title) {
        Intent intent = new Intent(mContext, PageActivity.class);
        intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, mTestamentType);
        intent.putExtra(Constants.EXTRA_CHAPTER_ID, id);
        intent.putExtra(Constants.EXTRA_CHAPTER_TITLE, title);
        mContext.startActivity(intent);
    }

    public void setData(Cursor cursor) {
        mCursor = cursor;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mCursor != null) {
            return mCursor.getCount();
        } else
            return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public Button buttonImage;
        public ImageView watchImage;
        public ImageView favImage;
        public ProgressBar progressBar;
        String chapterId, chapterName;
        RelativeLayout relTestamentBlur;

        public Holder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.v_chapter);
            buttonImage = itemView.findViewById(R.id.v_button);
            watchImage = itemView.findViewById(R.id.v_watch);
            favImage = itemView.findViewById(R.id.v_fav);
            relTestamentBlur = itemView.findViewById(R.id.rel_testament_blur);
        }
    }

    public interface DownloadCallback {
        //public void onCallReceive(String chapterId);
       // public void onCallReceive(String chapterId, String price);
        public void onCallReceive(String chapterId);

        void showDialog(String chapterId, String title, String price);
    }

    public void handleFavourite(String chapterId, ImageView imageView) {
        Log.e("handleFavourite", "called");
        ContentValues values = new ContentValues();
        int updateCount = -1;
        Cursor cursor = mContext.getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{IS_FAVOURITE}, CHAPTER_ID + "=?", new String[]{chapterId}, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int isFavourite = cursor.getInt(cursor.getColumnIndex(IS_FAVOURITE));
            if (isFavourite == mContext.getResources().getInteger(R.integer.FAVOURITE)) {
                values.put(IS_FAVOURITE, mContext.getResources().getInteger(R.integer.UN_FAVOURITE));
                updateCount = mContext.getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_ID + "=?", new String[]{chapterId});
                if (updateCount > 0) {
                    Toast.makeText(mContext, "Successfully marked as unFavourite", Toast.LENGTH_SHORT).show();
                    imageView.setImageResource(R.drawable.ic_fav_inactive);
                    hitApi(OPTION_UN_FAV, chapterId);
                }
            } else {
                values.put(IS_FAVOURITE, mContext.getResources().getInteger(R.integer.FAVOURITE));
                updateCount = mContext.getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_ID + "=?", new String[]{chapterId});
                if (updateCount > 0) {
                    Toast.makeText(mContext, "Successfully marked as favourite", Toast.LENGTH_SHORT).show();
                    imageView.setImageResource(R.drawable.ic_fav_active);
                    hitApi(OPTION_FAV, chapterId);
                }
            }
            cursor.close();
        }
        // handleNewText("");
    }

    public void hitApi(String option, String chapterID) {
        Log.e("hitAPI", "called " + option);

        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(ACTION_BASE_URL)
                .create(RetrofitInterface.class);
        String token = prefManager.getSharedPref().getString(mContext.getString(R.string.token), null);
        String uId = prefManager.getSharedPref().getString(mContext.getString(R.string.uId), null);
        String dId = prefManager.getSharedPref().getString(mContext.getString(R.string.dId), null);
        Log.e("token ", "" + token);
        Log.e("uId ", "" + uId);
        Log.e("dId ", "" + dId);
        Log.e("option ", "" + option);
        Log.e("chapterId ", "" + chapterID);
        Call<ActionResponse> responseCall = null;

      /*  switch (option) {
            case OPTION_FAV:
                responseCall = retrofitInterface.markFavourite(token, uId, option, dId, chapterID);
                break;
            case OPTION_UN_FAV:
                responseCall = retrofitInterface.markUnFavourite(token, uId, option, dId, mCId);
                break;
            case OPTION_SHARE:
                responseCall = retrofitInterface.markShared(token, uId, option, dId, mCId);
                break;
            case OPTION_WATCHED:
                responseCall = retrofitInterface.markWatched(token, uId, option, dId, mCId);
                break;
        }*/
        responseCall = retrofitInterface.markFavourite(token, uId, option, dId, String.valueOf(chapterID));

        if (responseCall != null)
            responseCall.enqueue(new Callback<ActionResponse>() {
                @Override
                public void onResponse(Call<ActionResponse> call, Response<ActionResponse> response) {
                    if (response != null) {
                        ActionResponse actionResponse = response.body();
                        if (actionResponse != null) {
                            Log.e("actionResponse", "not null");
                            Log.e("status", actionResponse.getStatus() + "");
                            Log.e("msg", actionResponse.getMessage() + "");
                        } else {
                            Log.e("actionResponse", "null");
                        }
                    } else {
                        Log.e("response", "NULL");
                    }
                }

                @Override
                public void onFailure(Call<ActionResponse> call, Throwable t) {

                }
            });
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}