package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.util.CommonMethods;

/**
 * Created by rmn on 21-09-2017.
 */

public class ShareChildAdapter extends RecyclerView.Adapter<ShareChildAdapter.Holder> {

    Context mContext;
    List<String> mImageList;
    public ShareChildAdapter(Context context){
        mContext=context;
        mImageList=new ArrayList<>();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.share_chapter_child,null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
         holder.imageView.setImageBitmap(CommonMethods.getImageBitmap(mContext,mImageList.get(position)));
    }

    public void setData(List<String> imageList){
        mImageList=imageList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        public Holder(View itemView) {
            super(itemView);
            //textView=itemView.findViewById(R.id.v_txt);
            imageView=itemView.findViewById(R.id.v_child_chapter);
        }
    }
}
