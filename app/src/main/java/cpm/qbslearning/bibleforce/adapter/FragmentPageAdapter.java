package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;
import android.database.Cursor;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import cpm.qbslearning.bibleforce.PagerFragment;

import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.AUDIO_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.CHAPTER_PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_TITLE;

/**
 * Created by rmn on 13-08-2017.
 */

public class FragmentPageAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private int mPos;
    private FragmentManager mFragmentManager;
    private Cursor mCursor;

    public FragmentPageAdapter(Context context, FragmentManager fragmentManager, int position) {
        super(fragmentManager);
        mContext = context;
        mPos = position;
        mFragmentManager = fragmentManager;
    }

    @Override
    public Fragment getItem(int position) {
        PagerFragment fragment;
        if (position<mCursor.getCount())
            mCursor.moveToPosition(position);
        fragment = PagerFragment.newInstance(position,mCursor.getString(mCursor.getColumnIndex(CHAPTER_PAGE_ID)),
                mCursor.getString(mCursor.getColumnIndex(PAGE_ID)),
                mCursor.getString(mCursor.getColumnIndex(PAGE_TITLE)),
                mCursor.getString(mCursor.getColumnIndex(AUDIO_URL)));
        return fragment;
    }

    public void setCursor(Cursor cursor) {
        mCursor = cursor;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mCursor != null) {
             return mCursor.getCount();
        } else
            return 0;

    }
}
