package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.dataModel.tending.TrendingData;
import cpm.qbslearning.bibleforce.fragment.DashBoardFragemnt;
import cpm.qbslearning.bibleforce.util.CommonMethods;

public class DashboardViewPagerAdapter extends PagerAdapter {

    Context context;
    ArrayList<Drawable> images;
    ArrayList<TrendingData> arrTrendingData;
    LayoutInflater mLayoutInflater;
    DashBoardFragemnt dashBoardFragemnt;

    public DashboardViewPagerAdapter(Context context, ArrayList<Drawable> arrImages, ArrayList<TrendingData> arrTrendingData, DashBoardFragemnt dashBoardFragemnt) {
        this.context = context;
        this.images = arrImages;
        this.arrTrendingData = arrTrendingData;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.dashBoardFragemnt = dashBoardFragemnt;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        // inflating the item.xml
        View itemView = mLayoutInflater.inflate(R.layout.raw_dashboard_image, container, false);

        // referencing the image view from the item.xml file
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageViewMain);
        TextView txtPagerTitle = (TextView) itemView.findViewById(R.id.txtPagerTitle);
        final ImageView ivdashboardRawRead = itemView.findViewById(R.id.ivdashboardRawRead);
        final TextView txtDashboardRead = itemView.findViewById(R.id.txtDashboardRead);
        LinearLayout llDashboardRawRead = itemView.findViewById(R.id.llDashboardRawRead);

        // setting the image in the imageView
        imageView.setImageDrawable(images.get(position));
        txtPagerTitle.setText(arrTrendingData.get(position).getChapterName());

        // Picasso.with(context).load(images.get(position)).into(imageView);

        // Adding the View
        Objects.requireNonNull(container).addView(itemView);

        int[] status = CommonMethods.getStatus(context, arrTrendingData.get(position).getChapterId());
        if (status[0] == context.getResources().getInteger(R.integer.DOWNLOADED)) {
            ivdashboardRawRead.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_read));
            txtDashboardRead.setText("Read");
        } else {
            ivdashboardRawRead.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_download));
            txtDashboardRead.setText("Download");
        }

        llDashboardRawRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashBoardFragemnt) dashBoardFragemnt).openTestament(arrTrendingData.get(position).getChapterId());
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout) object);
    }
}
