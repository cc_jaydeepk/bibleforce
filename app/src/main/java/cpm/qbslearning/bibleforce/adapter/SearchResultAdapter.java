package cpm.qbslearning.bibleforce.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import cpm.qbslearning.bibleforce.PageActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_AMOUNT;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_FAVOURITE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_WATCHED;

/**
 * Created by rmn on 13-08-2017.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.Holder> {

    private Context mContext;
    private Cursor mCursor;
    private String mCId;
    private DownloadSearchCallback mCallback;
    private int mTestamentType;

    public SearchResultAdapter(Context context, DownloadSearchCallback callback, int testamentType) {
        mContext = context;
        mCallback = callback;
        mTestamentType = testamentType;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.search_result_row, null));
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        mCursor.moveToPosition(position);
        holder.chapterNameView.setText(mCursor.getString(mCursor.getColumnIndex(CHAPTER_NAME)));
        final String price = mCursor.getString(mCursor.getColumnIndex(CHAPTER_AMOUNT));
        holder.chapterName = mCursor.getString(mCursor.getColumnIndex(CHAPTER_NAME));
        String cId = mCursor.getString(mCursor.getColumnIndex(CHAPTER_ID));
        final int[] status = CommonMethods.getStatus(mContext, cId);
        final String id = mCursor.getString(mCursor.getColumnIndex(CHAPTER_ID));
        holder.chapterId = id;
        holder.chapterTitle = mCursor.getString(mCursor.getColumnIndex(CHAPTER_NAME));

        switch (status[0]) {
            case 0:
                holder.buttonView.setText("view");
                break;
            case 1:
                holder.buttonView.setText("Download");
                break;
            case 2:
                holder.buttonView.setText("BUY NOW $" + price);
                break;
        }

        holder.buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonClick(holder, status[0], id, position, price);
            }
        });

        if (mCursor.getInt(mCursor.getColumnIndex(IS_WATCHED)) == mContext.getResources().getInteger(R.integer.UN_WATCHED)) {
            holder.watchView.setImageResource(R.drawable.ic_watch_inactive);
        } else {
            holder.watchView.setImageResource(R.drawable.ic_watch_active);
        }

        if (mCursor.getInt(mCursor.getColumnIndex(IS_FAVOURITE)) == mContext.getResources().getInteger(R.integer.UN_FAVOURITE)) {
            holder.favouriteView.setImageResource(R.drawable.ic_fav_inactive);
        } else {
            holder.favouriteView.setImageResource(R.drawable.ic_fav_active);
        }
        mCursor.getInt(mCursor.getColumnIndex(IS_FAVOURITE));
    }

    public void setData(Cursor cursor) {
        mCursor = cursor;
        Log.e("setData", "called");
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mCursor != null) {
            ///Log.e("search adapter", "cursor size " + mCursor.getCount());
            return mCursor.getCount();
        } else return 0;
    }

    private void onButtonClick(final Holder holder, int status, final String id, int position, String price) {
        Log.e("onButton", "click : " + status);
        if (status == mContext.getResources().getInteger(R.integer.NOT_DOWNLOADED)) {
            final String chapterId = id;
            //mCallback.onSearchCallReceive(holder.chapterId, holder.chapterName, true, price);
            mCallback.onSearchCallReceive(holder.chapterId, holder.chapterName, price);
            // mCallback.onCallReceive(holder.chapterId,holder.chapterName,true,price);
            Log.e("status", "NOT_DOWNLOADED");
        } else if (status == mContext.getResources().getInteger(R.integer.DOWNLOADED)) {
            Log.e("status", "DOWNLOADED");
            gotoNextActivity(id, holder.chapterTitle);
        } else if (status == mContext.getResources().getInteger(R.integer.BUY)) {
            Log.e("Buy", "called");
           // mCallback.onSearchCallReceive(id, holder.chapterName, false, price);
            mCallback.onSearchCallReceive(id, holder.chapterName, price);
        }
    }

    /*private void onButtonClick(final TestamentAdapter.Holder holder, int status, final String id, int position, String price) {
        //Log.e("onButton", "click" + status);
        if (status == mContext.getResources().getInteger(R.integer.NOT_DOWNLOADED)) {
            // holder.buttonImage.setEnabled(false);
            // mProgressDialog.show();
            final String chapterId = id;
            mCallback.onCallReceive(holder.chapterId);
            // mCallback.onCallReceive(holder.chapterId, price);
            // mCallback.showDialog(id, holder.chapterName, price);
            // mCallback.onCallReceive(holder.chapterId, price);
            //Log.e("status", "NOT_DOWNLOADED");
        } else if (status == mContext.getResources().getInteger(R.integer.DOWNLOADED)) {
            //Log.e("status", "DOWNLOADED");
            gotoNextActivity(id, holder.chapterName);
        } else if (status == mContext.getResources().getInteger(R.integer.BUY)) {
            //Log.e("buy ","clicked");
            mCallback.showDialog(id, holder.chapterName, price);
            //gotoBuyActivity(id, holder.chapterName);
        }
    }*/

    private void gotoNextActivity(String id, String title) {
        Intent intent = new Intent(mContext, PageActivity.class);
        intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, mTestamentType);
        intent.putExtra(Constants.EXTRA_CHAPTER_ID, id);
        intent.putExtra(Constants.EXTRA_CHAPTER_TITLE, title);
        mContext.startActivity(intent);
    }

    public interface DownloadSearchCallback {
        //public void onSearchCallReceive(String chapterId, String chapterName, boolean isPurchased, String price);
        public void onSearchCallReceive(String chapterId, String chapterName, String price);

    }

    class Holder extends RecyclerView.ViewHolder {
        TextView chapterNameView;
        Button buttonView;
        ImageView favouriteView, watchView;
        String chapterId, chapterTitle, chapterName;

        public Holder(View itemView) {
            super(itemView);
            chapterNameView = itemView.findViewById(R.id.v_chapter_name);
            buttonView = itemView.findViewById(R.id.v_button);
            favouriteView = itemView.findViewById(R.id.v_fav);
            watchView = itemView.findViewById(R.id.v_watch);
        }
    }
}