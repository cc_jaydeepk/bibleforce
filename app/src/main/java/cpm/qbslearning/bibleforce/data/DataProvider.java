package cpm.qbslearning.bibleforce.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_TABLE_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_TABLE_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_TABLE_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.SHARE_TABLE_NAME;

/**
 * Created by rmn on 09-08-2017.
 */

public class DataProvider extends ContentProvider {

    ParcelFileDescriptor mParcelFileDescriptor;
    private DbHelper mDbHelper;
    private static final String AUTHORITY = DbContract.AUTHORITY;
    private static final int CHAPTER = 100;
    private static final int CHAPTER_PAGE = 101;
    private static final int SHARE_PAGE = 102;
    private static final int COUNTRY_PAGE = 103;
    private static final UriMatcher mUriMatcher = buildUriMatcher();

    public static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, DbContract.PATH_CHAPTER, CHAPTER);
        matcher.addURI(AUTHORITY, DbContract.PATH_CHAPTER_PAGE, CHAPTER_PAGE);
        matcher.addURI(AUTHORITY, DbContract.PATH_SHARE_PAGE, SHARE_PAGE);
        matcher.addURI(AUTHORITY, DbContract.PATH_COUNTRY, COUNTRY_PAGE);
        return matcher;
    }

    @Nullable
    @Override
    public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
        // return super.openFile(uri, mode);
        try {
            AssetManager am = getContext().getAssets();
            InputStream is = am.open("file.pdf");
            File dir = new File(/*"data/data/package.name/files/"*/uri.getPath());
//            dir.mkdirs();
//            File file = new File("data/data/package.name/files/outFile.pdf");
//            file.createNewFile();
            OutputStream out = new FileOutputStream(dir);
            byte buf[] = new byte[1024];
            int len;
            while ((len = is.read(buf)) > 0)
                out.write(buf, 0, len);
            out.close();
            is.close();
            mParcelFileDescriptor = ParcelFileDescriptor.open(dir, ParcelFileDescriptor.MODE_READ_ONLY);
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return mParcelFileDescriptor;
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new DbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String s1) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        final int match = mUriMatcher.match(uri);
        Cursor cursor = null;
        try{


        switch (match) {
            case CHAPTER: {
                cursor = db.query(CHAPTER_TABLE_NAME, projection, selection, args, null, null, null);
                //   Log.e("chapter query","perform"+">"+selection+">"+args.length);
                break;
            }
            case CHAPTER_PAGE: {


                  cursor = db.query(PAGE_TABLE_NAME, projection, selection, args, null, null, null);

                //  Log.e("page query","perform" +">"+projection+">"+selection+">"+args.length);
                break;
            }
            case SHARE_PAGE: {
                cursor = db.query(SHARE_TABLE_NAME, projection, selection, args, null, null, null);
                //  Log.e("page query","perform" +">"+projection+">"+selection+">"+args.length);
                break;
            }
            case COUNTRY_PAGE: {
                cursor = db.query(COUNTRY_TABLE_NAME, projection, selection, args, null, null, null);
                break;
            }
        }

        //  Log.e("query ",selection+" args "+ args[0]);

//        if (cursor==null)
//            Log.e("query cursor ","null");
//       else if (cursor!=null)
//            Log.e("query cursor ","not null count "+cursor.getCount());
        }catch (Exception e){
            e.printStackTrace();
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = mUriMatcher.match(uri);
        Uri statusUri = null;
        long id = -1;
        switch (match) {
            case CHAPTER: {
                id = db.insert(CHAPTER_TABLE_NAME, null, contentValues);
                if (id > 0) {
                    statusUri = DbContract.Chapter.buildChapterUri(id);
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                Log.e("insert", uri.toString() + "<" + CHAPTER);
            }
            break;

            case CHAPTER_PAGE: {
                id = db.insert(PAGE_TABLE_NAME, null, contentValues);

                if (id > 0) {
                    statusUri = DbContract.ChapterPage.buildPageUri(id);
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                Log.e("chapter Page insert", uri.toString() + " id " + id);
            }
            break;

            case SHARE_PAGE: {
                id = db.insert(SHARE_TABLE_NAME, null, contentValues);

                if (id > 0) {
                    statusUri = DbContract.ChapterPage.buildPageUri(id);
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                Log.e("chapter Page insert", uri.toString() + " id " + id);
            }
            break;
            case COUNTRY_PAGE: {
                id = db.insert(COUNTRY_TABLE_NAME, null, contentValues);

                if (id > 0) {
                    statusUri = DbContract.ChapterPage.buildPageUri(id);
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                Log.e("chapter Page insert", uri.toString() + " id " + id);
            }
            break;

            default:
                throw new android.database.SQLException("Failed to insert row into " + uri);
        }
        if (id > 0)
            Log.e("ID ", id + ", inerted");
        else
            Log.e("ID ", id + ", not inserted");

        return statusUri;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String where, String[] whereArgs) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int match = mUriMatcher.match(uri);
        int id = -1;
        switch (match) {
            case CHAPTER: {
                id = db.update(CHAPTER_TABLE_NAME, contentValues, where, whereArgs);
                break;
            }
            case CHAPTER_PAGE: {
                id = db.update(PAGE_TABLE_NAME, contentValues, where, whereArgs);
                break;
            }
            case SHARE_PAGE: {
                id = db.update(SHARE_TABLE_NAME, contentValues, where, whereArgs);
                break;
            }
        }
        if (id < 0) {
            Log.e("update", "unsuccessfull");
        } else
            Log.e("update", "successfull");
        if (null == where || 0 != id) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return id;
    }
}
