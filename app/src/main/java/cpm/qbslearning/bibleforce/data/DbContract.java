package cpm.qbslearning.bibleforce.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by rmn on 09-08-2017.
 */

public class DbContract {

    public static final String  AUTHORITY="cpm.qbslearning.bibleforce";
    public static final Uri BASE_CONTENT_URI=Uri.parse("content://"+AUTHORITY);

    public static final String PATH_CHAPTER="chapterTable";
    public static final String PATH_CHAPTER_PAGE="chapterPageTable";
    public static final String PATH_SHARE_PAGE="sharePageTable";
    public static final String PATH_COUNTRY="countryTable";

    public static class Chapter implements BaseColumns{

        public static final String CHAPTER_TABLE_NAME="chapterTable";

        public static final Uri CONTENT_URI= BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHAPTER).build();


        public static final String CONTENT_TYPE= "vnd.android.cursor.dir/"
                +AUTHORITY+"/"+PATH_CHAPTER;

        public static final String CONTENT_ITEM_TYPE= "vnd.android.cursor.item/"
                +AUTHORITY+"/"+PATH_CHAPTER;

        public static final String C_Id="id";
        public static final String CHAPTER_ID="chapterId";
        public static final String CHAPTER_NAME="chapterName";
        public static final String TESTAMENT_TYPE="testamentType";
        public static final String IS_PAID="isPaid";
        public static final String IS_PURCHASED="isPurchased";
        public static final String PAGE_COUNT="pageCount";
        public static final String IS_DOWNLOADED="isDownloaded";
        public static final String IS_WATCHED="isWatched";
        public static final String IS_FAVOURITE="isFavourite";
        public static final String CHAPTER_THUMB="chapterThumb";
        public static final String CHAPTER_THUMB_IMAGE="chapterThumbImage";
        public static final String PURCHASED_VIA="purchasedVia";
        public static final String CHAPTER_AMOUNT="chapterAmount";
        public static final String IS_ACTIVE="isActive";

        public static Uri buildChapterUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }

    }

    public static class ChapterPage implements BaseColumns{

        public static final String PAGE_TABLE_NAME="chapterPageTable";

        public static final Uri CONTENT_URI= BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHAPTER_PAGE).build();

        public static final String CONTENT_TYPE= "vnd.android.cursor.dir/"
                +AUTHORITY+"/"+PATH_CHAPTER_PAGE;

        public static final String CONTENT_ITEM_TYPE=  "vnd.android.cursor.item/"
                +AUTHORITY+"/"+PATH_CHAPTER_PAGE;

        public static final String P_Id="id";
        public static final String PAGE_ID="pageId";
        public static final String CHAPTER_PAGE_ID="chapterId";
        public static final String PAGE_TITLE="pageTitle";
        public static final String PAGE_ORDER="pageOrder";
        public static final String IMAGE_URL="imageUrl";
        public static final String PAGE_IMAGE="pageImage";
        public static final String AUDIO_URL="audioUrl";
        public static final String PAGE_AUDIO="audioName";
        public static final String IS_IMAGE_DOWNLOADED="isImageDownloaded";
        public static final String IS_AUDIO_DOWNLOADED="isAudioDownloaded";
        public static final String IS_PAGE_DOWNLOADED="isPageDownloaded";

        public static Uri buildPageUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }

    }
    public static class SharePage implements BaseColumns{

        public static final String SHARE_TABLE_NAME="sharePageTable";

        public static final Uri CONTENT_URI= BASE_CONTENT_URI.buildUpon().appendPath(PATH_SHARE_PAGE).build();

        public static final String CONTENT_TYPE= "vnd.android.cursor.dir/"
                +AUTHORITY+"/"+PATH_SHARE_PAGE;

        public static final String CONTENT_ITEM_TYPE=  "vnd.android.cursor.item/"
                +AUTHORITY+"/"+PATH_SHARE_PAGE;

        public static final String S_Id="id";
        public static final String DATE="date";
        public static final String PAGE_PATH="pagePath";
        public static final String PAGE_TITLE="pageTitle";

        public static Uri buildPageUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }

    }

    public static class CountryTable implements BaseColumns{

        public static final String COUNTRY_TABLE_NAME="countryTable";

        public static final Uri CONTENT_URI= BASE_CONTENT_URI.buildUpon().appendPath(PATH_COUNTRY).build();

        public static final String CONTENT_TYPE= "vnd.android.cursor.dir/"
                +AUTHORITY+"/"+PATH_COUNTRY;

        public static final String CONTENT_ITEM_TYPE=  "vnd.android.cursor.item/"
                +AUTHORITY+"/"+PATH_COUNTRY;

        public static final String COUN_Id="id";
        public static final String COUNTRY_ID="countryId";
        public static final String COUNTRY_NAME="countryName"; ;

        public static Uri buildPageUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }

    }

}
