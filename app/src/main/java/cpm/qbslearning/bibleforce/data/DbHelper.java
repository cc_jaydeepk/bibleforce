package cpm.qbslearning.bibleforce.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_AMOUNT;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_TABLE_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_THUMB;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_THUMB_IMAGE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.C_Id;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_ACTIVE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_FAVOURITE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PAID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PURCHASED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_WATCHED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.PAGE_COUNT;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.PURCHASED_VIA;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.TESTAMENT_TYPE;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.AUDIO_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.CHAPTER_PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IMAGE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IS_AUDIO_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IS_IMAGE_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IS_PAGE_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_AUDIO;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_IMAGE;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ORDER;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_TABLE_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_TITLE;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.P_Id;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_TABLE_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUN_Id;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.DATE;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.PAGE_PATH;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.SHARE_TABLE_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.S_Id;

/**
 * Created by rmn on 09-08-2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    private Context mContext;
    private static int DB_VERSION = 1;
    private static final String DB_NAME = "chapterDb";

    private static final String chapterTable = "CREATE TABLE " + CHAPTER_TABLE_NAME + "(" + C_Id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CHAPTER_ID + " TEXT NOT NULL , " +
            CHAPTER_NAME + " TEXT NOT NULL , " +
            CHAPTER_THUMB + " TEXT NOT NULL , " +
            CHAPTER_THUMB_IMAGE + " TEXT NOT NULL , " +
            CHAPTER_AMOUNT + " TEXT NOT NULL , " +
            TESTAMENT_TYPE + " INTEGER NOT NULL , " +
            IS_PAID + " INTEGER NOT NULL , " +
            IS_PURCHASED + " INTEGER NOT NULL , " +
            IS_DOWNLOADED + " INTEGER NOT NULL , " +
            IS_WATCHED + " INTEGER  DEFAULT 0, " +
            IS_FAVOURITE + " INTEGER  DEFAULT 0 , " +
            IS_ACTIVE + " INTEGER  DEFAULT 0 , " +
            PURCHASED_VIA + " INTEGER  DEFAULT 0 , " +
            PAGE_COUNT + " INTEGER NOT NULL  " + ");";

    private static final String shareTable = "CREATE TABLE " + SHARE_TABLE_NAME + "(" + S_Id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DATE + " TEXT NOT NULL , " +
            DbContract.SharePage.PAGE_TITLE + " TEXT  NULL ," +
            PAGE_PATH + " TEXT NOT NULL );";

    private static final String countryTable = "CREATE TABLE " + COUNTRY_TABLE_NAME + "(" + COUN_Id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COUNTRY_ID + " TEXT NOT NULL , " +
            COUNTRY_NAME + " TEXT NOT NULL );";

    private static final String pageTable = "CREATE TABLE "
            + PAGE_TABLE_NAME +
            "(" + P_Id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            PAGE_ID + " TEXT NOT NULL , " +
            CHAPTER_PAGE_ID + " TEXT NOT NULL , " +
            PAGE_TITLE + " TEXT NOT NULL , " +
            PAGE_ORDER + " TEXT NOT NULL , " +
            IMAGE_URL + " TEXT NOT NULL , " +
            PAGE_IMAGE + " INTEGER  DEFAULT 1 , " +
            AUDIO_URL + " TEXT NOT NULL , " +
            PAGE_AUDIO + " INTEGER  DEFAULT 1 , " +
            IS_IMAGE_DOWNLOADED + " INTEGER DEFAULT 0, " +
            IS_AUDIO_DOWNLOADED + " INTEGER DEFAULT 0, " +
            IS_PAGE_DOWNLOADED + " INTEGER  DEFAULT 0, " +
            "FOREIGN KEY (" + CHAPTER_PAGE_ID + ") REFERENCES " + CHAPTER_TABLE_NAME + "(" + CHAPTER_ID + ")" + ");";


    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(chapterTable);
        database.execSQL(pageTable);
        database.execSQL(shareTable);
        database.execSQL(countryTable);
        Log.e("onCreate", "called");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + chapterTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + pageTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + shareTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + countryTable);
        onCreate(sqLiteDatabase);
    }
}
