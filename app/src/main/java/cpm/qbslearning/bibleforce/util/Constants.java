package cpm.qbslearning.bibleforce.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cpm.qbslearning.bibleforce.R;

/**
 * Created by rmn on 09-08-2017.
 */

public class Constants {

    public static final int NONE = 0;
    public static final int DRAG = 1;
    public static final int ZOOM = 2;
    public static final String TITLE = "title";
    public static final String ABOUT_URL = "aboutUrl";
    public static float MIN_ZOOM = 1f;
    public static float MAX_ZOOM = 5f;
    public static final String TAG = "TOUCH";

    //public static final String ACTION_BASE_URL="http://bibleapp-v1.regenapps.com/";
/*
    public static final String ACTION_BASE_URL="http://bibleapp-v1-staging.regenapps.com/";
*/

    public static final String ACTION_BASE_URL = "http://api-v1.bibleforce.net";
    // public static final String ACTION_BASE_URL = "http://192.168.1.114/bibleappapi/public/";

//    public static final String ACTION_BASE_URL="http://api2-v1.bibleforce.net";

    public static final String INVALID_DATA = "Invalid Email or Password";
    public static final String TESTAMENT_TYPE = "testamentType";
    public static final String EXTRA_TESTAMENT_TYPE = "extraTestamentType";
    public static final String EXTRA_IS_TESTAMENT_ADDED = "isTestamentAdded";
    public static final String EXTRA_ITEM_POSITION = "extraItemPosition";
    public static final String EXTRA_CHAPTER_ID = "extraChapterId";
    public static final String EXTRA_CHAPTER_NAME = "extraChapterName";
    public static final String EXTRA_CHAPTER_PAGE_ID = "extraChapterPageId";
    public static final String EXTRA_DB_UPDATED = "extraDbUpdated";
    public static final String IMAGE_BASE_URL = "imageBaseUrl";
    ;
    public static final String CHAPTER_DOWNLOAD_STATUS = "chapterDownloadStatus";
    public static final String CHAPTER_DOWNLOAD_MESSAGE = "Please Wait a While!";
    public static final String NEW_TESTAMENT = "New Testament";
    public static final String OLD_TESTAMENT = "Old Testament";
    public static final String INTERNET_TITLE = "Internet Unavailable";
    public static final String INTERNET_MSG = "Do you want to turn on Internet connection";
    public static final String EXTRA_CHAPTER_TITLE = "chapterTitle";
    public static final String EXTRA_CHAPTER_PRICE = "chapterPrice";
    public static final String IS_DATA_AVAILABLE = "isDataAvailable";
    public static final String IS_AUDIO_ON = "IS_AUDIO_ON";
    public static final String IS_NOTIFICATION_ON = "IS_NOTIFICATION_ON";
    public static final String EXTRA_CALLER = "extraCaller";

    public static final String OPTION_FAV = "markFavorite";
    public static final String OPTION_UN_FAV = "unfavoriteChapter";
    public static final String OPTION_SHARE = "shareChapter";
    public static final String OPTION_WATCHED = "markViewed";
    public static final String PAID_CHAPTER_ID = "paidChapterId";

    public static final String DOWNLOAD_SERVICE_REQUEST_CODE = "DOWNLOAD_SERVICE_REQUEST_CODE";
    public static final String DOWNLOAD_PERCENTAGE = "PERCENTAGE";


    public static final int PERCENTAGE = 1;
    public static final int DOWNLOAD_COMPLETE = 2;

    public static final String getAudioFilePath(Context context) {

        String filePath =/*SDCardRoot+*/"audio";
        File file = new File(context.getFilesDir(), filePath);
        if (!file.exists()) {
            Log.e("file ", "file not exists");
            boolean path = file.mkdirs();
            Log.e("path", "created ? " + path + " " + file.isDirectory());
        } else {
            Log.e("path", "already exists ");
        }

//        File file =new File(context.getFilesDir(),filePath);
//        if (!file.exists()){
//            Log.e("file ","file not exists");
//            boolean  path=file.mkdirs();
//             Log.e("path","created ? "+path +" "+file.isDirectory());
//        }else {
//            Log.e("path","already exists ");
//        }
        return filePath;
    }

    public static final String getImageFilePath(Context context) {

        String filePath =/*SDCardRoot+*/"image";
        File file = new File(context.getFilesDir(), filePath);
        if (!file.exists()) {
            Log.e("file ", "file not exists");
            boolean path = file.mkdirs();
            Log.e("path", "created ? " + path + " " + file.isDirectory());
        } else {
            Log.e("path", "already exists ");
        }
        return filePath;
    }

    public static final String getRecordingFilePath(Context context) {
        String SDCardRoot = Environment.getExternalStorageDirectory()
                .toString();
        String filePath = SDCardRoot + "/recording";
        File file = new File(filePath);
        if (!file.exists()) {
            Log.e("file ", "file not exists");
            boolean path = file.mkdirs();
            Log.e("path", "created ? " + path + " " + file.isDirectory());
        } else {
            Log.e("path", "already exists ");
        }
        return filePath;
    }

    public static final List<Integer> CHAPTER_IMAGE_LIST = new ArrayList<Integer>() {{
        add(R.drawable.ch1);
        add(R.drawable.ch2);
        add(R.drawable.ch3);
        add(R.drawable.ch4);
        add(R.drawable.ch5);
        add(R.drawable.ch6);
        add(R.drawable.ch7);
        add(R.drawable.ch8);
        add(R.drawable.ch9);
        add(R.drawable.ch10);
        add(R.drawable.ch11);
        add(R.drawable.ch12);
        add(R.drawable.ch13);
        add(R.drawable.ch14);
        add(R.drawable.ch15);
        add(R.drawable.ch16);
        add(R.drawable.ch17);
        add(R.drawable.ch18);
        add(R.drawable.ch19);
        add(R.drawable.ch20);
        add(R.drawable.ch21);
        add(R.drawable.ch22);
        add(R.drawable.ch23);
        add(R.drawable.ch24);
        add(R.drawable.ch25);
        add(R.drawable.ch26);
        add(R.drawable.ch27);
        add(R.drawable.ch28);
        add(R.drawable.ch29);
        add(R.drawable.ch30);
        add(R.drawable.ch31);
        add(R.drawable.ch32);
        add(R.drawable.ch33);
        add(R.drawable.ch34);
    }};

    public static final Map<String, Integer> CHAPTER_THUMB_LIST_MAP = new HashMap<String, Integer>() {{
        put("ch1", R.drawable.ch1);
        put("ch2", R.drawable.ch2);
        put("ch3", R.drawable.ch3);
        put("ch4", R.drawable.ch4);
        put("ch5", R.drawable.ch5);
        put("ch6", R.drawable.ch6);
        put("ch7", R.drawable.ch7);
        put("ch8", R.drawable.ch8);
        put("ch9", R.drawable.ch9);
        put("ch10", R.drawable.ch10);
        put("ch11", R.drawable.ch11);
        put("ch12", R.drawable.ch12);
        put("ch13", R.drawable.ch13);
        put("ch14", R.drawable.ch14);
        put("ch15", R.drawable.ch15);
        put("ch16", R.drawable.ch16);
        put("ch17", R.drawable.ch17);
        put("ch18", R.drawable.ch18);
        put("ch19", R.drawable.ch19);
        put("ch20", R.drawable.ch20);
        put("ch21", R.drawable.ch21);
        put("ch22", R.drawable.ch22);
        put("ch23", R.drawable.ch23);
        put("ch24", R.drawable.ch24);
        put("ch25", R.drawable.ch25);
        put("ch26", R.drawable.ch26);
        put("ch27", R.drawable.ch27);
        put("ch28", R.drawable.ch28);
        put("ch29", R.drawable.ch29);
        put("ch30", R.drawable.ch30);
        put("ch31", R.drawable.ch31);
        put("ch32", R.drawable.ch32);
        put("ch33", R.drawable.ch33);
        put("ch34", R.drawable.ch34);
    }};


    public static final Map<String, Integer> FAV_CHAPTER_THUMB_LIST_MAP = new HashMap<String, Integer>() {{
        put("ch1", R.drawable.fav1);
        put("ch2", R.drawable.fav2);
        put("ch3", R.drawable.fav3);
        put("ch4", R.drawable.fav4);
        put("ch5", R.drawable.fav5);
        put("ch6", R.drawable.fav6);
        put("ch7", R.drawable.fav7);
        put("ch8", R.drawable.fav8);
        put("ch9", R.drawable.fav9);
        put("ch10", R.drawable.fav10);
        put("ch11", R.drawable.fav11);
        put("ch12", R.drawable.fav12);
        put("ch13", R.drawable.fav13);
        put("ch14", R.drawable.fav14);
        put("ch15", R.drawable.fav15);
        put("ch16", R.drawable.fav16);
        put("ch17", R.drawable.fav17);
        put("ch18", R.drawable.fav18);
        put("ch19", R.drawable.fav19);
        put("ch20", R.drawable.fav20);
        put("ch21", R.drawable.fav21);
        put("ch22", R.drawable.fav22);
        put("ch23", R.drawable.fav23);
        put("ch24", R.drawable.fav24);
        put("ch25", R.drawable.fav25);
        put("ch26", R.drawable.fav26);
        put("ch27", R.drawable.fav27);
        put("ch28", R.drawable.fav28);
        put("ch29", R.drawable.fav29);
        put("ch29", R.drawable.fav30);
        put("ch29", R.drawable.fav31);
        put("ch29", R.drawable.fav32);
        put("ch29", R.drawable.fav33);
        put("ch29", R.drawable.fav34);
    }};

}
