package cpm.qbslearning.bibleforce.util;

/**
 * Created by rmn on 13-10-2017.
 */

public class Game {

    private String name;
    private int imageSource;

    public Game(int imageSource, String name) {
        this.name = "";
        this.imageSource = imageSource;
    }

    public String getName() {
        return name;
    }

    public int getImageSource() {
        return imageSource;
    }
}