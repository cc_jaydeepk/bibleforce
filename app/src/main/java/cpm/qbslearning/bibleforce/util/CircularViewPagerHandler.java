package cpm.qbslearning.bibleforce.util;

import android.util.Log;

import androidx.viewpager.widget.ViewPager;

public class CircularViewPagerHandler implements ViewPager.OnPageChangeListener {
    public static final int SET_ITEM_DELAY = 500;

    private ViewPager mViewPager;
    private ViewPager.OnPageChangeListener mListener;

    public CircularViewPagerHandler(final ViewPager viewPager) {
        mViewPager = viewPager;
        mViewPager.setCurrentItem(1, true);
    }

    public void setOnPageChangeListener(final ViewPager.OnPageChangeListener listener) {
        mListener = listener;
    }

    @Override
    public void onPageSelected(final int position) {
        handleSetCurrentItemWithDelay(position);
        invokeOnPageSelected(position);
    }

    private void handleSetCurrentItemWithDelay(final int position) {
        mViewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                handleSetCurrentItem(position);
            }
        }, SET_ITEM_DELAY);
    }

    private void handleSetCurrentItem(final int position) {
        final int lastPosition = mViewPager.getAdapter().getCount() - 1;
        if (position == 0) {
            mViewPager.setCurrentItem(lastPosition + 1, true);
        } else if (position == lastPosition) {
            Log.e("last to first", "last posi" + lastPosition + "posi " + position);
            mViewPager.setCurrentItem(1, false);
        }
    }

    private void invokeOnPageSelected(final int position) {
        if (mListener != null) {
            mListener.onPageSelected(position);
        }
    }

    @Override
    public void onPageScrollStateChanged(final int state) {
        invokeOnPageScrollStateChanged(state);
    }

    private void invokeOnPageScrollStateChanged(final int state) {
        if (mListener != null) {
            mListener.onPageScrollStateChanged(state);
        }
    }

    @Override
    public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
        invokeOnPageScrolled(position, positionOffset, positionOffsetPixels);
    }

    private void invokeOnPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
        if (mListener != null) {
            mListener.onPageScrolled(position + 1, positionOffset, positionOffsetPixels);
        }
    }
}
