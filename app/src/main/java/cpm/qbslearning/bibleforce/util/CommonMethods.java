package cpm.qbslearning.bibleforce.util;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.PhoneNumberUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cpm.qbslearning.bibleforce.DashboardActivity;
import cpm.qbslearning.bibleforce.DemoActivity;
import cpm.qbslearning.bibleforce.LoginActivity;
import cpm.qbslearning.bibleforce.R;
import cpm.qbslearning.bibleforce.RetrofitClient;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.Chapter;
import cpm.qbslearning.bibleforce.dataModel.ChapterPage;
import cpm.qbslearning.bibleforce.dataModel.ChapterResponse;
import cpm.qbslearning.bibleforce.dataModel.Country;
import cpm.qbslearning.bibleforce.dataModel.GetCountriesResponse;
import cpm.qbslearning.bibleforce.dataModel.SignupResponse;
import cpm.qbslearning.bibleforce.dataModel.UserProfile;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.LoginActivity.BASE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_AMOUNT;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_THUMB;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_THUMB_IMAGE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_ACTIVE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_FAVOURITE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PAID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PURCHASED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_WATCHED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.PAGE_COUNT;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.PURCHASED_VIA;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.TESTAMENT_TYPE;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.AUDIO_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.CHAPTER_PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IMAGE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IS_PAGE_DOWNLOADED;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_AUDIO;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ORDER;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_TITLE;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_NAME;
import static cpm.qbslearning.bibleforce.util.Constants.IMAGE_BASE_URL;
import static cpm.qbslearning.bibleforce.util.Constants.IS_DATA_AVAILABLE;

/**
 * Created by rmn on 09-08-2017.
 */

public class CommonMethods {

    Context context;

//    public static void imageDownload(Context ctx, String url, String id) throws MalformedURLException {
//        Log.e("Images url", url + " id " + id);
//
//        Picasso.with(ctx)
//                .load(url)
//                .into(getTarget(url, ctx, id));
//
//    }

/*

    private static com.squareup.picasso.Target getTarget(final String url, final Context context, final String id) throws MalformedURLException {

        com.squareup.picasso.Target target = new com.squareup.picasso.Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {

                        Log.e("chapterId ", id + " , trying to insert");

                        ByteArrayOutputStream oStream = new ByteArrayOutputStream();
                        try {
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
                            byte[] buffer = oStream.toByteArray();
                            ContentValues values = new ContentValues();
                            values.put(DbContract.Chapter.CHAPTER_THUMB, buffer);
                            String where = DbContract.Chapter.CHAPTER_ID + "=?";
                            String[] whereArgs = {id};
                            int i = context.getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, where, whereArgs);
                            Log.e("chapterId ", id + " ,  inserted at " + i);

                            oStream.flush();
                            oStream.close();
                        } catch (IOException e) {
                            Log.e("image error", e.getLocalizedMessage());
                        }
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.e("chapterId ", id + " , Failed to insert" + errorDrawable.toString());

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;

    }

*/

    public static boolean isPasswordValid(String password) {
        if (password == null || password.equals(""))
            return false;
        else if (password.length() < 5)
            return false;

        return true;
    }

    public static boolean isNameValid(String name) {

        Pattern pattern = Pattern.compile(new String("^[a-zA-Z\\s]*$"));
        Matcher matcher = pattern.matcher(name);
        //if pattern matches
        return matcher.matches();
    }

    public static void setupUI(final Activity activity, View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(activity, innerView);
            }
        }
    }

    public static boolean isMobileValid(String mobileNo) {
        String regexStr = "^[0-9]{10}$";
        Pattern pattern = Pattern.compile(regexStr);
        Matcher matcher = pattern.matcher(mobileNo);
        if (mobileNo.trim().equals(""))
            return false;

        if (mobileNo.length() > 5 && mobileNo.length() < 15)
            return true;
        return PhoneNumberUtils.isGlobalPhoneNumber(mobileNo);
/*
        return matcher.matches();
*/
    }

    public static boolean isEmailValid(String email) {

        if (email == null || email.equals("")) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }

    }

//    public static int imageDownloadAndSave(final Context context, final String imageUrl,final int i, final String... id) {
//
//        final boolean result = false;
//        new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//
//                Log.e("imageDownloadAndSave ", imageUrl + "< chapterId " + id[0] + " < ");
//                if (id.length == 2)
//                    Log.e("imageDownloadAndSave ", imageUrl + "< pageId " + id[1] + " < ");
//
//                HttpURLConnection urlConnection = null;
//                try {
//                    URL url = new URL(imageUrl);
//                    Bitmap bitmap;
//                    urlConnection = (HttpURLConnection) url.openConnection();
//                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//
//                    bitmap = BitmapFactory.decodeStream(in);
//                    ByteArrayOutputStream oStream = new ByteArrayOutputStream();
//                    try {
//                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
//                        byte[] buffer = oStream.toByteArray();
//                        upDateChapterData(context, buffer, i, id);
//                        oStream.flush();
//                        oStream.close();
//                    } catch (IOException e) {
//                        Log.e("image error", e.getLocalizedMessage());
//                    }
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } finally {
//                    urlConnection.disconnect();
//                }
//            }
//        }).start();
//
//
//        return i;
//    }

//    public static void upDateChapterData(Context context, byte[] buffer, int j, String... id) {
//
//        ContentValues values = new ContentValues();
//        if (id.length == 1) {
//            values.put(DbContract.Chapter.CHAPTER_THUMB, buffer);
//            String where = DbContract.Chapter.CHAPTER_ID + "=?";
//            String[] whereArgs = {id[0]};
//            int i = context.getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, where, whereArgs);
//            Log.e("chapter chapterId ",j+"="+ id[0] + " ,  updated row " + i);
//        } else {
//            values.put(DbContract.ChapterPage.PAGE_IMAGE, buffer);
//            String where = CHAPTER_PAGE_ID + "=? AND " + PAGE_ID + "=?";
//            String[] whereArgs = {id[0], id[1]};
//            int i = context.getContentResolver().update(DbContract.ChapterPage.CONTENT_URI, values, where, whereArgs);
//            Log.e("page chapterId ", j+"="+ id[0] + " and " + id[1] + " ,  insert status" + i);
//        }
//    }

    public static int[] getStatus(Context context, String id) {
        int[] status = new int[3];

        Cursor cursor = context.getContentResolver()
                .query(DbContract.Chapter.CONTENT_URI, new String[]{IS_PURCHASED, IS_DOWNLOADED, IS_PAID, IS_WATCHED, IS_FAVOURITE},
                        DbContract.Chapter.CHAPTER_ID + "=?", new String[]{id}, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();

                int down = cursor.getInt(cursor.getColumnIndex(IS_DOWNLOADED));
                int buy = cursor.getInt(cursor.getColumnIndex(IS_PAID));
                int purch = cursor.getInt(cursor.getColumnIndex(IS_PURCHASED));

                int isWatched = cursor.getInt(cursor.getColumnIndex(IS_WATCHED));
                int isFavourite = cursor.getInt(cursor.getColumnIndex(IS_FAVOURITE));

                status[1] = isWatched;
                status[2] = isFavourite;

                if (buy == context.getResources().getInteger(R.integer.BUY)) {
                    status[0] = buy;
                    if (purch == context.getResources().getInteger(R.integer.PURCHASED)) {
                        if (down == context.getResources().getInteger(R.integer.DOWNLOADED)) {
                            status[0] = down;
                        } else {
                            status[0] = context.getResources().getInteger(R.integer.NOT_DOWNLOADED);
                        }
                    }
                } else if (buy == context.getResources().getInteger(R.integer.NOT_DOWNLOADED)) {

                    if (down == context.getResources().getInteger(R.integer.DOWNLOADED)) {
                        status[0] = down;
                    } else {
                        status[0] = context.getResources().getInteger(R.integer.NOT_DOWNLOADED);
                    }

                }

                Log.e("getStatus", "bookId->" + id + " cursor not null buy " + buy + ", purchase " + purch + ", down " + down + "<" + status[0]);

            }
            cursor.close();
        } else {
        }
        //  Log.e("getStatus", "cursor  null");

        return status;
    }

    public static void printData(Context context, Uri uri, boolean bool) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (bool) {
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        Log.e("", cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.AUDIO_URL)) + ","
                                + cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.PAGE_AUDIO)) + ","
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.IMAGE_URL))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.PAGE_IMAGE))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.CHAPTER_PAGE_ID))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.PAGE_ID))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.PAGE_ID))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.ChapterPage.PAGE_TITLE)));
                    }
                }
            }
        } else {

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        Log.e(cursor.getString(cursor.getColumnIndex(DbContract.Chapter.CHAPTER_ID)), ","
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.Chapter.CHAPTER_NAME))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.Chapter.IS_PAID))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.Chapter.TESTAMENT_TYPE))
                                + "," + cursor.getString(cursor.getColumnIndex(DbContract.Chapter.IS_DOWNLOADED)));
                    }
                }
            }
        }
        cursor.close();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        Log.e("isNetworkAvailable ", "called");
        boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        Log.e("isNetworkAvailable ", "connected " + connected);
        return connected;
    }

    /*public writeStringToSharedPref(activity: Context, key: String?, value: String?) {
        val sharedPref: SharedPreferences =
                activity.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }*/

    public static void writeStringToSharedPref(Context activity, String key, String value) {
        SharedPreferences sharedPref = activity.getSharedPreferences("myPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void readStringFromSharedPref(
            Context activity,
            String key,
            String defaultValue
    ) {
        SharedPreferences sharedPref = activity.getSharedPreferences("myPref", Context.MODE_PRIVATE);
        if (sharedPref.getString(key, defaultValue) != null) {
            sharedPref.getString(
                    key,
                    defaultValue);
        } else {


        }
        return;

    }

    public static Bitmap getImageBitmap(Context context, String imageUrl) {
        Bitmap bitmap = null;
        String imageInSD = Constants.getImageFilePath(context) + imageUrl;
        Log.e("getImageBitmap ", imageInSD + "");
        try {
            FileInputStream inputStream = context.openFileInput(imageInSD);
            BitmapFactory.Options bOptions = new BitmapFactory.Options();
            bOptions.inTempStorage = new byte[16 * 1024];
            Log.e("file decodeing", "called");
            bitmap = BitmapFactory.decodeStream(inputStream);
            Log.e("file decoded", "called");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("FileNotFoundException", "called");
        }
        return bitmap;
    }

    public static Bitmap getBitmap(byte[] thumbByte) {
        Bitmap bitmap = null;
        bitmap = BitmapFactory.decodeByteArray(thumbByte, 0, thumbByte.length);
        return bitmap;
    }

    @NonNull
    public static List<Boolean> signUp(final Activity activity, final CommonMethodsCallback callback, final String loginType, final String email, String pass, final String deviceId,
                                       final String firstName, final String lastName, final String mobile, String socialId) {
        final List<Boolean> status = new ArrayList<>(2);
        final PrefManager mPrefManager = new PrefManager(activity);

        Log.e("socialId ", socialId + "");
        Log.e("BASE URL", Constants.ACTION_BASE_URL);

        Call<SignupResponse> responseCall = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL)
                .create(RetrofitInterface.class).signUp(loginType, email, pass, deviceId, firstName, lastName, mobile, activity.getResources().getString(R.string.isTab), socialId);

        responseCall.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                try {
                    SignupResponse signupResponse = response.body();

                    Log.e("response", response.toString());
                    Log.e("response", "status " + signupResponse.getStatus());
                    Log.e("response", "status " + signupResponse.getMessage());

                    if (signupResponse.getStatus() > 0) {
                        Log.e("uid", "----" + signupResponse.getUserId());
                        Log.e("token", "----" + signupResponse.getSessionToken());
                        Log.e("message", "----" + signupResponse.getMessage());
                        UserProfile userProfile = new UserProfile(activity);
                        userProfile.setFirstName(firstName);
                        userProfile.setLastName(lastName);
                        userProfile.setContactNo(mobile);
                        userProfile.setEmail(email);

                        mPrefManager.getEditor().putString(activity.getString(R.string.uId), signupResponse.getUserId()).apply();
                        mPrefManager.getEditor().putString(activity.getString(R.string.token), signupResponse.getSessionToken()).apply();
                        mPrefManager.getEditor().putString(activity.getString(R.string.login_type), loginType).apply();
                        status.add(0, true);
//                    publishResults(activity);
                        CommonMethods.fetchChapter(activity, callback);
                        // goToNextActivity(activity, deviceId);
                    } else {
                        Toast.makeText(activity, signupResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        callback.callbackFromCommonMethods(false);
                        status.add(0, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                call.cancel();
                //  Log.e("error",t.getMessage());
                // mProgressDialog.dismiss();
                if (t != null)
                    Log.e("onFailure", t.getMessage());
                else
                    Log.e("onFailure", "t null");

                status.add(0, false);
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
        Log.e("jlfks", "jhkjhd");
        return status;
    }

    public static void fetchChapter(final Context context, final CommonMethodsCallback callback) {
        Log.e("IS_DATA_AVAILABLE ", "false");
        PrefManager manager = new PrefManager(context);
        context.getContentResolver().delete(DbContract.Chapter.CONTENT_URI, null, null);
        SharedPreferences pref = manager.getSharedPref();
        String uId = pref.getString(context.getResources().getString(R.string.uId), null);
        String token = pref.getString(context.getResources().getString(R.string.token), null);
        String dId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        Log.e("service", "service getData " + uId + ".,  " + token + "  , " + dId);

        //dialog.show();

        Call<ChapterResponse> responseCall = RetrofitClient.getRetrofitClient(BASE_URL)
                .create(RetrofitInterface.class)
                .getChapters(token, uId, "getAllChapters", dId);

        responseCall.enqueue(new Callback<ChapterResponse>() {
            @Override
            public void onResponse(Call<ChapterResponse> call, Response<ChapterResponse> response) {
                Log.e("service", "response" + response.toString());
                ChapterResponse chapterResponse = response.body();
                if (chapterResponse != null) {
                    Log.e("service", "service message" + chapterResponse.getMessage());
                    Log.e("service", "service status" + chapterResponse.getStatus());

                    if (!chapterResponse.getStatus().equals("0") && !chapterResponse.getStatus().equals("Session Expired.")) {
                        Log.e("service", "service message" + chapterResponse.getMessage());
                        dataBackUp(chapterResponse, context, callback);
                    } else {
                        Toast.makeText(context, chapterResponse.getMessage(), Toast.LENGTH_LONG).show();
                        //dialog.dismiss();
                        callback.callbackFromCommonMethods(false);
                        Log.e("service", "service message" + chapterResponse.getStatus() + ",");
                    }
                } else {
                    callback.callbackFromCommonMethods(false);
                }
            }

            @Override
            public void onFailure(Call<ChapterResponse> call, Throwable t) {
                call.cancel();
                // dialog.dismiss();
                callback.callbackFromCommonMethods(false);
            }
        });

    }

    private static void dataBackUp(final ChapterResponse response, final Context context, final CommonMethodsCallback callback) {
        PrefManager manager = new PrefManager(context);
        manager.getEditor().putString(IMAGE_BASE_URL, response.getServerUrl()).apply();
        List<Chapter> chapterList = response.getResultData();
        for (int i = 0; i < chapterList.size(); i++) {
            Chapter chapter = chapterList.get(i);

            if (!manager.getSharedPref().getBoolean(IS_DATA_AVAILABLE, false)) {
                ContentValues ChapterValues = new ContentValues();
                ChapterValues.put(CHAPTER_ID, chapter.getChapterId());
                ChapterValues.put(CHAPTER_NAME, chapter.getChapterName());
                ChapterValues.put(TESTAMENT_TYPE, chapter.getTestamentType());
                ChapterValues.put(IS_PAID, chapter.getIsPaid());
                ChapterValues.put(PAGE_COUNT, chapter.getNoOfPages());
                ChapterValues.put(IS_DOWNLOADED, context.getResources().getInteger(R.integer.NOT_DOWNLOADED));
                ChapterValues.put(IS_PURCHASED, chapter.getIsPurchased());
                ChapterValues.put(IS_WATCHED, chapter.getIsView());
                ChapterValues.put(IS_FAVOURITE, chapter.getIsFav());
                ChapterValues.put(CHAPTER_THUMB, chapter.getChapterThumb());
                ChapterValues.put(CHAPTER_THUMB_IMAGE, chapter.getChapterThumbImage());
                ChapterValues.put(CHAPTER_AMOUNT, chapter.getPrice());
                ChapterValues.put(IS_ACTIVE, chapter.getIsActive());
                ChapterValues.put(PURCHASED_VIA, chapter.getPurchasedVia());
                Uri chapterUri = context.getContentResolver().insert(DbContract.Chapter.CONTENT_URI, ChapterValues);
                Log.e("chapterUri ", "inserted " + chapterUri + " at pos i:" + i);
                List<ChapterPage> pageList = chapter.getChapterPages();
                for (int j = 0; j < pageList.size(); j++) {
                    ChapterPage chapterPage = pageList.get(j);
                    ContentValues chapterPageValues = new ContentValues();
                    chapterPageValues.put(PAGE_ID, chapterPage.getPageId());
                    chapterPageValues.put(CHAPTER_PAGE_ID, chapter.getChapterId());
                    chapterPageValues.put(PAGE_TITLE, chapterPage.getPageTitle());
                    chapterPageValues.put(PAGE_ORDER, chapterPage.getPageOrder());
                    chapterPageValues.put(IMAGE_URL, chapterPage.getUrl());
                    chapterPageValues.put(AUDIO_URL, chapterPage.getAudioName());
                    chapterPageValues.put(PAGE_AUDIO, context.getResources().getInteger(R.integer.NOT_DOWNLOADED));
                    chapterPageValues.put(IS_PAGE_DOWNLOADED, context.getResources().getInteger(R.integer.NOT_DOWNLOADED));
                    Uri pageUri = context.getContentResolver().insert(DbContract.ChapterPage.CONTENT_URI, chapterPageValues);
                    Log.e("pageUri ", "inserted " + pageUri + " at pos i:" + i + " at j:" + j);
                }
            } else {
                ContentValues updateValue = new ContentValues();
                updateValue.put(IS_ACTIVE, chapter.getIsActive());
                updateValue.put(IS_FAVOURITE, chapter.getIsFav());
                updateValue.put(IS_WATCHED, chapter.getIsView());
                updateValue.put(IS_PURCHASED, chapter.getIsPurchased());
                updateValue.put(PURCHASED_VIA, chapter.getPurchasedVia());
                int updatedRow = context.getContentResolver()
                        .update(DbContract.Chapter.CONTENT_URI, updateValue, CHAPTER_ID + "=?", new String[]{chapter.getChapterId()});
                Log.e("updatedRow", updatedRow + "");
            }
        }
        manager.getEditor().putBoolean(IS_DATA_AVAILABLE, true).apply();
        callback.callbackFromCommonMethods(true);
    }

    public static void getCountryList(final Context context, final CommonMethodsCallback callback) {
        final PrefManager pref = new PrefManager(context);
        String uId = pref.getSharedPref().getString(context.getResources().getString(R.string.uId), null);
        String token = pref.getSharedPref().getString(context.getResources().getString(R.string.token), null);
        String dId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("getCountryList", "called");
        RetrofitInterface retrofitInterface = RetrofitClient
                .getRetrofitClient(Constants.ACTION_BASE_URL).create(RetrofitInterface.class);
        Call<GetCountriesResponse> call = retrofitInterface.getCountries(token, uId, "getCountries", dId, context.getResources().getString(R.string.isTab));
        if (CommonMethods.isNetworkAvailable(context)) {
            Log.e("NetworkAvailable", "true");
            call.enqueue(new Callback<GetCountriesResponse>() {
                @Override
                public void onResponse(Call<GetCountriesResponse> call, Response<GetCountriesResponse> response) {
                    if (response != null) {
                        GetCountriesResponse countriesResponse = response.body();
                        Log.e("response", response.toString());
                        if (countriesResponse != null) {
                            int status = countriesResponse.getStatus();
                            String msg = countriesResponse.getMessage();
                            Log.e("countryList ", "status :" + status + " message " + msg);
                            if (status == 1) {
                                List<Country> countryList = countriesResponse.getArrCountries();
                                Log.e("countryList", countryList.size() + "");
                                ContentValues[] valueVector = new ContentValues[countryList.size()];
                                for (int i = 0; i < countryList.size(); i++) {
                                    ContentValues values = new ContentValues();
                                    values.put(COUNTRY_ID, countryList.get(i).getCountryId());
                                    values.put(COUNTRY_NAME, countryList.get(i).getUserName());
                                    valueVector[i] = values;
                                }
                                context.getContentResolver().bulkInsert(DbContract.CountryTable.CONTENT_URI, valueVector);
                                //dialog.dismiss();
                                pref.getEditor().putBoolean("country", true).apply();
                                callback.callbackFromCommonMethods(true);
                            }
                        } else {
                            Log.e("countriesResponse", "called");
                            //dialog.dismiss();
                            callback.callbackFromCommonMethods(false);
                        }
                    } else {
                        Log.e("response", "null");
                        // dialog.dismiss();
                        callback.callbackFromCommonMethods(false);
                    }
                }

                @Override
                public void onFailure(Call<GetCountriesResponse> call, Throwable t) {
                    //dialog.dismiss();
                    Log.e("onFailure", "called");
                    if (t != null)
                        Log.e("onFailure", t.getMessage() + " ");
                    callback.callbackFromCommonMethods(false);
                }
            });
        } else {
            Toast.makeText(context, "Internet Not Available", Toast.LENGTH_SHORT).show();
        }
    }

    private static void publishResults(Context context) {
        Intent intent = new Intent("signup_action");
        intent.putExtra("msg", "done");
        context.sendBroadcast(intent);
    }

    public static void goToNextActivity(Activity activity, String deviceId) {
        final PrefManager mPrefManager = new PrefManager(activity);
        SharedPreferences.Editor mEditor = mPrefManager.getEditor();
        if (mPrefManager.getSharedPref().getString(activity.getString(R.string.dId), null) != null) {
            activity.startActivity(new Intent(activity, DashboardActivity.class));
        } else {
            mEditor.putString(activity.getString(R.string.dId), deviceId).apply();
            activity.startActivity(new Intent(activity, DemoActivity.class));
        }
        activity.finish();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        try {

            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {

        }
    }

    public interface CommonMethodsCallback {
        void callbackFromCommonMethods(boolean resultOk);
    }

    public static String getUserDeviceData(Context context) {
        PrefManager pref = new PrefManager(context);
        String base64 = null;
        String uId = pref.getSharedPref().getString(context.getResources().getString(R.string.uId), null);
        String token = pref.getSharedPref().getString(context.getResources().getString(R.string.token), null);
        String dId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionToken", token);
            jsonObject.put("userId", uId);
            jsonObject.put("deviceId", dId);
            jsonObject.put("deviceType", context.getResources().getString(R.string.isTab));

            byte[] data = new byte[0];
            data = jsonObject.toString().getBytes("UTF-8");
            base64 = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return base64;
    }


    /**
     * @param title              String title for the Dialog
     * @param message            String message for the Dialog
     * @param positiveButtonText String text for the positive button
     * @param negativeButtonText String text for the positive button
     * @param callback           {@link LoginActivity.ActionCallback} callbacks for positive and negative button clicks
     */
    public static void showAlert(String title, String message, String positiveButtonText, String negativeButtonText, final ActionCallback callback, Context context) {
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context).setTitle(title).setMessage(message)
                .setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (callback != null) {
                            callback.onPositiveBtnClick();
                        }
                    }
                });
        if (negativeButtonText != null) {
            alertDialogBuilder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (callback != null) {
                        callback.onNegativeBtnClick();
                    }
                }
            });
        }

        android.app.AlertDialog alertDialog = alertDialogBuilder.show();

        alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
        alertDialog.getButton(android.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(android.R.color.darker_gray));
    }


    public interface ActionCallback {
        void onPositiveBtnClick();

        void onNegativeBtnClick();

    }
}
