package cpm.qbslearning.bibleforce.helperMethods;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rmn on 06-08-2017.
 */

public class PrefManager {
    static SharedPreferences mPreferences;
    static SharedPreferences.Editor mEditor;
    Context mContext;
    public PrefManager(Context context){
        mContext=context;
    }
    public  SharedPreferences.Editor getEditor(){
        mPreferences=mContext.getSharedPreferences("data",Context.MODE_PRIVATE);
        return mPreferences.edit();
    }

    public SharedPreferences getSharedPref(){
        return mContext.getSharedPreferences("data",Context.MODE_PRIVATE);
    }

    public void putInt(String key,int value){
    }
    public void putString(String key,String value){
    }
    public void putBoolean(String key,Boolean value){
    }
    public  void putIsAccessCodeAuthSuccess(Boolean value){
        getEditor().putBoolean("accessCodeAuthSuccess",value).apply();
    }
    public boolean getIsAccessCodeAuthSuccess(){
      return   getSharedPref().getBoolean("accessCodeAuthSuccess",false);
    }

//    public int getInt(String key){
//        return ;
//    }
//    public String  getString(String key){
//        return ;
//    }
//    public Boolean getBoolean(String key){
//        return ;
//    }
}
