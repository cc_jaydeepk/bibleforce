package cpm.qbslearning.bibleforce;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.pdf.PdfDocument;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.navigation.NavigationView;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cpm.qbslearning.bibleforce.adapter.FragmentPageAdapter;
import cpm.qbslearning.bibleforce.adapter.SearchResultAdapter;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.AccessCodeVerifyResponse;
import cpm.qbslearning.bibleforce.dataModel.ActionResponse;
import cpm.qbslearning.bibleforce.dataModel.AudioUploadResponse;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.service.ChapterDownloadService;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_FAVOURITE;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PAID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PURCHASED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_WATCHED;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.PURCHASED_VIA;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.TESTAMENT_TYPE;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.AUDIO_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.CHAPTER_PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.IMAGE_URL;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.ChapterPage.PAGE_TITLE;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.DATE;
import static cpm.qbslearning.bibleforce.data.DbContract.SharePage.PAGE_PATH;
import static cpm.qbslearning.bibleforce.util.Constants.ACTION_BASE_URL;
import static cpm.qbslearning.bibleforce.util.Constants.IS_AUDIO_ON;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_FAV;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_SHARE;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_UN_FAV;
import static cpm.qbslearning.bibleforce.util.Constants.OPTION_WATCHED;
import static cpm.qbslearning.bibleforce.util.Constants.getRecordingFilePath;

public class PageActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchResultAdapter.DownloadSearchCallback, PagerFragment.OnFragmentInteractionListener, View.OnClickListener {

    final static int STORAGE_PERMISSION_REQUEST_CODE = 0;
    private static final int AUDIO_REQUEST_CODE = 1;
    String audioSavePathInDevice = null;
    CountDownTimer timer = null;
    LinearLayout recordingView;
    TextView countView;
    private static final int REQUEST_SHARE_ACTION = 1;
    private int currentPosition = 0, pageCount = 0;
    private int mTestamentType = -1;
    ViewPager mPager;
    TextView pageTitleView;
    FragmentPageAdapter mAdapter;
    Cursor cursor;
    MediaPlayer mediaPlayer, recordMediaPlayer;
    List<String> audioUrls;
    List<String> imageUrls, chapterPageIds;
    ImageView mMoreView;
    SearchView mSearchView;
    boolean isMenuClose = true, isToolbarClose = true;
    static final int menuMaxHeight = 250;
    static final int menuMinHeight = 0;
    ImageView item1, item2, item3, item4;
    LinearLayout view;
    Toolbar toolbar;
    String title, mCId;
    PrefManager prefManager;
    MediaRecorder mediaRecorder;
    private static boolean isRecording = false;
    RelativeLayout mNewRelativeOut, mOldRelativeOut;
    private RecyclerView mRecyclerNewSearch, mRecyclerOldSearch;
    private SearchResultAdapter mOldResultAdapter, mNewResultAdapter;
    ImageView oldImageIcon, newImageIcon;
    int oldImageIconCounter = 1, newImageIconCounter = 1;
    KProgressHUD mProgressDialog;
    private String mChapterId;
    Dialog dialog;
    TextView text;
    private boolean isCallFromBuy = false;
    int height = 0;
    private boolean onStopCalled = false;
    private static final int CALLER = 3;
    private int seeklength = -1;

    boolean  isChapterFree = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fabric.with(this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        prefManager = new PrefManager(this);
        Intent intent = getIntent();
        final LinearLayout layout = (LinearLayout) findViewById(R.id.w);
        height = layout.getMeasuredHeight();
        Log.e("height---", "------------" + height);

        mAdapter = new FragmentPageAdapter(this, getSupportFragmentManager(), 0);

        Log.e("PageActivity", "called");
        if (intent != null) {
            mTestamentType = intent.getIntExtra(Constants.EXTRA_TESTAMENT_TYPE, 0);
            mCId = intent.getStringExtra(Constants.EXTRA_CHAPTER_ID);
            hitApi(OPTION_WATCHED);
            title = intent.getStringExtra(Constants.EXTRA_CHAPTER_TITLE);
            Log.e("title", title + "");
            ContentValues values = new ContentValues();
            values.put(DbContract.Chapter.IS_WATCHED, getResources().getInteger(R.integer.WATCHED));
            getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, DbContract.Chapter.CHAPTER_ID + "=?", new String[]{mCId});
        }

        audioUrls = new ArrayList<>();
        imageUrls = new ArrayList<>();
        chapterPageIds = new ArrayList<>();
        String[] projection = {CHAPTER_PAGE_ID, PAGE_ID, PAGE_TITLE, AUDIO_URL, IMAGE_URL};
        cursor = getContentResolver().query(DbContract.ChapterPage.CONTENT_URI, projection, CHAPTER_PAGE_ID + "=?", new String[]{mCId}, DbContract.ChapterPage.P_Id + " ASC");
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String audioUrl = cursor.getString(cursor.getColumnIndex(AUDIO_URL));
                String imageUrl = cursor.getString(cursor.getColumnIndex(IMAGE_URL));
                String pageId = cursor.getString(cursor.getColumnIndex(PAGE_ID));
                audioUrls.add(audioUrl);
                imageUrls.add(imageUrl);
                chapterPageIds.add(pageId);
                ++pageCount;
            }
        }
        ContentValues values = new ContentValues();
        values.put(IS_WATCHED, getResources().getInteger(R.integer.WATCHED));
        getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_PAGE_ID + "=?", new String[]{mCId});
        mAdapter.setCursor(cursor);

        registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));
    }

    private void initialViewSetup() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recordingView = (LinearLayout) findViewById(R.id.v_record_view);
        countView = (TextView) findViewById(R.id.v_count_txt);
        mPager = (ViewPager) findViewById(R.id.v_pager);
        pageTitleView = (TextView) findViewById(R.id.v_page_title);
        item1 = (ImageView) findViewById(R.id.q1);
        item2 = (ImageView) findViewById(R.id.q2);
        item3 = (ImageView) findViewById(R.id.q3);
        item4 = (ImageView) findViewById(R.id.q4);
        view = (LinearLayout) findViewById(R.id.content_page);
        item1.setOnClickListener(this);
        item2.setOnClickListener(this);
        item3.setOnClickListener(this);
        item4.setOnClickListener(this);
        view.setOnClickListener(this);

        oldImageIcon = (ImageView) findViewById(R.id.v_old_icon);
        newImageIcon = (ImageView) findViewById(R.id.v_new_icon);

        NavigationView navigationViewRight = (NavigationView) findViewById(R.id.nav_view_right);
        navigationViewRight.setNavigationItemSelectedListener(this);

        mNewRelativeOut = (RelativeLayout) findViewById(R.id.v_new_out);
        mOldRelativeOut = (RelativeLayout) findViewById(R.id.v_old_out);
        searchViewSetup();

        mOldRelativeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSearchClick(1, 1);
            }
        });
        mNewRelativeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSearchClick(2, 1);
            }
        });

        mRecyclerNewSearch = (RecyclerView) findViewById(R.id.v_new_recycle);
        mRecyclerOldSearch = (RecyclerView) findViewById(R.id.v_old_recycle);

        mProgressDialog = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        mOldResultAdapter = new SearchResultAdapter(this, this, 1);
        mRecyclerOldSearch.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerOldSearch.setHasFixedSize(true);
        mRecyclerOldSearch.setAdapter(mOldResultAdapter);

        mNewResultAdapter = new SearchResultAdapter(this, this, 2);
        mRecyclerNewSearch.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerNewSearch.setHasFixedSize(true);
        mRecyclerNewSearch.setAdapter(mNewResultAdapter);
        handleNewText("");

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_keyboard_backspace_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        pageTitleView.setText(title);
        mAdapter.setCursor(cursor);
        currentPosition = 0;
        mPager.setAdapter(mAdapter);

        if (mTestamentType == 1)
            setTitle(Constants.OLD_TESTAMENT);
        else
            setTitle(Constants.NEW_TESTAMENT);


        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                Log.e("onPageSelected ", "" + position + "  " + currentPosition);

                seeklength = -1;
                boolean isAudioOn = prefManager.getSharedPref().getBoolean(IS_AUDIO_ON, false);
                Log.e("onPageSelected ", "  " + isAudioOn);
                if (prefManager.getSharedPref().getBoolean(IS_AUDIO_ON, false))
                    playAudio(audioUrls.get(currentPosition));

                mPager.getOffscreenPageLimit();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

        });
        initialMenuSetup();
        setEnterExitAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e("PageActivity", "onResume() called");

        initialViewSetup();
        Log.e("onResumePage", "called page");
        handleMediaPlayer();
        String chapterId = prefManager.getSharedPref().getString(Constants.PAID_CHAPTER_ID, null);
        if (chapterId != null) {
            isCallFromBuy = true;
            mAdapter.notifyDataSetChanged();
            prefManager.getEditor().putString(Constants.PAID_CHAPTER_ID, null).apply();
            mChapterId = chapterId;
            handlePermission(/*chapterId*/);
        }

        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
//        registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("onPause", "called page");
        if (mediaPlayer != null)
            mediaPlayer.reset();
    }


    public void setEnterExitAnimation() {

    }

    private void initialMenuSetup() {
        Cursor chapterCursor = getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{IS_FAVOURITE}, CHAPTER_ID + "=?", new String[]{mCId}, null);
        if (chapterCursor != null) {
            chapterCursor.moveToFirst();
            int isFav = chapterCursor.getInt(chapterCursor.getColumnIndex(IS_FAVOURITE));
            if (isFav == getResources().getInteger(R.integer.FAVOURITE))
                item1.setImageResource(R.drawable.ic_fav_active);
            else
                item1.setImageResource(R.drawable.like);
            chapterCursor.close();
        }

        if (prefManager.getSharedPref().contains(IS_AUDIO_ON)) {
            if (!prefManager.getSharedPref().getBoolean(IS_AUDIO_ON, false)) {
                item3.setImageResource(R.drawable.audio_off);
            } else
                item3.setImageResource(R.drawable.audio_on);
        } else
            item3.setImageResource(R.drawable.audio_off);
    }

    private void searchViewSetup() {
        String text = "";
        mSearchView = (SearchView) findViewById(R.id.v_search);
        mSearchView.setQueryHint("Search");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                CommonMethods.hideSoftKeyboard(PageActivity.this);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                handleNewText(newText);
                return true;
            }
        });
    }

    private void handleNewText(String newText) {
        Log.e("text ", " " + newText);
        Cursor oldCursor = getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, TESTAMENT_TYPE + "=? AND " + CHAPTER_NAME + " LIKE ?", new String[]{"1", (newText == null || newText.trim().equals("")) ? (newText + "%") : ("%" + newText.trim() + "%")}, CHAPTER_ID + " ASC");
        Cursor newCursor = getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, TESTAMENT_TYPE + "=? AND " + CHAPTER_NAME + " LIKE ?", new String[]{"2", "%" + newText + "%"}, CHAPTER_ID + " ASC");
        if (newCursor != null) {
            handleSearchClick(2, 0);
            mNewResultAdapter.setData(newCursor);
        } else {
            Log.e("newcursor", "is null");
        }
        if (oldCursor != null) {
            handleSearchClick(1, 0);
            mOldResultAdapter.setData(oldCursor);
        } else {
            Log.e("oldcursor", "is null");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.page_menu, menu);
        return true;
    }

    public void handleMediaPlayer() {

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.e("onCompletion ", "called");
                if (++currentPosition < pageCount) {
                    Log.e("if currentposition ", currentPosition + " " + pageCount);
                    mPager.setCurrentItem(currentPosition);
                    playAudio(audioUrls.get(currentPosition));
                } else {
                    Log.e("else currentposition ", currentPosition + " " + pageCount);
                }

            }
        });

        if (prefManager.getSharedPref().contains(IS_AUDIO_ON)) {
            Log.e("IS_AUDIO_ON", "contains true");
            if (prefManager.getSharedPref().getBoolean(IS_AUDIO_ON, false)) {
                if (audioUrls.size() != 0)
                    playAudio(audioUrls.get(currentPosition));
            }
        } else {
            prefManager.getEditor().putBoolean(IS_AUDIO_ON, true).apply();
            if (audioUrls.size() != 0)
                playAudio(audioUrls.get(currentPosition));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, mTestamentType);
            intent.putExtra(Constants.EXTRA_IS_TESTAMENT_ADDED, true);
            Log.e("home", "clicked");
            navigateUpTo(intent);
            return true;
        } else if (id == R.id.action_search) {

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.END);

            return true;

        } else if (id == R.id.action_more) {
            if (isMenuClose) {
                isMenuClose = false;
                handleMenu(getResources().getInteger(R.integer.maxHeight));
            } else {
                isMenuClose = true;
                handleMenu(menuMinHeight);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String title) {
        Log.e("----------", "++++++++++++++++++++++++++++++++++");
    }

    @Override
    public void onFragmentTap(int i) {
        Log.e("onTap", "called" + i);
        if (isToolbarClose) {
            // getSupportActionBar().show();
            showActionBar();
            isToolbarClose = false;
        } else {
            if (!isMenuClose) {
                handleMenu(menuMinHeight);
                isMenuClose = true;
            }
            getSupportActionBar().hide();
            hideActionBar();
            isToolbarClose = true;
        }
    }

    public void playAudio(String name) {
        String[] files = fileList();
        String audioPath = Constants.getAudioFilePath(PageActivity.this);
        Log.e("audioPath ", audioPath + name);

        if (seeklength < 0) {
            try {
                mediaPlayer.reset();
                Log.e("audioPath ", "image" + name);
                FileInputStream inputStream = openFileInput("image" + name);
                // mediaPlayer.setDataSource("image" + name);
                mediaPlayer.setDataSource(inputStream.getFD());
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("playAudio error", e.getMessage() + "\n" + e.getLocalizedMessage());
            }
        } else {
            mediaPlayer.seekTo(seeklength);
        }
        mediaPlayer.start();

    }

    public void stopAudio(String name) {
        mediaPlayer.pause();
        seeklength = mediaPlayer.getCurrentPosition();
//        mediaPlayer.stop();
    }

    @Override
    protected void onDestroy() {
        if (cursor != null)
            cursor.close();
        if (mediaPlayer != null)
            mediaPlayer.release();
        if (mediaRecorder != null)
            mediaRecorder.reset();
        if (recordMediaPlayer != null) {
            recordMediaPlayer.release();
        }
        if (timer != null) {
            timer.cancel();
        }
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.q1:
                handleFavourite(mCId);
                break;
            case R.id.q2:
                handlePermission();
                String imageUri = Constants.getImageFilePath(PageActivity.this);
                imageUri = imageUri.concat(imageUrls.get(currentPosition));
                try {
                    FileInputStream inputStream = openFileInput(imageUri);

                    share(imageUri, inputStream);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(PageActivity.this, "File not found", Toast.LENGTH_SHORT).show();
                }

                Log.e("imageUri:", imageUri + "");
                Log.e("imageUrl", imageUri);
                isMenuClose = true;
                handleMenu(menuMinHeight);
                break;
            case R.id.q3:
                handleAudio();
                break;
            case R.id.q4:
                handleRecording();
                break;
            case R.id.content_page:
                if (isToolbarClose) {
                    // getSupportActionBar().show();
                    showActionBar();
                    isToolbarClose = false;
                } else {
                    if (!isMenuClose) {
                        handleMenu(menuMinHeight);
                        isMenuClose = true;
                    }
                    getSupportActionBar().hide();
                    hideActionBar();
                    isToolbarClose = true;
                }
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onSearchCallReceive(String chapterId, String chapterName, String price) {
        /*mChapterId = chapterId;
        Log.e("onSearchCallReceive", "apge " + isPurChased + "");
        if (!isPurChased) {
            showDialogForToken(chapterId, chapterName, price);
            // showDialogForTokenHaveOrNot(chapterId, chapterName, price);
        } else {
            int purchasedVia = 0;
            Cursor cursor1 = getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{PURCHASED_VIA}, CHAPTER_ID + "=?", new String[]{chapterId}, null);
            if (cursor1 != null) {
                Log.e("cursor1", "" + "not null");
                while (cursor1.moveToNext()) {
                    purchasedVia = cursor1.getInt(cursor1.getColumnIndex(PURCHASED_VIA));
                    Log.e("purchasedVia", "in while " + purchasedVia);
                    cursor1.close();
                    break;
                }
            }
            Log.e("purchasedVia", "" + purchasedVia);

            if (purchasedVia == 1) {
                isCallFromBuy = true;
                handlePermission();
                // showDialogForToken(chapterId, chapterName, price);
            } else if (purchasedVia == 2) {
                //showDialogForToken(chapterId);
                showDialogForToken(chapterId, chapterName, price);
            }
        }*/

         int isPaid = -9;
        int isPurchased = -1;

        Cursor cursor = getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{DbContract.Chapter.IS_PAID,DbContract.Chapter.IS_PURCHASED}, DbContract.Chapter.CHAPTER_ID + "=?", new String[]{chapterId}, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    isPaid = cursor.getInt(cursor.getColumnIndex(IS_PAID));
                    isPurchased = cursor.getInt(cursor.getColumnIndex(IS_PURCHASED));
                }
            }
            cursor.close();
        }

        if (isPaid == getResources().getInteger(R.integer.FREE) || (isPurchased==1)) {
            isChapterFree = true;
            mChapterId = chapterId;
            Log.e("isChapterFree", "" + isChapterFree + " cId " + mChapterId);
            handlePermission();
        } else {
            mChapterId = chapterId;
            PrefManager prefManager = new PrefManager(this);
            if (prefManager.getIsAccessCodeAuthSuccess()) {
                handlePermission();
            } else {
               // showDialogForToken(chapterId);
                showDialogForToken(chapterId, title, price);
            }
        }



    }



    /*@Override
    public void showDialog(String chapterId, String title, String price) {

    }*/


    public interface listnerCallback {
        public void reScale();
    }

    private void handleMenu(final int size) {
        final LinearLayout layout = (LinearLayout) findViewById(R.id.w);
        Log.e("height ", size + "");

        ValueAnimator anim = ValueAnimator.ofInt(layout.getMeasuredHeight(), -size);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
                layoutParams.height = val;
                layout.setLayoutParams(layoutParams);
            }
        });

        anim.setDuration(200);
        anim.start();
//        if (size==menuMinHeight)
//        layout.animate().scaleX(0).scaleY(-100f).setDuration(300);
//        else
//            layout.animate().scaleX(1).scaleY(1).setDuration(300);
//        int heightInDP = (int) convertPixelsToDp(size);
//        ValueAnimator anim = ValueAnimator.ofInt(layout.getMeasuredHeight(), heightInDP);
//        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                int val = (Integer) valueAnimator.getAnimatedValue();
//                ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
//                layoutParams.height = val;
//                layout.setLayoutParams(layoutParams);
//            }
//        });
//        anim.setDuration(100);
//        anim.start();
    }

    public float convertPixelsToDp(float px) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    protected void hideActionBar() {
        final ActionBar ab = getSupportActionBar();
        if (ab != null && ab.isShowing()) {
            if (toolbar != null) {
                ab.hide();
                toolbar.animate().translationY(-112).setDuration(80L)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                            }
                        }).setStartDelay(120L).start();
            } else {
                ab.hide();
            }
        }
    }

    protected void showActionBar() {
        ActionBar ab = getSupportActionBar();
        if (ab != null && !ab.isShowing()) {
            ab.show();
            if (toolbar != null) {
                toolbar.animate().translationY(0).setDuration(200L).start();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("PageActivity", "onActivityResult() called");
        if (requestCode == REQUEST_SHARE_ACTION) {
//            if (resultCode == RESULT_OK) {
            if (onStopCalled) {
                onStopCalled = false;
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String currentDate = sdf.format(calendar.getTime());
                Log.e("currentDate", currentDate);
                ContentValues values = new ContentValues();
                values.put(DATE, currentDate);
                values.put(PAGE_PATH, imageUrls.get(currentPosition));
                getContentResolver().insert(DbContract.SharePage.CONTENT_URI, values);
                hitApi(OPTION_SHARE);
            }
        }
    }

    public void handleFavourite(String chapterId) {
        Log.e("handleFavourite", "called");
        ContentValues values = new ContentValues();
        int updateCount = -1;
        Cursor cursor = getContentResolver().query(DbContract.Chapter.CONTENT_URI, new String[]{IS_FAVOURITE}, CHAPTER_ID + "=?", new String[]{chapterId}, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int isFavourite = cursor.getInt(cursor.getColumnIndex(IS_FAVOURITE));
            if (isFavourite == getResources().getInteger(R.integer.FAVOURITE)) {
                values.put(IS_FAVOURITE, getResources().getInteger(R.integer.UN_FAVOURITE));
                updateCount = getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_ID + "=?", new String[]{chapterId});
                if (updateCount > 0) {
                    Toast.makeText(this, "Successfully marked as unFavourite", Toast.LENGTH_SHORT).show();
                    item1.setImageResource(R.drawable.like);
                    hitApi(OPTION_UN_FAV);
                }
            } else {
                values.put(IS_FAVOURITE, getResources().getInteger(R.integer.FAVOURITE));
                updateCount = getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_ID + "=?", new String[]{chapterId});
                if (updateCount > 0) {
                    Toast.makeText(this, "Successfully marked as favourite", Toast.LENGTH_SHORT).show();
                    item1.setImageResource(R.drawable.ic_fav_active);
                    hitApi(OPTION_FAV);
                }
            }
            cursor.close();
        }
        handleNewText("");
    }


    private void prepareEmail(File report) {
        Uri uri = FileProvider.getUriForFile(getApplicationContext(), "com.loopbreakr.filesend", report);


        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");

        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        Intent chooser = Intent.createChooser(intent, "Share File");

        List<ResolveInfo> resInfoList = this.getPackageManager().queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY);

        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            this.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        startActivity(chooser);
    }

    public void share(String path, FileInputStream inputStream) {
        File dstFile;
        dstFile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                "/" + path);
        if (!dstFile.exists()) {
            try {
                boolean isCreated = dstFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("createNewFile", e.getLocalizedMessage());
            }
        }
        try {

            Intent intent = new Intent(Intent.ACTION_SEND);
            File file;

            boolean isCopied = copyFile(inputStream, dstFile);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + path);
            } else {
                file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                        "/" + path);
            }*/
            file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                    "/" + path);
            Log.e("share", " path " + path + " : " + file.getAbsolutePath());
            intent.setType("image/*");
            //Uri uri = Uri.fromFile(file);
            Uri uri = FileProvider.getUriForFile(getApplicationContext(), "cpm.qbslearning.bibleforce.fileprovider", file);
            // Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider",file);
            Log.e("imageUri--", uri.toString() + "");
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            startActivityForResult(Intent.createChooser(intent, "Share Via"), REQUEST_SHARE_ACTION);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("copyExecption", e.getLocalizedMessage());
        }


    }

    public void handleAudio() {
        Log.e("handleAudio", "called");
        if (prefManager.getSharedPref().getBoolean(IS_AUDIO_ON, false)) {
            prefManager.getEditor().putBoolean(IS_AUDIO_ON, false).apply();
            item3.setImageResource(R.drawable.audio_off);
            stopAudio(audioUrls.get(currentPosition));
        } else {
            prefManager.getEditor().putBoolean(IS_AUDIO_ON, true).apply();
            item3.setImageResource(R.drawable.audio_on);
            playAudio(audioUrls.get(currentPosition));
        }
    }

    public void handleRecording() {
        final int time = 20;
        Log.e("handleRecording", "called");
        if (!isRecording) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("hhmmssddMMyyyy");
            String currentTime = sdf.format(calendar.getTime());
            audioSavePathInDevice = getRecordingFilePath(PageActivity.this) + currentTime + ".mp3";
            Log.e("AudioSavePathInDevice", audioSavePathInDevice + "");
            handleAudioRecorderPermission();
        } else {
            if (timer != null) {
                timer.cancel();
                Log.e("handleRecording", "onFinish called");
                recordingView.setVisibility(View.GONE);
                countView.setText("");
                stopRecording();
                uploadAudio(audioSavePathInDevice);
                mProgressDialog.setLabel("Please Wait a while!!");
                mProgressDialog.setCancellable(false);
                mProgressDialog.show();
                playRecording(audioSavePathInDevice);
                isRecording = false;
            }
        }

    }

    public void shareImageAndAudio(String audioUrl) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        Intent intent = new Intent(Intent.ACTION_SEND);
        ArrayList<Uri> imageUris = new ArrayList<Uri>();
//        String imageUri = Constants.getImageFilePath(PageActivity.this);
        String imagePath = Constants.getImageFilePath(PageActivity.this).concat(imageUrls.get(currentPosition));
        Log.e("imageUri1", imagePath);
        Log.e("imageUri2", audioSavePathInDevice);
        Uri imageUri = Uri.parse(imagePath);
        Uri audioUri = Uri.parse(audioUrl);

        /*imageUris.add(audioUri);
        imageUris.add(imageUri);

        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
//         shareIntent.setType("text/*");
        // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setType("text/plain");
        //shareIntent.putExtra(Intent.EXTRA_STREAM, audioUri);
//        jkl
        shareIntent.putExtra(Intent.EXTRA_TEXT, audioUrl);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);*/

//        String imageUri = Constants.getImageFilePath(PageActivity.this);
//        imageUri = imageUri.concat(imageUrls.get(currentPosition));
        try {
            FileInputStream inputStream = openFileInput(imagePath);
            File dstFile = new File(Environment.getExternalStorageDirectory() + "/" + imagePath);
            if (!dstFile.exists()) {
                try {
                    boolean isCreated = dstFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("createNewFile", e.getLocalizedMessage());
                }
            }

            boolean isCopied = copyFile(inputStream, dstFile);
//            Intent intent = new Intent(Intent.ACTION_SEND);
            File file = new File(Environment.getExternalStorageDirectory() + "/" + imagePath);
            Log.e("share", " path " + imagePath + " : " + file.getAbsolutePath());
            intent.setType("*/*");
            //Uri uri = Uri.fromFile(file);
            Uri uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".fileprovider", file);
            // Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider",file);
            Log.e("imageUri--", uri.toString() + "");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
//            Uri audioUri = Uri.parse(audioUrl);
            intent.putExtra(Intent.EXTRA_TEXT, audioUrl);

        } catch (Throwable e) {
            e.printStackTrace();
        }
//        startActivity(Intent.createChooser(shareIntent, "Share files to.."));
        startActivity(Intent.createChooser(intent, "Share files to.."));
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Log.e("PageActivity","onStop() called");
        onStopCalled = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
//        Log.e("PageActivity","onStart() called");
        onStopCalled = false;
    }


    public String checkDigit(int number) {
//        if (number<=9)
//        return "0:0" + number ;
//
//        if (number<60)
//            return "0:"+String.valueOf(number);
//
        String secondMin = "";
        int minute = (int) number / 60;
        int second = number % 60;
        if (second <= 9)
            secondMin = "0" + second;
        else
            secondMin = "" + second;

        return minute + ":" + secondMin;
    }

    public void mediaRecorderReady(String audioSavePathInDevice) {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(audioSavePathInDevice);
    }

    public boolean startRecording(final String audioSavePathInDevice) {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
        }
        Log.e("startRecording", "called");

        mediaRecorderReady(audioSavePathInDevice);

        try {
            int time = 0;
            item4.setImageResource(R.drawable.recording_on);
            mediaRecorder.prepare();
            mediaRecorder.start();


            timer = new CountDownTimer(20000 * 100 * 60, 1000) {

                int time = 0;

                public void onTick(long millisUntilFinished) {

                    Log.e("handleRecording", "called onTick");
                    if (recordingView.getVisibility() == View.GONE)
                        recordingView.setVisibility(View.VISIBLE);

                    countView.setText(" Recording..." + checkDigit(time));
                    time++;
                }

                public void onFinish() {
                    Log.e("handleRecording", "onFinish called");
                    recordingView.setVisibility(View.GONE);
                    stopRecording();
                    isRecording = false;
                    //playRecording(audioSavePathInDevice);
                }
            };
            timer.start();
            isRecording = true;
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("recordingmsg", e.getLocalizedMessage());
        }
        // Toast.makeText(this, "Recording Started!! ", Toast.LENGTH_SHORT).show();
//        } else{
//            requestPermission();
//        }
        return false;
    }

    public void playRecording(final String audioSavePathInDevice) {
        recordMediaPlayer = new MediaPlayer();
        Log.e("playRecording", "called");
        recordMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.e("onCompletion ", "called");

                //playAudio(audioUrls.get(currentPosition));
                // uploadAudio(audioSavePathInDevice);
            }
        });
        try {
            recordMediaPlayer.setDataSource(audioSavePathInDevice);
            recordMediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recordMediaPlayer.start();
        // Toast.makeText(this, "Recording Playing", Toast.LENGTH_SHORT).show();
    }

    public void stopRecording() {
        Log.e("stopRecording", "called");
        if (mediaRecorder != null) {
            // mediaRecorderReady(audioSavePathInDevice);
            try {
                mediaRecorder.reset();
            } catch (RuntimeException e) {
                Log.e("stopRecording", "RuntimeException" + e.getMessage());
            }
            item4.setImageResource(R.drawable.recording_off);
            //Toast.makeText(this, "Recording Stop", Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadAudio(String audioSavePathInDevice) {
        Log.e("uploadAudio", "called");
        Log.e("audioSavePathInDevice", audioSavePathInDevice);
        RetrofitInterface retrofitInterface1 = RetrofitClient.getRetrofitClient(ACTION_BASE_URL)
                .create(RetrofitInterface.class);
        String token = prefManager.getSharedPref().getString(getString(R.string.token), null);
        final String uId = prefManager.getSharedPref().getString(getString(R.string.uId), null);
        String dId = prefManager.getSharedPref().getString(getString(R.string.dId), null);
        Log.e("token ", token);
        Log.e("uId ", uId);
        Log.e("dId ", dId);
        Log.e("options ", "audioUpload");
        Log.e("chapterPageId", chapterPageIds.get(currentPosition));

        File file = new File(audioSavePathInDevice);
        Log.e("file", file.getAbsolutePath());
        Log.e("file name", file.getName());
        RequestBody requestFile = RequestBody.create(MediaType.parse("audio/*"), file);
        RequestBody tokenBody = RequestBody.create(MediaType.parse("text/plain"), token);
        RequestBody uIdBody = RequestBody.create(MediaType.parse("text/plain"), uId);
        RequestBody dIdBody = RequestBody.create(MediaType.parse("text/plain"), dId);
        RequestBody optionBody = RequestBody.create(MediaType.parse("text/plain"), "audioUpload");
        RequestBody pIdBody = RequestBody.create(MediaType.parse("text/plain"), chapterPageIds.get(currentPosition));
        RequestBody cIdBody = RequestBody.create(MediaType.parse("text/plain"), mCId);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<AudioUploadResponse> responseCall = retrofitInterface1
                .uploadAudio(body, tokenBody, uIdBody, optionBody, dIdBody, cIdBody, pIdBody);
        responseCall.enqueue(new Callback<AudioUploadResponse>() {
            @Override
            public void onResponse(Call<AudioUploadResponse> call, Response<AudioUploadResponse> response) {
                mProgressDialog.dismiss();
                if (response != null) {
                    Log.e("response", response.toString());
                    AudioUploadResponse uploadResponse = response.body();
                    if (uploadResponse != null) {
                        Integer status = uploadResponse.getStatus();
                        String messge = uploadResponse.getMessage();
                        String serverUrl = uploadResponse.getServerUrl();
                        String url = uploadResponse.getUrl();
                        Log.e("status", status + "");
                        Log.e("message", messge);
                        Log.e("sercerUrl", serverUrl + "");
                        Log.e("url", url + "");
                        if (status > 0)
                            shareImageAndAudio(serverUrl + url);
                        else {
                        }

                    } else {
                        Log.e("audioResponse", "null");
                    }
                } else {
                    Log.e("response", "NULL");
                }
            }

            @Override
            public void onFailure(Call<AudioUploadResponse> call, Throwable t) {
                Log.e("onFailure", "called" + t.getMessage());
                mProgressDialog.dismiss();
                Toast.makeText(PageActivity.this, "Something went Wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void hitApi(String option) {
        Log.e("hitAPI", "called " + option);

        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(ACTION_BASE_URL)
                .create(RetrofitInterface.class);
        String token = prefManager.getSharedPref().getString(getString(R.string.token), null);
        String uId = prefManager.getSharedPref().getString(getString(R.string.uId), null);
        String dId = prefManager.getSharedPref().getString(getString(R.string.dId), null);
        Log.e("token ", "" + token);
        Log.e("uId ", "" + uId);
        Log.e("dId ", "" + dId);
        Log.e("option ", "" + option);
        Log.e("chapterId ", mCId + "");
        Call<ActionResponse> responseCall = null;

        switch (option) {
            case OPTION_FAV:
                responseCall = retrofitInterface.markFavourite(token, uId, option, dId, mCId);
                break;
            case OPTION_UN_FAV:
                responseCall = retrofitInterface.markUnFavourite(token, uId, option, dId, mCId);
                break;
            case OPTION_SHARE:
                responseCall = retrofitInterface.markShared(token, uId, option, dId, mCId);
                break;
            case OPTION_WATCHED:
                responseCall = retrofitInterface.markWatched(token, uId, option, dId, mCId);
                break;
        }

        if (responseCall != null)
            responseCall.enqueue(new Callback<ActionResponse>() {
                @Override
                public void onResponse(Call<ActionResponse> call, Response<ActionResponse> response) {
                    if (response != null) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        ActionResponse actionResponse = response.body();
                        if (actionResponse != null) {
                            Log.e("actionResponse", "not null");
                            Log.e("status", actionResponse.getStatus() + "");
                            Log.e("msg", actionResponse.getMessage() + "");
                        } else {
                            Log.e("actionResponse", "null");
                        }
                    } else {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        Log.e("response", "NULL");
                    }
                }

                @Override
                public void onFailure(Call<ActionResponse> call, Throwable t) {
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                }
            });
    }

    public void handleSearchClick(int i, int j) {

        if (j == 1) {
            if (i == 1) {
                if (mRecyclerOldSearch.getVisibility() == View.GONE) {
                    oldImageIcon.setImageResource(R.drawable.arrow_down);
                    mRecyclerOldSearch.setVisibility(View.VISIBLE);
                } else {
                    oldImageIcon.setImageResource(R.drawable.arrow_left);
                    mRecyclerOldSearch.setVisibility(View.GONE);
                }

            } else if (i == 2) {
                if (mRecyclerNewSearch.getVisibility() == View.VISIBLE) {
                    newImageIconCounter = 1;
                    newImageIcon.setImageResource(R.drawable.arrow_left);
                    mRecyclerNewSearch.setVisibility(View.GONE);
                } else {
                    newImageIconCounter = 0;
                    newImageIcon.setImageResource(R.drawable.arrow_down);
                    mRecyclerNewSearch.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (i == 1) {
                oldImageIcon.setImageResource(R.drawable.arrow_down);
                mRecyclerOldSearch.setVisibility(View.VISIBLE);
            } else if (i == 2) {
                newImageIcon.setImageResource(R.drawable.arrow_down);
                mRecyclerNewSearch.setVisibility(View.VISIBLE);
            }
        }
    }

    public void showDialogForTokenHaveOrNot(final String chapterId, final String title, final String price) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.ask_for_code);

        TextView noButton = dialog.findViewById(R.id.v_no);
        TextView yesButton = dialog.findViewById(R.id.v_yes);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                gotoBuyActivity(mChapterId, title, price);
            }
        });

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                // showDialogForToken("");
                showDialogForToken(chapterId, title, price);
            }
        });

        dialog.show();
    }

    public void showDialogForToken(final String chapterId, final String title, final String price) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.accesstoken_dialog);
        text = (TextView) dialog.findViewById(R.id.v_text);
        final EditText editText = dialog.findViewById(R.id.v_token);
        TextView cancelButton = dialog.findViewById(R.id.v_cancel);
        TextView okButton = dialog.findViewById(R.id.v_ok);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                hideSoftKeyboard(getActivity());
                String enterToken = editText.getText().toString();
                gotoBuyActivity(mChapterId, title, price);

               /* if (enterToken.trim().equals("")) {
                    text.setText("Access code can't be blank");
                } else {
                    //hideSoftKeyboard(getActivity());
                    VerifyAccessCode(PageActivity.this, enterToken);
                }*/
            }
        });

        dialog.show();
    }

    public boolean VerifyAccessCode(Context context, String accessCode) {

        if (CommonMethods.isNetworkAvailable(context)) {
            mProgressDialog.show();
            final PrefManager pref = new PrefManager(context);
            String uId = pref.getSharedPref().getString(context.getResources().getString(R.string.uId), null);
            String token = pref.getSharedPref().getString(context.getResources().getString(R.string.token), null);
            String dId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            boolean result[] = {false};
            RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL).create(RetrofitInterface.class);
            Call<AccessCodeVerifyResponse> call = retrofitInterface.verifyAccessCode(token, uId, "checkAccessCode", dId, accessCode);
            call.enqueue(new Callback<AccessCodeVerifyResponse>() {
                @Override
                public void onResponse(Call<AccessCodeVerifyResponse> call, Response<AccessCodeVerifyResponse> response) {
                    mProgressDialog.dismiss();
                    if (response != null) {
                        AccessCodeVerifyResponse verifyResponse = response.body();
                        if (verifyResponse != null) {
                            String msg = verifyResponse.getMessage();
                            int status = verifyResponse.getStatus();
                            if (status > 0) {
                                dialog.dismiss();

                                ContentValues values = new ContentValues();
                                values.put(DbContract.Chapter.IS_PURCHASED, getResources().getInteger(R.integer.PURCHASED));
                                getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, null, null);
                                handlePermission("");
                            } else {
                                text.setText("Invalid Access code!");
                            }
                        } else
                            Toast.makeText(PageActivity.this, "Something went wrong Please try again", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(PageActivity.this, "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AccessCodeVerifyResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                    Toast.makeText(PageActivity.this, "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                }
            });

        } else {

            Toast.makeText(PageActivity.this, "Internet not available", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    private void gotoBuyActivity(String id, String title, String price) {
        Log.e("gotoBuyActivity", "page " + "called");
        Intent intent = new Intent(this, BuyActivity.class);
        intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, mTestamentType);
        intent.putExtra(Constants.EXTRA_CHAPTER_ID, id);
        intent.putExtra(Constants.EXTRA_CHAPTER_TITLE, title);
        intent.putExtra(Constants.EXTRA_CHAPTER_PRICE, price);
        startActivity(intent);
        dialog.dismiss();
    }

    public void handlePermission() {

        int writePermissionCheck = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (writePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.e("permission ", "not granted");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
        } else {
            Log.e("permission ", "granted");
            //  mProgressDialog.show();
            String cId = null;
            if (isCallFromBuy)
                cId = mChapterId;
            //ChapterDownloadService.startChapterDownloading(this, null, cId, 0, CALLER);
        }
    }

    public void handlePermission(String chapterId) {

        int writePermissionCheck = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (writePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.e("permission ", "not granted");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);

        } else {
            Log.e("permission ", "granted");
            mProgressDialog.show();
            // ChapterDownloadService.startChapterDownloading(PageActivity.this, null, mChapterId, 0, CALLER);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission ", "granted");
                    mProgressDialog.show();
                    String cId = null;
                    if (isCallFromBuy)
                        cId = mChapterId;
                    // ChapterDownloadService.startChapterDownloading(this, null, cId, 0, CALLER);
                } else {
                    // This is Case 1 again as Permission is not granted by user

                    //Now further we check if used denied permanently or not
                    if (ActivityCompat.shouldShowRequestPermissionRationale(PageActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showDialogOK("Some Permissions are required for downloading books",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                handlePermission(mChapterId);
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                // proceed with logic by disabling the related features or quit the app.
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                });
                    } else {
                        explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                    }
                }
                break;
            case AUDIO_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startRecording(audioSavePathInDevice);
                }
                break;
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(PageActivity.this, R.style.AlertDialogTheme)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void explain(String msg) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(PageActivity.this, R.style.AlertDialogTheme);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        dialog.create().dismiss();

                    }
                });
        dialog.show();
    }

    public void handleAudioRecorderPermission() {
        int isGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if (isGranted != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, AUDIO_REQUEST_CODE);
        } else {
            startRecording(audioSavePathInDevice);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra(Constants.EXTRA_TESTAMENT_TYPE, mTestamentType);
        intent.putExtra(Constants.EXTRA_IS_TESTAMENT_ADDED, true);
        navigateUpTo(intent);
        Log.e("backPressed", "clicked");
        super.onBackPressed();
    }

    //    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == AUDIO_REQUEST_CODE) {
//           
//        }
//    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Log.e("PageActivity", "onReceive");
            if (bundle != null) {
                Log.e("onRecieve", "intent not null");
                Log.e("onReceive", "onReceive called from PageActivity");
                int resultCode = bundle.getInt(ChapterDownloadService.RESULT);
                String chapterId = bundle.getString(Constants.EXTRA_CHAPTER_ID);
                int position = bundle.getInt(Constants.EXTRA_ITEM_POSITION, -1);
                int caller = bundle.getInt(Constants.EXTRA_CALLER, -1);
                if (caller == CALLER) {
                    if (/*resultCode == 1*/true) {

                        ContentValues values = new ContentValues();
                        values.put(DbContract.Chapter.IS_DOWNLOADED, getResources().getInteger(R.integer.DOWNLOADED));
                        Log.e("OnRecieve", resultCode + "");
                        int id = getContentResolver().update(DbContract.Chapter.CONTENT_URI,
                                values, CHAPTER_ID + "=?", new String[]{chapterId});
                        if (id > 0) {
                            // mHolder.buttonImage.setImageResource(R.drawable.view);
                            PrefManager mPrefManager = new PrefManager(PageActivity.this);
                            mPrefManager.getEditor().putString(Constants.PAID_CHAPTER_ID, null).commit();
                            mAdapter.notifyDataSetChanged();
                            mOldResultAdapter.notifyDataSetChanged();
                            mNewResultAdapter.notifyDataSetChanged();
                        }
                        mOldResultAdapter.notifyDataSetChanged();
                        mNewResultAdapter.notifyDataSetChanged();
                        mAdapter.notifyDataSetChanged();
                       /* Toast.makeText(PageActivity.this, "Download Completed",
                                Toast.LENGTH_LONG).show();*/
                    } else {
                        Toast.makeText(PageActivity.this, "Download failed",
                                Toast.LENGTH_LONG).show();
                        mAdapter.notifyDataSetChanged();
                        mOldResultAdapter.notifyDataSetChanged();
                        mNewResultAdapter.notifyDataSetChanged();
                    }
                }
            }
//            mHolder.buttonImage.setEnabled(true);
            mProgressDialog.dismiss();
            // mHolder.progressBar.setVisibility(View.GONE);
        }
    };


    private boolean copyFile(FileInputStream src, File dst) throws IOException {
        Log.e("copyFile", "called");
        if (src == null) {
            return false;
        } else {
            if (dst.isFile()) {
                dst.mkdir();
            }
            FileOutputStream os = new FileOutputStream(dst);
            byte[] buff = new byte[1024];
            int len;
            while ((len = src.read(buff)) > 0) {
                os.write(buff, 0, len);
            }
            src.close();
            os.close();
        }
        return true;
    }

}
