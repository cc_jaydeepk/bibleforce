package cpm.qbslearning.bibleforce;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.provider.Settings;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

//import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.adapter.SearchResultAdapter;
import cpm.qbslearning.bibleforce.adapter.TestamentAdapter;
import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.FileDownloading;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.service.ChapterDownloadService;
import cpm.qbslearning.bibleforce.util.Constants;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_NAME;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.TESTAMENT_TYPE;

public class TestamentActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener, SearchResultAdapter.DownloadSearchCallback,TestamentAdapter.DownloadCallback, GoogleApiClient.OnConnectionFailedListener {

    final static int STORAGE_PERMISSION_REQUEST_CODE = 1;
    String chapterIdDownloading = null, mChapterId = null;
    private static final int CALLER = 4;

    Cursor cursor;
    int testamentType=-1;
    RecyclerView mRecyclerView;
    TestamentAdapter mAdapter;
    TextView headerView;
    SearchView mSearchView;
    TestamentAdapter.Holder mHolder;
    RelativeLayout mOldRelativeLayout, mNewRelativeLayout, mOldRelativeOut, mNewRelativeOut;
    RecyclerView mRecyclerOldSearch, mRecyclerNewSearch;
    SearchResultAdapter mOldResultAdapter, mNewResultAdapter;
    ProgressDialog mProgressDialog;
    Cursor searchCursor;
    ImageView oldImageIcon, newImageIcon;
    int oldImageIconCounter = 1, newImageIconCounter = 1;
    boolean rightDrawerCounter = false;
    DownloadManager mDownloadManager;
    Uri download_Uri;
    List<Long> downloadingRequest = new ArrayList<>();
    List<FileDownloading> downloadingRequestId = new ArrayList<>();
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Log.e("TESTAMENT ", "onRecieve");
            if (bundle != null) {

                Log.e("onRecieve ", "intent not null");
                int resultCode = bundle.getInt(ChapterDownloadService.RESULT);
                String chapterId = bundle.getString(Constants.EXTRA_CHAPTER_ID);
                int position = bundle.getInt(Constants.EXTRA_ITEM_POSITION, -1);
                int caller = bundle.getInt(Constants.EXTRA_CALLER, -1);
                int requestCode = bundle.getInt(Constants.DOWNLOAD_SERVICE_REQUEST_CODE, -1);
                int percentage = bundle.getInt(Constants.DOWNLOAD_PERCENTAGE, 0);

                if(caller == CALLER) {
                    if(requestCode == Constants.DOWNLOAD_COMPLETE) {
                    if (resultCode == 1) {
                        ContentValues values = new ContentValues();
                        values.put(DbContract.Chapter.IS_DOWNLOADED, getResources().getInteger(R.integer.DOWNLOADED));
                        Log.e("OnRecieve", resultCode + "");
                        int id = getContentResolver().update(DbContract.Chapter.CONTENT_URI,
                                values, CHAPTER_ID + "=?", new String[]{chapterId});
                        if (id > 0) {
                            // mHolder.buttonImage.setImageResource(R.drawable.view);
                            mAdapter.notifyItemChanged(position);
                        }
                        Toast.makeText(TestamentActivity.this, "Download Completed",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(TestamentActivity.this, "Download failed",
                                Toast.LENGTH_LONG).show();
                        mAdapter.notifyDataSetChanged();
                    }
                        mProgressDialog.setProgress(0);
                        mProgressDialog.dismiss();
                    }
                    else if(requestCode == Constants.PERCENTAGE)
                    {
                        Log.e("PERCENTAGE", "PERCENTAGE_DOWNLOAD" + percentage);
                            /*mProgressDialog = new ProgressDialog(getContext());
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setMessage("DOWNLOADING CHAPTER");
                            mProgressDialog.setMax(100);*/
                        // mProgressDialog.setProgressPercentFormat("%");
                        mProgressDialog.setTitle(getString(R.string.downloading_chapter_title));
                        mProgressDialog.setProgress(percentage);

//                            mProgressDialog.setCancelable(false);
                        // mProgressDialog.show();
                    }
                }
            }
//            mHolder.buttonImage.setEnabled(true);
//            mProgressDialog.dismiss();
            // mHolder.progressBar.setVisibility(View.GONE);
        }
    };

//
//    BroadcastReceiver onComplete = new BroadcastReceiver() {
//
//        public void onReceive(Context ctxt, Intent intent) {
//
//
//            // get the refid from the download manager
//            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
//            Log.e("onComplete ", "BroadcastReceiver referenceId " + referenceId);
//// remove it from our list
//            if (referenceId != -1) {
//                downloadingRequest.remove(referenceId);
//            }
//// if list is empty means all downloads completed
//            if (downloadingRequest.isEmpty()) {
//                Log.e("onComplete ", "BroadcastReceiver downnload complete ");
//
//// show a notification
//
//                ContentValues values = new ContentValues();
//                values.put(DbContract.Chapter.IS_DOWNLOADED, getResources().getInteger(R.integer.DOWNLOADED));
//                getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_ID + "=?", new String[]{chapterIdDownloading});
//                Log.e("INSIDE", "" + referenceId);
//                NotificationCompat.Builder mBuilder =
//                        (NotificationCompat.Builder) new NotificationCompat.Builder(TestamentActivity.this)
//                                .setSmallIcon(R.mipmap.ic_launcher)
//                                .setContentTitle("GadgetSaint")
//                                .setContentText("All Download completed");
//
//                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                notificationManager.notify(455, mBuilder.build());
//                mAdapter.notifyDataSetChanged();
//                mProgressDialog.dismiss();
//            }
//
//        }
//    };

    private GoogleSignInOptions mGso;
    private GoogleApiClient mGoogleApiClient;
    private RelativeLayout dashboardView,logoutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testament);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        dashboardView=(RelativeLayout)findViewById(R.id.dashboard);
        logoutView=(RelativeLayout)findViewById(R.id.logout);
        headerView = (TextView) findViewById(R.id.header);
        logoutView.setOnClickListener(this);
        dashboardView.setOnClickListener(this);
        headerView.setOnClickListener(this);

        mDownloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        mNewRelativeOut = (RelativeLayout) findViewById(R.id.v_new_out);
        mOldRelativeOut = (RelativeLayout) findViewById(R.id.v_old_out);
        searchViewSetup();
        Log.e("TESTAMENT ", "called");
        oldImageIcon = (ImageView) findViewById(R.id.v_old_icon);
        newImageIcon = (ImageView) findViewById(R.id.v_new_icon);
        mOldRelativeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSearchClick(1, 1);
            }
        });

        mNewRelativeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSearchClick(2, 1);
            }
        });

        mOldRelativeLayout = (RelativeLayout) findViewById(R.id.v_old);
        mNewRelativeLayout = (RelativeLayout) findViewById(R.id.v_new);

        /*mProgressDialog =  new ProgressDialog(this,R.style.AppCompatAlertDialogStyle);
        mProgressDialog.setMessage("Chapter Downloading...");
        mProgressDialog.setCancelable(false);*/

        mProgressDialog =  new ProgressDialog(this,ProgressDialog.THEME_DEVICE_DEFAULT_DARK);
        mProgressDialog.setMessage("Chapter Downloading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);

        if (getIntent() != null)
            testamentType = getIntent().getIntExtra(Constants.EXTRA_TESTAMENT_TYPE, -1);
        Log.e("testament", testamentType + "");
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        NavigationView navigationViewRight = (NavigationView) findViewById(R.id.nav_view_right);
        navigationViewRight.setNavigationItemSelectedListener(this);
        mAdapter = new TestamentAdapter(this, this, testamentType);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycle);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(getResources().getInteger(R.integer.column), StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerNewSearch = (RecyclerView) findViewById(R.id.v_new_recycle);
        mRecyclerOldSearch = (RecyclerView) findViewById(R.id.v_old_recycle);

        if (testamentType >= 0) {
            Cursor cursor = getCursor(testamentType);
            if (testamentType == 1) {
                headerView.setText(Constants.OLD_TESTAMENT);
            } else {
                headerView.setText(Constants.NEW_TESTAMENT);
            }
            if (cursor != null && cursor.getCount() > 0) {
                mAdapter.setData(cursor);
            }
        }

        mGso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, mGso)
                .build();

        mOldResultAdapter = new SearchResultAdapter(this,this,1);
        mRecyclerOldSearch.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerOldSearch.setHasFixedSize(true);
        mRecyclerOldSearch.setAdapter(mOldResultAdapter);

        mNewResultAdapter = new SearchResultAdapter(this,this,2);
        mRecyclerNewSearch.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerNewSearch.setHasFixedSize(true);
        mRecyclerNewSearch.setAdapter(mNewResultAdapter);
        handleNewText("");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide(Gravity.END);
            slide.addTarget(R.id.recycle);
            slide.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.interpolator.linear_out_slow_in));
            slide.setDuration(500);
            getWindow().setEnterTransition(slide);
        }

    }

    private void searchViewSetup() {
        String text = "";
        mSearchView = (SearchView) findViewById(R.id.v_search);
        mSearchView.setQueryHint("Search");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                handleNewText(newText);
                return true;
            }
        });
    }

    private void handleNewText(String newText) {
        Log.e("text ", " " + newText);
        Cursor oldCursor = getContentResolver()
                .query(DbContract.Chapter.CONTENT_URI, null, TESTAMENT_TYPE + "=? AND " + CHAPTER_NAME + " LIKE ?", new String[]{"1", (newText == null || newText.trim().equals("")) ? (newText + "%") : ("%" + newText.trim() + "%")}, CHAPTER_ID + " ASC");

        Cursor newCursor = getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, TESTAMENT_TYPE + "=? AND " + CHAPTER_NAME + " LIKE ?", new String[]{"2", "%" + newText + "%"}, CHAPTER_ID + " ASC");
        if (newCursor != null) {
            handleSearchClick(2, 0);
            mNewResultAdapter.setData(newCursor);
        } else {
            Log.e("newcursor", "is null");
        }
        if (oldCursor != null) {
            handleSearchClick(1, 0);
            mOldResultAdapter.setData(oldCursor);
        } else {
            Log.e("oldcursor", "is null");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));
    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public Cursor getCursor(int testamentType) {
        String selection = DbContract.Chapter.TESTAMENT_TYPE + "=?";
        String args[] = {Integer.toString(testamentType)};
        cursor = getContentResolver().query(DbContract.Chapter.CONTENT_URI, null, selection, args, CHAPTER_ID + " ASC");
        Log.e("cursor", "setup" + "size " + cursor.getCount());
        return cursor;

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.testament, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.END);

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            startActivity(new Intent(this, DashboardActivity.class));
            finish();
        } else if (id == R.id.logout) {
            PrefManager manager = new PrefManager(this);
            manager.getEditor().remove(getString(R.string.token)).apply();
            manager.getEditor().remove(getString(R.string.uId)).apply();

            String loginType = manager.getSharedPref().getString(getString(R.string.login_type), null);

            if (loginType != null) {
                switch (loginType) {

                    case "2":
//                        LoginManager.getInstance().logOut();
                        break;

                    case "3":
                        /*Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                    }
                                });*/
                        break;
                }
            }
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    public void handleSearchClick(int i, int j) {

        if (j == 1) {
            if (i == 1) {
                if (mRecyclerOldSearch.getVisibility() == View.GONE) {
                    oldImageIcon.setImageResource(R.drawable.arrow_down);
                    mRecyclerOldSearch.setVisibility(View.VISIBLE);
                } else {
                    oldImageIcon.setImageResource(R.drawable.arrow_left);
                    mRecyclerOldSearch.setVisibility(View.GONE);
                }
            } else if (i == 2) {
                if (mRecyclerNewSearch.getVisibility() == View.VISIBLE) {
                    newImageIconCounter = 1;
                    newImageIcon.setImageResource(R.drawable.arrow_left);
                    mRecyclerNewSearch.setVisibility(View.GONE);
                } else {
                    newImageIconCounter = 0;
                    newImageIcon.setImageResource(R.drawable.arrow_down);
                    mRecyclerNewSearch.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (i == 1) {
                oldImageIcon.setImageResource(R.drawable.arrow_down);
                mRecyclerOldSearch.setVisibility(View.VISIBLE);
            } else if (i == 2) {
                newImageIcon.setImageResource(R.drawable.arrow_down);
                mRecyclerNewSearch.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onCallReceive(String chapterId) {
        mChapterId = chapterId;
        handlePermission(chapterId);
    }

    @Override
    public void showDialog(String chapterId, String title,String price) {

    }



    public void handlePermission(String chapterId) {
        int permissionCheck = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.e("permission ", "not granted");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
        } else {
            Log.e("permission ", "granted");
            mProgressDialog.show();
//            mProgressDialog.setProgress(0);
            ChapterDownloadService.startChapterDownloading(TestamentActivity.this, null, mChapterId, 0,CALLER);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission ", "granted");
                    mProgressDialog.show();
//                    mProgressDialog.setProgress(0);
                    ChapterDownloadService.startChapterDownloading(TestamentActivity.this, null, mChapterId, 0,CALLER);
                } else {
                    // This is Case 1 again as Permission is not granted by user

                    //Now further we check if used denied permanently or not
                    if (ActivityCompat.shouldShowRequestPermissionRationale(TestamentActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ) {
                        showDialogOK("Some Permissions are required for downloading books",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                handlePermission(mChapterId);
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                // proceed with logic by disabling the related features or quit the app.
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                });
                    } else {
                        explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                    }
                }
        }
    }
//    @Override
//    public void onSearchCallReceive(String chapterId) {
//        mChapterId = chapterId;
//        handlePermission(chapterId);
//    }


    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(TestamentActivity.this, R.style.AlertDialogTheme)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void explain(String msg) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(TestamentActivity.this, R.style.AlertDialogTheme);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        dialog.create().dismiss();

                    }
                });
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        switch (view.getId()){
            case R.id.dashboard:
                startActivity(new Intent(this, DashboardActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                finish();
                break;
            case R.id.logout:

                PrefManager manager = new PrefManager(this);
                manager.getEditor().remove(getString(R.string.token)).apply();
                manager.getEditor().remove(getString(R.string.uId)).apply();

                String loginType = manager.getSharedPref().getString(getString(R.string.login_type), null);

                if (loginType != null) {
                    switch (loginType) {

                        case "2":
//                            LoginManager.getInstance().logOut();
                            break;

                        case "3":
                            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                    new ResultCallback<Status>() {
                                        @Override
                                        public void onResult(Status status) {
                                        }
                                    });
                            break;
                    }
                }
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                drawer.closeDrawer(GravityCompat.START);

                break;
        }
    }

    /*@Override
    public void onSearchCallReceive(String chapterId, String chapterName, boolean isPurchased,String price) {

    }*/

    @Override
    public void onSearchCallReceive(String chapterId, String chapterName, String price) {

    }
}
