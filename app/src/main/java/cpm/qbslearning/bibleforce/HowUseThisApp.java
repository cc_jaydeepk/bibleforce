package cpm.qbslearning.bibleforce;

import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class HowUseThisApp extends AppCompatActivity {
    PDFView pdfView;
    private PdfRenderer mPdfRenderer;
    private ParcelFileDescriptor mFileDescriptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_how_use_this_app);
        ImageView imageView = (ImageView) findViewById(R.id.v_back);
        LinearLayout llHowToUseBack = findViewById(R.id.ll_how_to_use_back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        llHowToUseBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        pdfView = (PDFView) findViewById(R.id.web);
        pdfView.setMinZoom(1.3f);
        showPdfLib();
    }

    public void showPdfLib() {
        File file = new File("file:///android_asset/bible_how_use.pdf");
//        //try {
//            pdfView.fromAsset("android_asset/pdf/bible_how_use.pdf")
        try {
            pdfView.fromStream(getAssets().open("bible_how_use.pdf"))
//                    .enableSwipe(true) // allows to b lock changing pages using swipe
//                    .swipeHorizontal(false)
//                    .enableDoubletap(true)
//                    .defaultPage(0)
//                    //.pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
//                    // allows to draw something on the current page, usually visible in the middle of the screen
//    //                .onDraw(onDrawListener)
//    //                // allows to draw something on all pages, separately for every page. Called only for visible pages
//    //                .onDrawAll(onDrawListener)
//    //                .onLoad(onLoadCompleteListener) // called after document is loaded and starts to be rendered
//    //                .onPageChange(onPageChangeListener)
//    //                .onPageScroll(onPageScrollListener)
//    //                .onError(onErrorListener)
//    //                .onPageError(onPageErrorListener)
//    //                .onRender(onRenderListener) // called after document is rendered for the first time
//    //                // called on single tap, return true if handled, false to toggle scroll handle visibility
//    //                .onTap(onTapListener)
//                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
//                    .password(null)
//                    .scrollHandle(null)
                    // .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    // .spacing(0)
                    //                .linkHandler(DefaultLinkHandler)
                    //                .pageFitPolicy(FitPolicy.WIDTH)
                    .load();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.e("error",e.getMessage());
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void showPdf() {
        File file = new File(getCacheDir(), "bible_how_use.pdf");
        if (!file.exists()) {
            // Since PdfRenderer cannot handle the compressed asset file directly, we copy it into
            // the cache directory.
            InputStream asset = null;
            try {
                asset = getAssets().open("bible_how_use.pdf");
                FileOutputStream output = new FileOutputStream(file);
                final byte[] buffer = new byte[1024];
                int size;
                while ((size = asset.read(buffer)) != -1) {
                    output.write(buffer, 0, size);
                }
                asset.close();
                output.close();

                mFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                // This is the PdfRenderer we use to render the PDF.
                if (mFileDescriptor != null) {
                    mPdfRenderer = new PdfRenderer(mFileDescriptor);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
