package cpm.qbslearning.bibleforce;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardMultilineWidget;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.PaymentResponse;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.CHAPTER_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.Chapter.IS_PURCHASED;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_ID;
import static cpm.qbslearning.bibleforce.data.DbContract.CountryTable.COUNTRY_NAME;
import static cpm.qbslearning.bibleforce.util.Constants.EXTRA_CHAPTER_ID;
import static cpm.qbslearning.bibleforce.util.Constants.EXTRA_CHAPTER_PRICE;
import static cpm.qbslearning.bibleforce.util.Constants.EXTRA_CHAPTER_TITLE;
import static cpm.qbslearning.bibleforce.util.Constants.EXTRA_IS_TESTAMENT_ADDED;
import static cpm.qbslearning.bibleforce.util.Constants.EXTRA_TESTAMENT_TYPE;

public class BuyActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Card cardToSave;
    CardInputListener mCardInputListener;
    CardMultilineWidget mCardMultilineWidget;
    ImageView cardView;
    RotateAnimation rotate;
    Spinner countryView;
    String PAYMENT_BASE_URL = "http://bibleapp-v1.regenapps.com/";
    String mChapterId, mChapterName;
    int mTestamentType;
    Button doneView;
    ImageView backView;
    KProgressHUD mProgressDialog;
    PrefManager prefManager;
    private LinearLayout rootView;
    List<String> countryList = new ArrayList<>();
    private String mChapterPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fabric.with(this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);
        rootView = (LinearLayout) findViewById(R.id.root_buy);
        CommonMethods.setupUI(this, rootView);
        countryView = (Spinner) findViewById(R.id.v_country);

        countryList.add("Select Country");
        Cursor cursor = getContentResolver()
                .query(DbContract.CountryTable.CONTENT_URI, null, null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String cID = cursor.getString(cursor.getColumnIndex(COUNTRY_ID));
                String cName = cursor.getString(cursor.getColumnIndex(COUNTRY_NAME));
                countryList.add(cName);
            }
            cursor.close();
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.country_select_item, countryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryView.setAdapter(adapter);
        countryView.setSelection(0);
        countryView.setOnItemSelectedListener(this);

        doneView = (Button) findViewById(R.id.v_done);
        backView = (ImageView) findViewById(R.id.v_back);
        mProgressDialog = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        prefManager = new PrefManager(this);

        if (getIntent() != null) {
            mChapterId = getIntent().getStringExtra(EXTRA_CHAPTER_ID);
            mChapterName = getIntent().getStringExtra(EXTRA_CHAPTER_TITLE);
            mChapterPrice = getIntent().getStringExtra(EXTRA_CHAPTER_PRICE);
            mTestamentType = getIntent().getIntExtra(EXTRA_TESTAMENT_TYPE, -1);
            Log.e("mTestamentType", mTestamentType + "------------in getintent-----------------------------");
            Log.e("mChapterId", mChapterId + "------------in getintent-----------------------------");
            Log.e("mChapterName", mChapterName + "------------in getintent-----------------------------");
            Log.e("mChapterPrice", mChapterPrice + "------------in getintent-----------------------------");
        }

        prefManager = new PrefManager(this);

        //4358 0125 6661 1278

        doneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("onClick", "called");
                if (mChapterId != null)
                    if (CommonMethods.isNetworkAvailable(BuyActivity.this)) {

                        cardToSave = mCardMultilineWidget.getCard();

                        if (cardToSave != null && cardToSave.validateCard()) {
                            doneView.setEnabled(false);
                            doneView.setClickable(false);

                            String cvc = cardToSave.getCVC();
                            String number = cardToSave.getNumber();
                            String expMonth = Integer.toString(cardToSave.getExpMonth());
                            String expYear = Integer.toString(cardToSave.getExpYear());
                            cardToSave = new Card(number, Integer.parseInt(expMonth), Integer.parseInt(expYear), cvc);
                            Stripe stripe = new Stripe(BuyActivity.this, "pk_live_j1BTkEeoLKlROCMwoVbB47UT");

                            mProgressDialog.show();

                            stripe.createToken(cardToSave, new TokenCallback() {

                                @Override
                                public void onError(Exception error) {
                                    mProgressDialog.dismiss();
                                    doneView.setEnabled(true);
                                    doneView.setClickable(true);
                                    finish();
                                    Toast.makeText(BuyActivity.this, "Payment Unsuccessful", Toast.LENGTH_SHORT).show();
                                }


                                @Override
                                public void onSuccess(Token token) {
                                    String currToken = token.toString();
                                    Log.e("Id", token.getId());
                                    Log.e("token value", String.valueOf(token));
                                    Log.e("token", currToken);
                                    byte[] data = new byte[0];
                                    try {
                                        data = token.getId().getBytes("UTF-8");
                                        final String sessionToken = prefManager.getSharedPref().getString(getResources().getString(R.string.token), null);
                                        String dId = prefManager.getSharedPref().getString(getResources().getString(R.string.dId), null);
                                        String uId = prefManager.getSharedPref().getString(getResources().getString(R.string.uId), null);

                                        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                                        RetrofitInterface retrofitInterface = RetrofitClient
                                                .getRetrofitClient(Constants.ACTION_BASE_URL).create(RetrofitInterface.class);
                                        Call<PaymentResponse> call = retrofitInterface
                                                .sendPaymentId(sessionToken, uId, "checkout",
                                                        dId, getResources()
                                                                .getString(R.string.isTab), token.getId(), mChapterPrice, "USD", mChapterId, mChapterName);
                                        call.enqueue(new Callback<PaymentResponse>() {
                                            @Override
                                            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {

                                                doneView.setEnabled(true);
                                                doneView.setClickable(true);

                                                if (response != null) {
                                                    PaymentResponse paymentResponse = response.body();
                                                    if (paymentResponse != null) {
                                                        int status = paymentResponse.getStatus();
                                                        String message = paymentResponse.getMessage();
                                                        Boolean paidStatus = paymentResponse.getPaidStatus();
                                                        if (paidStatus != null && paidStatus) {
                                                            ContentValues values = new ContentValues();
                                                            values.put(IS_PURCHASED, getResources().getInteger(R.integer.PURCHASED));
                                                            int rowUpdated = getContentResolver()
                                                                    .update(DbContract.Chapter.CONTENT_URI, values, CHAPTER_ID + "=?", new String[]{mChapterId});
                                                            if (rowUpdated > 0) {
                                                                prefManager.getEditor().putString(Constants.PAID_CHAPTER_ID, mChapterId).commit();
                                                                Intent intent = new Intent(BuyActivity.this, DashboardActivity.class);
                                                                intent.putExtra(EXTRA_TESTAMENT_TYPE, mTestamentType);
                                                                intent.putExtra(EXTRA_IS_TESTAMENT_ADDED, true);
                                                                Log.e("mTestamentType", mTestamentType + "-------");
                                                                Log.e("home", "clicked");
                                                                mProgressDialog.dismiss();
                                                                navigateUpTo(intent);
                                                                Toast.makeText(BuyActivity.this, "Payment has been successfully charged", Toast.LENGTH_SHORT).show();

                                                            } else {
                                                                finish();
                                                                Toast.makeText(BuyActivity.this, "Something went Wrong", Toast.LENGTH_SHORT).show();
                                                                mProgressDialog.dismiss();
                                                            }
                                                        } else {
                                                            mProgressDialog.dismiss();
                                                            finish();
                                                            Toast.makeText(BuyActivity.this, "Payment Unsuccessful", Toast.LENGTH_SHORT).show();
                                                        }
                                                        Log.e("status", status + " ,message " + message + " , paidStatus " + paidStatus);
                                                    } else {
                                                        Toast.makeText(BuyActivity.this, "Payment Unsuccessful", Toast.LENGTH_SHORT).show();
                                                        finish();
                                                        Log.e("paymentResponse", "null");
                                                    }
                                                } else {
                                                    Log.e("onResponse", "null");
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                                                mProgressDialog.dismiss();

                                                doneView.setEnabled(true);
                                                doneView.setClickable(true);

                                                Toast.makeText(BuyActivity.this, "Payment Unsuccessful", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        });
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else if (cardToSave == null) {
                            Log.e("card ", "null");

                            doneView.setEnabled(true);
                            doneView.setClickable(true);
                        }
                    } else {
                        Toast.makeText(BuyActivity.this, "Internet not Available", Toast.LENGTH_SHORT).show();
                    }
            }
        });

        cardView = (ImageView) findViewById(R.id.v_card);

        mCardMultilineWidget = (CardMultilineWidget) findViewById(R.id.v_card_multiline_widget);
        mCardInputListener = new CardInputListener() {
            @Override
            public void onFocusChange(String focusField) {
                Log.e("focusField", focusField + "");
            }

            @Override
            public void onCardComplete() {

                Log.e("focusField", "onCardComplete");
            }

            @Override
            public void onExpirationComplete() {
                Log.e("focusField", "onCardComplete");
            }

            @Override
            public void onCvcComplete() {
                Log.e("focusField", "onCvcComplete");
            }

            @Override
            public void onPostalCodeComplete() {
                Log.e("focusField", "onPostalCodeComplete");
            }

        };


        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                /*Intent intent = new Intent(BuyActivity.this, DashboardActivity.class);
                intent.putExtra(EXTRA_TESTAMENT_TYPE, mTestamentType);
                intent.putExtra(EXTRA_IS_TESTAMENT_ADDED, true);
                navigateUpTo(intent);*/
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        doneView.setEnabled(true);
        doneView.setClickable(true);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
