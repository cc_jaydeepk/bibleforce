package cpm.qbslearning.bibleforce;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class NavigationHintsActivity extends AppCompatActivity {

    WebView webView;
    ImageView button;
    private LinearLayout llSignupBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_navigation_hints);
        llSignupBack = findViewById(R.id.ll_signup_back);
        llSignupBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        button = (ImageView) findViewById(R.id.v_back);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        webView = (WebView) findViewById(R.id.v_web);
        webView.loadUrl("file:///android_asset/App_Instructions.html");
    }

}
