package cpm.qbslearning.bibleforce;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.kaopiz.kprogresshud.KProgressHUD;

import cpm.qbslearning.bibleforce.data.DbContract;
import cpm.qbslearning.bibleforce.dataModel.AccessCodeVerifyResponse;
import cpm.qbslearning.bibleforce.helperMethods.PrefManager;
import cpm.qbslearning.bibleforce.interfaceSet.RetrofitInterface;
import cpm.qbslearning.bibleforce.service.ChapterDownloadService;
import cpm.qbslearning.bibleforce.util.CommonMethods;
import cpm.qbslearning.bibleforce.util.Constants;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cpm.qbslearning.bibleforce.TestamentActivity.STORAGE_PERMISSION_REQUEST_CODE;

public class AccessCodeActivity extends AppCompatActivity implements View.OnClickListener {

    WebView webView;
    KProgressHUD mProgressDialog;
    TextView skipView;
    ImageView backView;
    private final String url = Constants.ACTION_BASE_URL + "accesscode.php?key=";
    TextView text;
    Dialog dialog;
    boolean isThisFromSetting=false;
    private static final int CALLER = 5;

    ImageView ivLoginPulsEffect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fabric.with(this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_code);
       backView=(ImageView)findViewById(R.id.v_back);
        mProgressDialog = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.7f);

        ivLoginPulsEffect=findViewById(R.id.bibl);

        Glide.with(this)
                .load(R.raw.logo_animation)
                .into(ivLoginPulsEffect);

        //Button skip=(Button)findViewById(R.id.v_btn_skip);
        TextView skip=(Button)findViewById(R.id.v_btn_skip);
        Intent intent=getIntent();
        if (intent!=null){
             isThisFromSetting= intent.getBooleanExtra("isThisFromSetting",false);
           if (isThisFromSetting) {
               backView.setVisibility(View.VISIBLE);
               backView.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       onBackPressed();
                   }
               });
           }

        }

//        webView = (WebView) findViewById(R.id.v_web);
//        webView.getSettings().setJavaScriptEnabled(true);
//
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                mProgressDialog.dismiss();
//            }
//
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                mProgressDialog.dismiss();
//                Toast.makeText(AccessCodeActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
//            }
//
//        });
//
//         webView = (WebView) findViewById(R.id.v_web);
    }

    @Override
    public void onResume() {
        super.onResume();
        String finalurl = url.concat(CommonMethods.getUserDeviceData(this));
        //Log.e("finalUrl", finalurl + "");
        registerReceiver(receiver, new IntentFilter(ChapterDownloadService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public void done(View view) {
        if (!isThisFromSetting)
          launchHomeScreen();
        else {
            onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
    }

    private void launchHomeScreen() {
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission ", "granted");
                    mProgressDialog.show();
                    ChapterDownloadService.startChapterDownloading(this, null, null, 0,CALLER);
                } else {
                    Log.e("permission", "DENIED");
                }
        }
    }


    public boolean VerifyAccessCode(Context context, String accessCode) {
        Log.e("VerifyAccessCode", accessCode);
        if (CommonMethods.isNetworkAvailable(context)) {
            mProgressDialog.show();
            final PrefManager pref = new PrefManager(context);
            String uId = pref.getSharedPref().getString(context.getResources().getString(R.string.uId), null);
            String token = pref.getSharedPref().getString(context.getResources().getString(R.string.token), null);
            String dId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            boolean result[] = {false};
            RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient(Constants.ACTION_BASE_URL).create(RetrofitInterface.class);
            Call<AccessCodeVerifyResponse> call = retrofitInterface.verifyAccessCode(token, uId, "checkAccessCode", dId, accessCode);
            call.enqueue(new Callback<AccessCodeVerifyResponse>() {
                @Override
                public void onResponse(Call<AccessCodeVerifyResponse> call, Response<AccessCodeVerifyResponse> response) {
                    mProgressDialog.dismiss();
                    Log.e("VerifyAccessCode", "response: " + response + "" + response.body());
                    if (response != null) {
                        AccessCodeVerifyResponse verifyResponse = response.body();
                        if (verifyResponse != null) {
                            String msg = verifyResponse.getMessage();
                            int status = verifyResponse.getStatus();
                            if (status > 0) {
                                PrefManager prefManager=new PrefManager(AccessCodeActivity.this);
                                prefManager.putIsAccessCodeAuthSuccess(true);
                                ContentValues values = new ContentValues();
                                values.put(DbContract.Chapter.IS_PURCHASED, getResources().getInteger(R.integer.PURCHASED));
                               // Toast.makeText(AccessCodeActivity.this,"",Toast.LENGTH_SHORT).show();
                                getContentResolver().update(DbContract.Chapter.CONTENT_URI, values, null, null);
                               // handlePermission();
                                launchHomeScreen();
                            } else {
                                Toast.makeText(AccessCodeActivity.this,"Invalid Access code!",Toast.LENGTH_SHORT).show();
                            }
                        } else
                            Toast.makeText(AccessCodeActivity.this, "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AccessCodeActivity.this, "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AccessCodeVerifyResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                    Toast.makeText(AccessCodeActivity.this, "Something went wrong Please try again", Toast.LENGTH_SHORT).show();
                }
            });

        } else {

            Toast.makeText(AccessCodeActivity.this, "Internet not available", Toast.LENGTH_SHORT).show();
        }
        return true;
    }



    public void onAccessCodeSubmit(View view){
        EditText accessCodeView=(EditText)findViewById(R.id.v_accesscode);
        String accessCode=accessCodeView.getText().toString();
        VerifyAccessCode(this,accessCode);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            mProgressDialog.dismiss();
            Log.e("TESTAMENT ", "onReceive");
            if (bundle != null) {
                Log.e("onRecieve ", "intent not null");
                int resultCode = bundle.getInt(ChapterDownloadService.RESULT);
                String chapterId = bundle.getString(Constants.EXTRA_CHAPTER_ID);
                int position = bundle.getInt(Constants.EXTRA_ITEM_POSITION, -1);
                int caller = bundle.getInt(Constants.EXTRA_CALLER, -1);
                if(caller == CALLER) {
                    if (/*resultCode == 1*/true) {
                        Toast.makeText(AccessCodeActivity.this, "Download Completed",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(AccessCodeActivity.this, "Download failed",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
            launchHomeScreen();
        }
    };

}
